/*
 * ana_win.c - A. BERAUD  (September 2003)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <errno.h>

#include "ana_win.h"
#include "interface.h"
#include "callbacks.h"


#define PI 3.1415926536


GtkWidget *ana_win,*ana_vbox,*ana_label;
GtkWidget *ana_cancel_button,*ana_print_button,*ana_plot_button;
GtkWidget *ana_hbox_names,*ana_hbox_buttons;
GtkWidget *ana_but_file_name,*ana_txt_ext,*ana_txt_ctime,*ana_txt_q;
GtkWidget *ana_hbox_desc,*ana_txt_desc;
GtkWidget *ana_hbox_choices,*ana_ana_opt_menu,*ana_reflex_opt_menu;
GtkWidget *ana_toggle_all;
GtkWidget *ana_file_selection,*ana_file_label;
GtkWidget *ana_hbox_extrem,*ana_txt_xmin,*ana_txt_xmax,*ana_txt_xshift;
char *ana_fname,*ana_small_fname,ana_ext[100],ana_desc[300];
char ana_ref[100];
int ana_num,ana_ctime,ref_num;
double ana_q;
char ana_xmin[100],ana_xmax[100];
double ana_xshift;
int main_only;



/* Self explanatory. */
void create_analysers_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	char tmpstr[100];

/*	if (ana_win && (GTK_WIDGET_REALIZED(ana_win)))
		return; */

	if (!strlen(ana_ref))
		strcpy(ana_ref,"9 9 9");
	if (!ana_num)
		ana_num = 2;
	if (!ref_num)
		ref_num = 2;
	if (!ana_q)
		ana_q = 0;
	if (!ana_xshift)
		ana_xshift = 0;
	if (!main_only)
		main_only = 0;
	if (!ana_fname)
		ana_fname = g_strdup("data_file");
	if (!ana_small_fname)
		ana_small_fname = g_strdup("data file");

	ana_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(ana_win),"Analyzers");
	gtk_signal_connect(GTK_OBJECT(ana_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(ana_win));
	gtk_signal_connect(GTK_OBJECT(ana_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(ana_win));

	ana_vbox=gtk_vbox_new(FALSE,2);
	gtk_widget_ref(ana_vbox);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_vbox",ana_vbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_container_add(GTK_CONTAINER(ana_win),ana_vbox);

	ana_hbox_names = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(ana_hbox_names);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_hbox_names",ana_hbox_names,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_hbox_names);
	gtk_box_pack_start(GTK_BOX(ana_vbox),ana_hbox_names,FALSE,FALSE,0);

	ana_but_file_name = gtk_button_new();
	ana_file_label = gtk_label_new(ana_small_fname);
	gtk_container_add(GTK_CONTAINER(ana_but_file_name),ana_file_label);
	gtk_widget_show(ana_file_label);
	gtk_widget_show(ana_but_file_name);
	gtk_signal_connect (GTK_OBJECT (ana_but_file_name), "clicked",
		GTK_SIGNAL_FUNC (on_ana_file_activate), NULL);
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_but_file_name,TRUE,TRUE,0);

/*
	ana_label=gtk_label_new("File name:");
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_label,FALSE,FALSE,0);

	ana_txt_file_name = gtk_entry_new();
	gtk_widget_ref(ana_txt_file_name);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_file_name",ana_txt_file_name,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)ana_txt_file_name,ana_fname);
	gtk_widget_show(ana_txt_file_name);
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_txt_file_name,FALSE,FALSE,0);
*/

	ana_label=gtk_label_new("Extension:");
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_label,FALSE,FALSE,0);

	ana_txt_ext = gtk_entry_new();
	gtk_widget_ref(ana_txt_ext);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_ext",ana_txt_ext,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)ana_txt_ext,ana_ext);
	gtk_widget_show(ana_txt_ext);
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_txt_ext,FALSE,FALSE,0);

	ana_label=gtk_label_new("Count. time (sec):");
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_label,FALSE,FALSE,0);

	ana_txt_ctime = gtk_entry_new();
	gtk_widget_ref(ana_txt_ctime);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_ctime",ana_txt_ctime,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",ana_ctime);
	gtk_entry_set_text((GtkEntry *)ana_txt_ctime,tmpstr);
	gtk_widget_show(ana_txt_ctime);
	gtk_box_pack_start(GTK_BOX(ana_hbox_names),ana_txt_ctime,FALSE,FALSE,0);

	ana_hbox_desc = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(ana_hbox_desc);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_hbox_desc",ana_hbox_desc,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_hbox_desc);
	gtk_box_pack_start(GTK_BOX(ana_vbox),ana_hbox_desc,FALSE,FALSE,0);

	ana_label=gtk_label_new("Description:");
	gtk_box_pack_start(GTK_BOX(ana_hbox_desc),ana_label,FALSE,FALSE,0);

	ana_txt_desc = gtk_entry_new();
	gtk_widget_ref(ana_txt_desc);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_desc",ana_txt_desc,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)ana_txt_desc,ana_desc);
	gtk_widget_show(ana_txt_desc);
	gtk_box_pack_start(GTK_BOX(ana_hbox_desc),ana_txt_desc,TRUE,TRUE,0);

	ana_hbox_choices = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(ana_hbox_choices);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_hbox_choices",ana_hbox_choices,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_hbox_choices);
	gtk_box_pack_start(GTK_BOX(ana_vbox),ana_hbox_choices,TRUE,TRUE,0);

	ana_label=gtk_label_new("Analyzer:");
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_label,FALSE,FALSE,0);

	ana_ana_opt_menu = gtk_option_menu_new();
	gtk_widget_ref(ana_ana_opt_menu);
	menu = gtk_menu_new();

	menu_item = gtk_menu_item_new_with_label ("1");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_ana_activate,"1");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("2");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_ana_activate,"2");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("3");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_ana_activate,"3");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("4");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_ana_activate,"4");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("5");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_ana_activate,"5");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);

	gtk_option_menu_set_menu(GTK_OPTION_MENU(ana_ana_opt_menu),menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(ana_ana_opt_menu),ana_num-1);
	gtk_widget_show(ana_ana_opt_menu);
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_ana_opt_menu,FALSE,FALSE,0);

	ana_label=gtk_label_new("Reflexion:");
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_label,FALSE,FALSE,0);

	ana_reflex_opt_menu = gtk_option_menu_new();
	gtk_widget_ref(ana_reflex_opt_menu);
	menu = gtk_menu_new();

	menu_item = gtk_menu_item_new_with_label ("7 7 7");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_reflex_activate,"7 7 7");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("8 8 8");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_reflex_activate,"8 8 8");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("9 9 9");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_reflex_activate,"9 9 9");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("11 11 11");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_reflex_activate,"11 11 11");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("13 13 13");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_ana_reflex_activate,"13 13 13");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);

	gtk_option_menu_set_menu(GTK_OPTION_MENU(ana_reflex_opt_menu),menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(ana_reflex_opt_menu),ref_num);
	gtk_widget_show(ana_reflex_opt_menu);
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_reflex_opt_menu,FALSE,FALSE,0);

	ana_label=gtk_label_new("Q [nm-1] (optional):");
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_label,FALSE,FALSE,0);

	ana_txt_q = gtk_entry_new();
	gtk_widget_ref(ana_txt_q);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_q",ana_txt_q,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",ana_q);
	gtk_entry_set_text((GtkEntry *)ana_txt_q,tmpstr);
	gtk_widget_show(ana_txt_q);
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_txt_q,FALSE,FALSE,0);

	ana_toggle_all = gtk_toggle_button_new_with_label("Print main ana only");
	gtk_widget_ref(ana_toggle_all);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_toggle_all",ana_toggle_all,(GtkDestroyNotify)gtk_widget_unref);
	if (main_only)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ana_toggle_all),TRUE);
	gtk_widget_show(ana_toggle_all);
	gtk_box_pack_start(GTK_BOX(ana_hbox_choices),ana_toggle_all,TRUE,TRUE,0);

	ana_hbox_extrem = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(ana_hbox_extrem);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_hbox_extrem",ana_hbox_extrem,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_hbox_extrem);
	gtk_box_pack_start(GTK_BOX(ana_vbox),ana_hbox_extrem,FALSE,FALSE,0);

	ana_label=gtk_label_new("Xmin (optional):");
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_label,FALSE,FALSE,0);

	ana_txt_xmin = gtk_entry_new();
	gtk_widget_ref(ana_txt_xmin);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_xmin",ana_txt_xmin,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)ana_txt_xmin,ana_xmin);
	gtk_widget_show(ana_txt_xmin);
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_txt_xmin,FALSE,FALSE,0);

	ana_label=gtk_label_new("Xmax (optional):");
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_label,FALSE,FALSE,0);

	ana_txt_xmax = gtk_entry_new();
	gtk_widget_ref(ana_txt_xmax);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_xmax",ana_txt_xmax,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)ana_txt_xmax,ana_xmax);
	gtk_widget_show(ana_txt_xmax);
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_txt_xmax,FALSE,FALSE,0);

	ana_label=gtk_label_new("Xshift :");
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_label,FALSE,FALSE,0);

	ana_txt_xshift = gtk_entry_new();
	gtk_widget_ref(ana_txt_xshift);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_txt_xshift",ana_txt_xshift,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",ana_xshift);
	gtk_entry_set_text((GtkEntry *)ana_txt_xshift,tmpstr);
	gtk_widget_show(ana_txt_xshift);
	gtk_box_pack_start(GTK_BOX(ana_hbox_extrem),ana_txt_xshift,FALSE,FALSE,0);


	ana_hbox_buttons = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(ana_hbox_buttons);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_hbox_buttons",ana_hbox_buttons,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_hbox_buttons);
	gtk_box_pack_start(GTK_BOX(ana_vbox),ana_hbox_buttons,FALSE,FALSE,0);

	ana_print_button=gtk_button_new_with_label("Print");
	gtk_widget_ref(ana_print_button);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_print_button",ana_print_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_print_button);
	gtk_signal_connect(GTK_OBJECT(ana_print_button),"clicked",
		GTK_SIGNAL_FUNC(on_ana_print_activate),ana_win);
	gtk_box_pack_start(GTK_BOX(ana_hbox_buttons),ana_print_button,TRUE,TRUE,0);

	ana_plot_button=gtk_button_new_with_label("Plot");
	gtk_widget_ref(ana_plot_button);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_plot_button",ana_plot_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_plot_button);
	gtk_signal_connect(GTK_OBJECT(ana_plot_button),"clicked",
		GTK_SIGNAL_FUNC(on_ana_plot_activate),ana_win);
	gtk_box_pack_start(GTK_BOX(ana_hbox_buttons),ana_plot_button,TRUE,TRUE,0);

	ana_cancel_button=gtk_button_new_with_label("Cancel");
	gtk_widget_ref(ana_cancel_button);
	gtk_object_set_data_full(GTK_OBJECT(ana_win),"ana_cancel_button",ana_cancel_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(ana_cancel_button);
	gtk_signal_connect(GTK_OBJECT(ana_cancel_button),"clicked",
		GTK_SIGNAL_FUNC(on_ana_cancel_activate),ana_win);
	gtk_box_pack_start(GTK_BOX(ana_hbox_buttons),ana_cancel_button,TRUE,TRUE,0);

	gtk_widget_show_all(ana_win);
} 

void on_ana_cancel_activate (GtkWidget *item, gpointer user_data)
{
	ana_capture_data();
	gtk_widget_destroy(GTK_WIDGET(ana_win));
}

void on_ana_plot_activate (GtkWidget *widget, gpointer data)
{
	ana_write_files(1);
}

void ana_file_select_ok (GtkWidget *w, GtkFileSelection *fs)
{
	char *tmp1,*tmp2,tmp3[300];

	ana_fname=g_strdup(gtk_file_selection_get_filename(fs));
	gtk_widget_destroy(GTK_WIDGET(ana_file_selection));
	ana_small_fname = g_basename(ana_fname);
	tmp1 = g_dirname(ana_fname);
	tmp2 = g_get_current_dir();
	g_rel2abs(tmp1,tmp2,tmp3,500);
	strcat(tmp3,"/");
	strcat(tmp3,ana_small_fname);
	ana_fname =g_strdup(tmp3);
	gtk_label_set_text(GTK_LABEL(ana_file_label),ana_small_fname);
}

void on_ana_file_activate (GtkWidget *item, gpointer user_data)
{
	ana_file_selection=gtk_file_selection_new ("Select a file...");
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(ana_file_selection)->ok_button),
                      "clicked",(GtkSignalFunc)ana_file_select_ok,ana_file_selection);
	gtk_signal_connect_object (GTK_OBJECT(GTK_FILE_SELECTION(ana_file_selection)->cancel_button),
                             "clicked",(GtkSignalFunc)gtk_widget_destroy,
                             GTK_OBJECT(ana_file_selection));
	if (ana_fname)
		gtk_file_selection_set_filename(GTK_FILE_SELECTION(ana_file_selection),ana_fname);
/*	gtk_file_selection_complete(GTK_FILE_SELECTION(ana_file_selection),"*.inp"); */
	gtk_widget_show(ana_file_selection);
}

void on_ana_reflex_activate (GtkWidget *item, gpointer user_data)
{
	strcpy(ana_ref,user_data);
	if (!strcmp(user_data,"7 7 7"))
		ref_num = 0;
	if (!strcmp(user_data,"8 8 8"))
		ref_num = 1;
	if (!strcmp(user_data,"9 9 9"))
		ref_num = 2;
	if (!strcmp(user_data,"11 11 11"))
		ref_num = 3;
	if (!strcmp(user_data,"13 13 13"))
		ref_num = 4;
}

void on_ana_ana_activate (GtkWidget *item, gpointer user_data)
{
	char txt[100];
	strcpy(txt,user_data);
	ana_num = atoi(txt);
}

void ana_capture_data (void)
{
/*	strcpy(ana_fname,gtk_entry_get_text(GTK_ENTRY(ana_txt_file_name))); */
	strcpy(ana_ext,gtk_entry_get_text(GTK_ENTRY(ana_txt_ext)));
	strcpy(ana_desc,gtk_entry_get_text(GTK_ENTRY(ana_txt_desc)));
	ana_ctime = atoi(gtk_entry_get_text(GTK_ENTRY(ana_txt_ctime)));
	ana_q = atof(gtk_entry_get_text(GTK_ENTRY(ana_txt_q)));
	ana_xshift = atof(gtk_entry_get_text(GTK_ENTRY(ana_txt_xshift)));
	strcpy(ana_xmin,gtk_entry_get_text(GTK_ENTRY(ana_txt_xmin)));
	strcpy(ana_xmax,gtk_entry_get_text(GTK_ENTRY(ana_txt_xmax)));
	if (GTK_TOGGLE_BUTTON (ana_toggle_all)->active) 
		main_only = 1;
	else
		main_only = 0;
}

void on_ana_print_activate (GtkWidget *item, gpointer user_data)
{
	ana_write_files(0);
}

void ana_write_files (int type)
{
	int i,j,n,step,ref_num;
	char txt[BUFSIZ],line[BUFSIZ];
	char full_name[BUFSIZ];
	double tth,q;
	FILE *fil_src,*fil_dest;

	/* Definition of the offset parameters. */
	double off_convert[5][5] =  /* [Analyzer][Reflexion] */
		{
			{0,-23,-13.5,-28.1,13.2},
			{0,-4.7,-4.7,-93,13.2},
			{0,-18.3,-13.3,-22,13.2},
			{0,-16.5,-10.3,-19.2,13.2},
			{0,-29,-7.3,-34.5,13.2}
		};
	double off_de[5][5] =  /* [Analyzer][Reflexion] */
		{
			{0,-14.32,-16.9,15.56975,0},
			{0,-12.13,0,19.06975,0},
			{0,-21.519,-12.2,5.5,0},
			{0,-25.14,-10.3,7.35,0},
			{0,-33,-11.1,-3.842,0}
		};
	double tth_diff [5][5] =  /* 2theta difference between analyzers */
		{
			{0,1.577,3.085,4.625,6.203},
			{-1.577,0,1.508,3.058,4.626},
			{-3.085,-1.508,0,1.55,3.118},
			{-4.625,-3.058,-1.55,0,1.568},
			{-6.203,-4.626,-3.118,-1.568,0}
		};
	double y_pos[4] = {20,14,8,2};
	double lambda[5] = {0,0.07839,0.06968,0.05701,0.04824}; /* nm */

	ana_capture_data();

	for (i=0;i<5;i++)
		for (j=0;j<5;j++)
			off_convert[i][j] += ana_xshift;

	ref_num=0;
	if (!strcmp(ana_ref,"8 8 8"))
		ref_num = 1;
	if (!strcmp(ana_ref,"9 9 9"))
		ref_num = 2;
	if (!strcmp(ana_ref,"11 11 11"))
		ref_num = 3;
	if (!strcmp(ana_ref,"13 13 13"))
		ref_num = 4;

	strcpy(full_name,getenv("HOME"));
	strcat(full_name,"/.fit28/");
	strcat(full_name,"one_ana.do");

	fil_src = fopen(full_name,"r");
	if (!fil_src)
	{
		printf("Error: can not open source file [%s]\n",full_name);
		return;
	}
	fil_dest = fopen("one_ana.do","w");
	if (!fil_dest)
	{
		printf("Error: can not open destination file [%s]\n","one_ana.do");
		return;
	}

	while (fgets(line,BUFSIZ,fil_src))
	{
		if ((line == NULL) || (strlen(line)==0))
			continue;
		if (!strncasecmp(line,"{replace1}",10))
		{
			sprintf(txt,"fn scans.4 -f %s +dSn -s x=%d y=30 z=%d m=34 %s\n",
				ana_fname,12+ana_num,23+ana_num,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace2}",10))
		{
			sprintf(txt,"%s ; Si(%s) - %s.%s\n",ana_desc,ana_ref,ana_small_fname,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace3}",10))
		{
			sprintf(txt,"%d secs\n",ana_ctime);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace4}",10))
		{
			strcpy(txt,"\n");
			if (!strcmp(ana_ref,"8 8 8"))
				sprintf(txt,"fn calc.4 x = (x-y)*0.00258*15817 %+g\n",
					off_convert[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"9 9 9"))
				sprintf(txt,"fn calc.4 x = (x-y)*0.00258*17794 %+g\n",
					off_convert[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"11 11 11"))
				sprintf(txt,"fn calc.4 x = (x-y)*0.00258*21747 %+g\n",
					off_convert[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"13 13 13"))
				sprintf(txt,"fn calc.4 x = (x-y)*0.00258*25703 %+g\n",
					off_convert[ana_num-1][ref_num]);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace5}",10))
		{
			sprintf(txt,"fn scans.4 -f %s -n +dS -s x=1 y=40 z=30 %s\n",
				ana_fname,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace6}",10))
		{
			sprintf(txt,"fn scans.4 -f %s -n +S -s x=1 y=%d %s\n",
				ana_fname,12+ana_num,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace7}",10))
		{
			strcpy(txt,"\n");
			if (!strcmp(ana_ref,"8 8 8"))
				sprintf(txt,"fn calc.4 y = 2.58e-6*1000*15817*(y-22.218) %+g\n",
					off_de[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"9 9 9"))
				sprintf(txt,"fn calc.4 y = 2.58e-6*1000*17794*(y-22.5154) %+g\n",
					off_de[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"11 11 11"))
				sprintf(txt,"fn calc.4 y = 2.58e-6*1000*21747*(y-22.851) %+g\n",
					off_de[ana_num-1][ref_num]);
			if (!strcmp(ana_ref,"13 13 13"))
				sprintf(txt,"fn calc.4 y = 2.58e-6*1000*25703*(y-22.0687) %+g\n",
					off_de[ana_num-1][ref_num]);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace8}",10))
		{
			sprintf(txt,"%d secs\n",ana_ctime);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace9}",10))
		{
			sprintf(txt,"fn scans.4 -f %s -n +S -s x=1 y=34 z=40 %s\n",
				ana_fname,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace10}",11))
		{
			sprintf(txt,"%d secs\n",ana_ctime);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace11}",11))
		{
			sprintf(txt,"fn scans.4 -f %s -n +S -s x=1 y=%d %s\n",
				ana_fname,23+ana_num,ana_ext);
			fputs(txt,fil_dest);
			continue;
		}
		if (!strncasecmp(line,"{replace12}",11))
		{
			sprintf(txt,"%s\n%s\n\n\n",ana_xmin,ana_xmax);
			fputs(txt,fil_dest);
			continue;
		}
		fputs(line,fil_dest);
	}

	fclose(fil_dest);
	fclose(fil_src);

	if (type)
		sprintf(txt,"chg one_ana.do x11");
	else
		sprintf(txt,"chg one_ana.do ps");
	system(txt);

	if (plot_prgm_now == 1)
		close_gnuplot();
/*	if (plot_prgm_now != 0) */
		open_cplot();
	if (!tcname)
		return;
	fprintf(cplot, "do one_ana.do\n");
	fflush(cplot);

	if (main_only)
		return;

	/* Writing and printing of the other analyzers */
	strcpy(full_name,getenv("HOME"));
	strcat(full_name,"/.fit28/");
	strcat(full_name,"all_ana.do");

	fil_src = fopen(full_name,"r");
	if (!fil_src)
	{
		printf("Error: can not open source file [%s]\n",full_name);
		return;
	}
	fil_dest = fopen("all_ana.do","w");

	fputs("# Treatment for IXS Scans\nre\ntu 1\n\n",fil_dest);

	step = 0;
	/* Loop over all the analysers... */
	for (n=0;n<5;n++)
	{
		if (n==(ana_num-1))
			continue; /* ...all but the chosen one. */
		rewind(fil_src); /* Point at the beginning of the source file because
			we will read it 4 times. */
		fputs("##########################################################################\n\n",fil_dest);
		sprintf(txt,"# Analyser %d\n",n+1); /* This is cosmetic. */
		fputs(txt,fil_dest);

		while (fgets(line,BUFSIZ,fil_src))
		{
			if (!strncasecmp(line,"{replace1}",10))
			{
				if (ana_q) /* We have to calculate Q for all the anas */
				{
					tth = ana_q*lambda[ref_num]/(4*PI);
					/* In fact, tth contains the sine. */
					tth = asin(tth)*2;
					/* Now we calculate the tth for this perticular	analyser. */
					tth += (tth_diff[ana_num-1][n])*PI/180;
					/* Recalculate Q */
					q = (4*PI)*sin(tth/2)/lambda[ref_num];
					sprintf(txt,"Ana%d, Q%d=%.2f\n",n+1,n+1,q);
				}
				else
				{
					sprintf(txt,"Ana%d, Q%d=______\n",n+1,n+1);
				}
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace2}",10))
			{
				sprintf(txt,"deta%d/I%d\n",n+1,n+1);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace3}",10))
			{
				sprintf(txt,"fn scans.4 -f %s +dSn -s x=%d y=30 z=%d m=34 %s\n",
					ana_fname,13+n,24+n,ana_ext);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace4}",10))
			{
				strcpy(txt,"\n");
				if (!strcmp(ana_ref,"8 8 8"))
					sprintf(txt,"fn calc.4 x = (x-y)*0.00258*15817 %+g\n",
						off_convert[n][ref_num]);
				if (!strcmp(ana_ref,"9 9 9"))
					sprintf(txt,"fn calc.4 x = (x-y)*0.00258*17794 %+g\n",
						off_convert[n][ref_num]);
				if (!strcmp(ana_ref,"11 11 11"))
					sprintf(txt,"fn calc.4 x = (x-y)*0.00258*21747 %+g\n",
						off_convert[n][ref_num]);
				if (!strcmp(ana_ref,"13 13 13"))
					sprintf(txt,"fn calc.4 x = (x-y)*0.00258*25703 %+g\n",
						off_convert[n][ref_num]);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace5}",10))
			{
				sprintf(txt,"wi 3.0 %g 5.5 4\n",y_pos[step]);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace6}",10))
			{
				if (step==0)
					fputs("zi psfilter @lp @-da4bid28\n#zi x11 -rotate\nztapldw\nsy L\nzp\n",fil_dest);
				else
					fputs("zaltp\n",fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace7}",10))
			{
				if (step==0)
					sprintf(txt,"%s.%s\n",ana_small_fname,ana_ext);
				else
					sprintf(txt,"DE of analyser\n");
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace8}",10))
			{
				sprintf(txt,"fn scans.4 -f %s -n +S -s x=1 y=%d %s\n",ana_fname,13+n,ana_ext);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace9}",10))
			{
				strcpy(txt,"\n");
				if (!strcmp(ana_ref,"8 8 8"))
					sprintf(txt,"fn calc.4 y = 2.58e-6*1000*15817*(y-22.218) %+g\n",
						off_de[n][ref_num]);
				if (!strcmp(ana_ref,"9 9 9"))
					sprintf(txt,"fn calc.4 y = 2.58e-6*1000*17794*(y-22.449) %+g\n",
						off_de[n][ref_num]);
				if (!strcmp(ana_ref,"11 11 11"))
					sprintf(txt,"fn calc.4 y = 2.58e-6*1000*21747*(y-22.851) %+g\n",
						off_de[n][ref_num]);
				if (!strcmp(ana_ref,"13 13 13"))
					sprintf(txt,"fn calc.4 y = 2.58e-6*1000*25703*(y-22.0697) %+g\n",
						off_de[n][ref_num]);
				fputs(txt,fil_dest);
				continue;
			}
			if (!strncasecmp(line,"{replace10}",11))
			{
				sprintf(txt,"wi 11.5 %g 5.5 4\n",y_pos[step]);
				fputs(txt,fil_dest);
				continue;
			}
			fputs(line,fil_dest);
		}
		step++;
	}
	fputs("zs\n",fil_dest);

	fclose(fil_dest);
	fclose(fil_src);

	if (type)
		sprintf(txt,"chg all_ana.do x11");
	else
		sprintf(txt,"chg all_ana.do ps");
	system(txt);

	if (!tcname)
		open_cplot();
	if (!tcname)
		return;
	fprintf(cplot, "do all_ana.do\n");
	fflush(cplot);
}

char *g_rel2abs (path, base, result, size)
     const char *path;
     const char *base;
     char *result;
     const size_t size;
{
	const char *pp, *bp;
	/* endp points the last position which is safe in the result buffer. */
	const char *endp = result + size - 1;
	char *rp;
	int length;
	char dots[] = {'.', '.', G_DIR_SEPARATOR, '\0'};
	char *parent = dots;
	char *current = dots + 1;

  if (*path == G_DIR_SEPARATOR)
    {
      if (strlen (path) >= size)
	goto erange;
      strcpy (result, path);
      goto finish;
    }
  else if (*base != G_DIR_SEPARATOR || !size)
    {
      errno = EINVAL;
      return (NULL);
    }
  else if (size == 1)
    goto erange;
  if (!strcmp (path, ".") || !strcmp (path, current))
    {
      if (strlen (base) >= size)
	goto erange;
      strcpy (result, base);
      /* rp points the last char. */
      rp = result + strlen (base) - 1;
      if (*rp == G_DIR_SEPARATOR)
	*rp = 0;
      else
	rp++;
      /* rp point NULL char */
      if (*++path == G_DIR_SEPARATOR)
	{
	  /* Append G_DIR_SEPARATOR to the tail of path name. */
	  *rp++ = G_DIR_SEPARATOR;
	  if (rp > endp)
	    goto erange;
	  *rp = 0;
	}
      goto finish;
    }
  bp = base + strlen (base);
  if (*(bp - 1) == G_DIR_SEPARATOR)
    --bp;
  /* up to root. */
  for (pp = path; *pp && *pp == '.';)
    {
      if (!strncmp (pp, parent, 3))
	{
	  pp += 3;
	  while (bp > base && *--bp != G_DIR_SEPARATOR)
	    ;
	}
      else if (!strncmp (pp, current, 2))
	{
	  pp += 2;
	}
      else if (!strncmp (pp, "..\0", 3))
	{
	  pp += 2;
	  while (bp > base && *--bp != G_DIR_SEPARATOR)
	    ;
	}
      else
	break;
    }
  /* down to leaf. */
  length = bp - base;
  if (length >= size)
    goto erange;
  strncpy (result, base, length);
  rp = result + length;
  if (*pp || *(pp - 1) == G_DIR_SEPARATOR || length == 0)
    *rp++ = G_DIR_SEPARATOR;
  if (rp + strlen (pp) > endp)
    goto erange;
  strcpy (rp, pp);
finish:
  return result;
erange:
  errno = ERANGE;
  return (NULL);
}
