
#include <gtk/gtk.h>

extern GtkWidget *ana_win,*ana_vbox,*ana_label;
extern GtkWidget *ana_cancel_button,*ana_print_button,*ana_plot_button;
extern GtkWidget *ana_hbox_names,*ana_hbox_buttons;
extern GtkWidget *ana_but_file_name,*ana_txt_ext,*ana_txt_ctime,*ana_txt_q;
extern GtkWidget *ana_hbox_desc,*ana_txt_desc;
extern GtkWidget *ana_hbox_choices,*ana_ana_opt_menu,*ana_reflex_opt_menu;
extern GtkWidget *ana_toggle_all;
extern GtkWidget *ana_file_selection,*ana_file_label;
extern GtkWidget *ana_hbox_extrem,*ana_txt_xmin,*ana_txt_xmax,*ana_txt_xshift;
extern char *ana_fname,*ana_small_fname,ana_ext[100],ana_desc[300];
extern char ana_ref[100];
extern int ana_num,ana_ctime,ref_num;
extern double ana_q;
extern char ana_xmin[100],ana_xmax[100];
extern double ana_xshift;
extern int main_only;

void create_analysers_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void on_ana_cancel_activate (GtkWidget *item, gpointer user_data);
void ana_file_select_ok (GtkWidget *w, GtkFileSelection *fs);
void on_ana_file_activate (GtkWidget *item, gpointer user_data);
void ana_capture_data (void);
void on_ana_print_activate (GtkWidget *item, gpointer user_data);
void on_ana_reflex_activate (GtkWidget *item, gpointer user_data);
void on_ana_ana_activate (GtkWidget *item, gpointer user_data);
void ana_write_files (int type);
void on_ana_plot_activate (GtkWidget *widget, gpointer data);
char *g_rel2abs (const char *path, const char *base, char *result, const size_t size);
