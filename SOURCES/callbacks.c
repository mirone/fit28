/*
 * callbacks.c - A. BERAUD  (March 2003-2007)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <pthread.h>
#include <sys/timeb.h>

#include "callbacks.h"
#include "interface.h"
#include "fitvie.h"
#include "plot.h"

char *par_nm[6] = {"BG","SL","ZR","T","PC","GC"};
char *visc_labels[10] = {"VISI","OMEGA0","GAMMA","TAUT","OMEGAINF","TAU1","GAMMA0", "DELTA","TAUD","BETA"};


char nomefile[300],nomeout[300],nomeris[300],model[16],nomedo[300],nomenorm[300],nomepar[300],title[300],counts[5],centtype[30];
int i,nn,nc,nex,ntotpar,nris,res_zero,res_z1,res_z2;
int fl_ylog,fl_preview,fl_stop_fit,fl_visco_e2,fl_cen_bose;
int fl_live,fl_t_as_param,fl_convol_type;
char last_data_file[300];
int fit_types[2];
int fft_active;
int iter,swap[2];
double temp,frac,dist,avmon;
double frq[600],dat[600],erd[600],mon[600];
double err_par[52];
double thc,tcn[12000],tcne2[12000],tcnc[1200],tcnv[1200],tcne[12][1200],a[12];
double visb,visa;
double fris[12000],dris[12000],ddris[12000],frisi,frise,dfris;
double fff[2*10000+1],ris[2*10000+1];
double sum,pig,fatt,bol,TK,stpr,stepr,stepq,res_max;
double BG,SL,ZR,GR,PC,_GC,SA,ta,aa,bb,P[12],O[12],G[12],V[10];
double wj,wk,dw,ak,dak,ac;
double sumexc[12],sumcen,sumall,sumvis,sume2,meane2;
double sumexc_err[12],sumcen_err,sumall_err,sumvis_err,meane2;
int ik;
double chi2,real_chi2,dummy,bose;
double psv_gamma1,psv_gamma2,psv_eta;
double parameters[100],delta[100];
char ex[12][5];
char rest_arg[BUFSIZ];
pthread_t fit_thread;
int fit_running;
struct timeb live_timeb;
struct timeb info_timeb;
double live_interval;
char *tname,*tcname;
FILE *gnuplot,*cplot,*data;



void open_cplot(void)
{
	if ((tcname = tmpnam((char *)0)) == 0)
		print_info("Error: can not create cplot temporary name.\n","");
	cplot = popen("cplot","w");
	plot_prgm_now = 0;
}

void close_cplot(void)
{
	if (cplot)
		pclose(cplot);
	if (tcname)
		unlink(tcname);
	tcname = NULL;
	plot_prgm_now = NULL;
}

void open_gnuplot(void)
{
	if ((tcname = tmpnam((char *)0)) == 0)
		print_info("Error: can not create gnuplot temporary name.","");
	gnuplot = popen("gnuplot","w");
	fprintf(gnuplot, "set mouse\n");
/*	fprintf(gnuplot, "plot \"icf6_8.005\"\n");
	fflush(gnuplot); */
	plot_prgm_now = 1;
}

void close_gnuplot(void)
{
	if (gnuplot)
		pclose(gnuplot);
	if (tcname)
		unlink(tcname);
	tcname = NULL;
	plot_prgm_now = NULL;
}

int read_dat_file_to_array (void)
{
	int pt,n;
	double x,y;
	char tmpstr[300];
	FILE *fil;

	fil = fopen(nomefile,"r");
	if (!fil)
		return -1;
	pt = find_free_plot_table();
	if (pt == -1)
		return -1;
	n = 0;
	if (plot_table[pt] != NULL)
		free(plot_table[pt]);
	while (fgets(tmpstr,BUFSIZ,fil))
	{
		plot_table[pt] = (plot_point *) realloc(plot_table[pt],(n+1)*sizeof(plot_point));
		sscanf(tmpstr,"%lf %lf",&x,&y);
		plot_table[pt][n].x = x;
		plot_table[pt][n].y = y;
		n++;
	}
	plot_info[pt].used = 1;
	plot_info[pt].nb_points = n;
	fclose(fil);
	return pt;
}

void plot_do_file (void)
{
	int n;
	char filename[300];
	FILE *fil;

	capture_data();
	if (fl_preview)
	{
		strcpy(filename,getenv("HOME"));
		strcat(filename,"/.fit28/");
		strcat(filename,"preview.do");
	}
	else
	{
		strcpy(filename,nomedo);
	}
	if (plot_prgm != 2)
	{
		fil = fopen(filename,"r");
		if (!fil)
		{
			print_info("No suitable DO file [%s]. Something went wrong.",nomedo);
			return;
		}
		fclose(fil);
	}
	if (plot_prgm == 0)
	{
		if (plot_prgm_now==1)
			close_gnuplot();
		if (!tcname)
			open_cplot();
		if (!tcname)
			return;
		make_x11_do_file();
		if (fl_ylog)
			make_log_do_file();
		else
			make_linear_do_file();
		fprintf(cplot, "do %s\n",filename);
		fflush(cplot);
		return;
	}
	if (plot_prgm == 1)
	{
		if (plot_prgm_now==0)
			close_cplot();
		if (!tcname)
			open_gnuplot();
		if (!tcname)
			return;
		if (fl_ylog)
			fprintf(gnuplot, "set logscale y\n");
		else
			fprintf(gnuplot, "set nologscale y\n");
		fprintf(gnuplot, "load \"%s\"\n",filename);
		fflush(gnuplot);
		return;
	}
	if (plot_prgm == 2)
	{
		if (!plot_win)
			create_plot_window();
		n = read_dat_file_to_array();
		if (n!=-1)
			plot_data(n);
	}
}

void on_plot_activate (GtkWidget *widget, gpointer data)
{
	plot_do_file();
}

void print_do_file (GtkWidget *widget, gpointer data)
{
	if (plot_prgm == 0) /* C-plot */
	{
		if (!tcname)
			open_cplot();
		if (!tcname)
			return;
		capture_data();
		make_ps_do_file();
		if (fl_ylog)
			make_log_do_file();
		else
			make_linear_do_file();
		fprintf(cplot, "do %s\n",nomedo);
		fflush(cplot);
		return;
	}
	if (plot_prgm == 1)
	{
		if (!tcname)
			open_gnuplot();
		if (!tcname)
			return;
		capture_data();
		if (fl_ylog)
			fprintf(gnuplot, "set logscale y\n");
		else
			fprintf(gnuplot, "set nologscale y\n");
		fprintf(gnuplot, "set terminal postscript landscape enhanced\n");
		fprintf(gnuplot, "set output \"| lpr -P%s\"\n",printer);
		fprintf(gnuplot, "replot\n");
		fprintf(gnuplot, "set output\n");
		fprintf(gnuplot, "set terminal x11\n");
		fflush(gnuplot);
		print_info("Printed on %s.",printer);
		return;
	}

}

void print_file_do_file (GtkWidget *widget, gpointer data)
{
	if (plot_prgm == 0) /* C-plot */
	{
		if (!tcname)
			open_cplot();
		if (!tcname)
			return;
		capture_data();
		make_ps_do_file();
		if (fl_ylog)
			make_log_do_file();
		else
			make_linear_do_file();
		fprintf(cplot, "do %s\n",nomedo);
		fflush(cplot);
		return;
	}
	if (plot_prgm == 1)
	{
		if (!tcname)
			open_gnuplot();
		if (!tcname)
			return;
		capture_data();
		if (fl_ylog)
			fprintf(gnuplot, "set logscale y\n");
		else
			fprintf(gnuplot, "set nologscale y\n");
		fprintf(gnuplot, "set terminal postscript eps color solid\n");
		fprintf(gnuplot, "set output 'plot.eps'\n");
		fprintf(gnuplot, "replot\n");
		fprintf(gnuplot, "set output\n");
		fprintf(gnuplot, "set terminal x11\n");
		fflush(gnuplot);
		print_info("Printed in 'plot.eps'.","");
		return;
	}

}

int is_valid_model (char *mo)
{
	int i;
	char *models[5]={"dho","lor","gau","del","vis"};
	for (i=0;i<5;i++)
		if (!strcasecmp(mo,models[i]))
			return 1;
	return 0;
}

void general_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(model,data);
	if (!strcasecmp(model,"vis"))
	{
		nb_visc = 1;
		visco_fixed[2] = 1;
		visco_fixed[3] = 1;
		visco_fixed[4] = 1;
		visco_fixed[5] = 1;
	}
	else
		nb_visc = 0;
	refresh_displayed_data();
}

void exc1_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[0],data);
}

void exc2_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[1],data);
}

void exc3_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[2],data);
}

void exc4_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[3],data);
}

void exc5_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[4],data);
}

void exc6_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[5],data);
}

void exc7_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[6],data);
}

void exc8_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[7],data);
}

void exc9_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[8],data);
}

void exc10_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[9],data);
}

void exc11_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[10],data);
}

void exc12_model_menu (GtkWidget *widget, gpointer data)
{
	strcpy(ex[11],data);
}

void handle_cntint (GtkWidget *widget, gpointer data)
{
	strcpy(nomenorm,data);
}

void handle_central_type (GtkWidget *widget, gpointer data)
{
	strcpy(centtype,data);
}

void fix_callback (GtkWidget *widget, gpointer data)
{
	int nb = (int)data;
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		fixed[nb] = 1;
	} else {
		fixed[nb] = 0;
	}
}

void clear_swap (void)
{
	swap[0] = -1;
	swap[1] = -1;
}

void swap_callback (GtkWidget *widget, gpointer data)
{
	int i,k,l,f;
	int nb = (int)data;
	char tmp[BUFSIZ];
	struct {double a,b,c,d;} tmp_exc[3];

	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		for (i=0;i<2;i++)
		{
			if (swap[i]==-1)
			{
				swap[i] = nb;
				if ((swap[0]!=-1) && (swap[1]!=-1))
				{
					/* Swap data */
					k = (swap[0]-1)*3;
					l = (swap[1]-1)*3;
					memcpy(&tmp_exc[0],&excits[k],4*sizeof(double));
					memcpy(&tmp_exc[1],&excits[k+1],4*sizeof(double));
					memcpy(&tmp_exc[2],&excits[k+2],4*sizeof(double));
					memcpy(&excits[k],&excits[l],4*sizeof(double));
					memcpy(&excits[k+1],&excits[l+1],4*sizeof(double));
					memcpy(&excits[k+2],&excits[l+2],4*sizeof(double));
					memcpy(&excits[l],&tmp_exc[0],4*sizeof(double));
					memcpy(&excits[l+1],&tmp_exc[1],4*sizeof(double));
					memcpy(&excits[l+2],&tmp_exc[2],4*sizeof(double));
					/* Swap fixes */
					k += 6;
					l += 6;
					f = fixed[k];
					fixed[k] = fixed[l];
					fixed[l] = f;
					f = fixed[k+1];
					fixed[k+1] = fixed[l+1];
					fixed[l+1] = f;
					f = fixed[k+2];
					fixed[k+2] = fixed[l+2];
					fixed[l+2] = f;
					/* Swap models types */
					k = swap[0]-1;
					l = swap[1]-1;
					strcpy(tmp,ex[k]);
					strcpy(ex[k],ex[l]);
					strcpy(ex[l],tmp);
					clear_swap();
					refresh_displayed_data();
				}
				return;
			}
		}
	} else {
		for (i=0;i<2;i++)
		{
			if (swap[i]==nb)
			{
				swap[i] = -1;
				return;
			}
		}
	}
}

void fix_visco_callback (GtkWidget *widget, gpointer data)
{
	int nb = (int)data;
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		visco_fixed[nb] = 1;
	} else {
		visco_fixed[nb] = 0;
	}
}

void log_fix_callback (GtkWidget *widget, gpointer data)
{
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		fl_ylog = 1;
	} else {
		fl_ylog = 0;
	}
}

void live_chk_callback (GtkWidget *widget, gpointer data)
{
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		fl_live = 1;
		live_interval = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(live_spin));
	} else {
		fl_live = 0;
	}
}

/*
void pseudo_voigt_callback (GtkWidget *widget, gpointer data)
{
	int i,j;
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		fl_no_convol = 2;
		fixed[5] = 1;
		for (i=0;i<nex;i++)
		{
			j = 6+(i*3)+2;
			fixed[j] = 1;
		}
	} else {
		fl_no_convol = 1;
	}
}
*/

void central_bose_callback (GtkWidget *widget, gpointer data)
{
	int i,j;
	if (GTK_TOGGLE_BUTTON(widget)->active) 
	{
		fl_cen_bose = 1;
	} else {
		fl_cen_bose = 0;
	}
}


void convol_choices_callback (GtkWidget *widget, gpointer data)
{
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
	{
		fl_convol_type = (int) data;
/*		if ((fl_convol_type==2) || (fl_convol_type==3))
		{
			on_pv_load_activate(NULL,NULL);
		} */
	}
/*	refresh_displayed_data(); */
}


void init_vis_params (void)
{
	viscs[0].a = 100;
	viscs[0].b = 1;
	viscs[0].c = 0;
	viscs[0].d = 1000000;
	viscs[1].a = 1;
	viscs[1].b = 0.01;
	viscs[1].c = 0.1;
	viscs[1].d = 100;
	viscs[2].a = 2;
	viscs[2].b = 0.01;
	viscs[2].c = 1;
	viscs[2].d = 10;
	viscs[3].a = 1;
	viscs[3].b = 0.01;
	viscs[3].c = 0;
	viscs[3].d = 100;
	viscs[4].a = 2;
	viscs[4].b = 0.01;
	viscs[4].c = 1;
	viscs[4].d = 100;
	viscs[5].a = 1;
	viscs[5].b = 0.01;
	viscs[5].c = 0;
	viscs[5].d = 100;
	viscs[6].a = 1;
	viscs[6].b = 0.01;
	viscs[6].c = 0;
	viscs[6].d = 100;
	viscs[7].a = 1;
	viscs[7].b = 0.01;
	viscs[7].c = 0;
	viscs[7].d = 10;
	viscs[8].a = 1;
	viscs[8].b = 0.01;
	viscs[8].c = 0;
	viscs[8].d = 100;
	viscs[9].a = 0.8;
	viscs[9].b = 0.01;
	viscs[9].c = 0;
	viscs[9].d = 1;
}

void homogene_names (GtkWidget *widget, gpointer data)
{
	GtkWidget *entry;
	int i,j;
	char bname[300],name[300],ext[100],*ptr;
	GtkWidget *entries[4] = {
		box_files1_text_0,
		box_files1_text_1,
		box_files2_text_1,
		box_files2_text_2,
	};

	entry = (GtkWidget *)data;
	strcpy(bname,gtk_entry_get_text(GTK_ENTRY(entry)));
	for (i=strlen(bname);i>0;i--)
	{
		if (bname[i]=='.')
		{
			bname[i]='\0';
			break;
		}
	}
	for (i=0;i<4;i++)
	{
		strcpy(name,gtk_entry_get_text(GTK_ENTRY(entries[i])));
		ptr = name;
		for (j=0;j<strlen(name);j++)
		{
			ptr++;
			if (*ptr=='.')
			{
				ptr++;
				strcpy(ext,ptr);
				strcpy(name,bname);
				strcat(name,".");
				strcat(name,ext);
				break;
			}
		}
		gtk_entry_set_text(GTK_ENTRY(entries[i]),name);
	}
	capture_data();
}

void refresh_displayed_data (void)
{
	int i;
	char txt[300];
	char *models[5]={"dho","lor","gau","del","vis"};
	char *cntint[2]={"cnt","int"};
	/* We define the pointers to the existing check buttons. */
	GtkWidget *chk_but[42] = {
	box_bg_check_0,
	box_sl_check_0,
	box_zr_check_0,
	box_gr_check_0,
	box_pc_check_0,
	box_gc_check_0,
	box_exc1_check_0,
	box_exc1_check_1,
	box_exc1_check_2,
	box_exc2_check_0,
	box_exc2_check_1,
	box_exc2_check_2,
	box_exc3_check_0,
	box_exc3_check_1,
	box_exc3_check_2,
	box_exc4_check_0,
	box_exc4_check_1,
	box_exc4_check_2,
	box_exc5_check_0,
	box_exc5_check_1,
	box_exc5_check_2,
	box_exc6_check_0,
	box_exc6_check_1,
	box_exc6_check_2,
	box_exc7_check_0,
	box_exc7_check_1,
	box_exc7_check_2,
	box_exc8_check_0,
	box_exc8_check_1,
	box_exc8_check_2,
	box_exc9_check_0,
	box_exc9_check_1,
	box_exc9_check_2,
	box_exc10_check_0,
	box_exc10_check_1,
	box_exc10_check_2,
	box_exc11_check_0,
	box_exc11_check_1,
	box_exc11_check_2,
	box_exc12_check_0,
	box_exc12_check_1,
	box_exc12_check_2
	};
	/* We define the pointers to the existing visco check buttons. */
	GtkWidget *visco_chk_but[10] = {
		box_visco_visi_check_0,
		box_visco_omega0_check_0,
		box_visco_gamma_check_0,
		box_visco_taut_check_0,
		box_visco_omegainf_check_0,
		box_visco_tau1_check_0,
		box_visco_gamma0_check_0,
		box_visco_delta_check_0,
		box_visco_taud_check_0,
		box_visco_beta_check_0,
	};

	gtk_entry_set_text(GTK_ENTRY(box_files1_text_0),nomefile);
	gtk_entry_set_text(GTK_ENTRY(box_files1_text_1),nomeout);
	gtk_entry_set_text(GTK_ENTRY(box_res_file_text),nomeris);
	gtk_entry_set_text(GTK_ENTRY(box_files2_text_1),nomedo);
	gtk_entry_set_text(GTK_ENTRY(box_files2_text_2),nomepar);
	sprintf(txt,"%g",temp);
	gtk_entry_set_text(GTK_ENTRY(box_data1_text_0),txt);
	sprintf(txt,"%g",frac);
	gtk_entry_set_text(GTK_ENTRY(box_data1_text_1),txt);
	sprintf(txt,"%s",counts);
	gtk_entry_set_text(GTK_ENTRY(box_data1_text_2),txt);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(box_data2_spin_0),nex);
	for (i=0;i<5;i++)
	{
		if (!strcasecmp(model,models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_data2_list_0),i);
			break;
		}
	}
	for (i=0;i<2;i++)
	{
		if (!strcasecmp(nomenorm,cntint[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_data2_list_1),i);
			break;
		}
	}
	sprintf(txt,"%g",psv_gamma1);
	gtk_entry_set_text(GTK_ENTRY(box_convol_lor_width),txt);
	sprintf(txt,"%g",psv_gamma2);
	gtk_entry_set_text(GTK_ENTRY(box_convol_gau_width),txt);
	sprintf(txt,"%g",psv_eta);
	gtk_entry_set_text(GTK_ENTRY(box_convol_eta),txt);

	gtk_entry_set_text(GTK_ENTRY(text_title),title);
/*	gtk_editable_delete_text(GTK_EDITABLE(text_rest),0,-1);
	gtk_text_insert(GTK_TEXT(text_rest),NULL,NULL,NULL,rest_arg,-1); */

	/* Beginning of the (very dirty) refresh of the main fit data. */
	sprintf(txt,"%g",params[0].a);
	gtk_entry_set_text(GTK_ENTRY(box_bg_text_0),txt);
	sprintf(txt,"%g",params[0].b);
	gtk_entry_set_text(GTK_ENTRY(box_bg_text_1),txt);
	sprintf(txt,"%g",params[0].c);
	gtk_entry_set_text(GTK_ENTRY(box_bg_text_2),txt);
	sprintf(txt,"%g",params[0].d);
	gtk_entry_set_text(GTK_ENTRY(box_bg_text_3),txt);

	sprintf(txt,"%g",params[1].a);
	gtk_entry_set_text(GTK_ENTRY(box_sl_text_0),txt);
	sprintf(txt,"%g",params[1].b);
	gtk_entry_set_text(GTK_ENTRY(box_sl_text_1),txt);
	sprintf(txt,"%g",params[1].c);
	gtk_entry_set_text(GTK_ENTRY(box_sl_text_2),txt);
	sprintf(txt,"%g",params[1].d);
	gtk_entry_set_text(GTK_ENTRY(box_sl_text_3),txt);

	sprintf(txt,"%g",params[2].a);
	gtk_entry_set_text(GTK_ENTRY(box_zr_text_0),txt);
	sprintf(txt,"%g",params[2].b);
	gtk_entry_set_text(GTK_ENTRY(box_zr_text_1),txt);
	sprintf(txt,"%g",params[2].c);
	gtk_entry_set_text(GTK_ENTRY(box_zr_text_2),txt);
	sprintf(txt,"%g",params[2].d);
	gtk_entry_set_text(GTK_ENTRY(box_zr_text_3),txt);

	sprintf(txt,"%g",params[3].a);
	gtk_entry_set_text(GTK_ENTRY(box_gr_text_0),txt);
	sprintf(txt,"%g",params[3].b);
	gtk_entry_set_text(GTK_ENTRY(box_gr_text_1),txt);
	sprintf(txt,"%g",params[3].c);
	gtk_entry_set_text(GTK_ENTRY(box_gr_text_2),txt);
	sprintf(txt,"%g",params[3].d);
	gtk_entry_set_text(GTK_ENTRY(box_gr_text_3),txt);

	sprintf(txt,"%g",params[4].a);
	gtk_entry_set_text(GTK_ENTRY(box_pc_text_0),txt);
	sprintf(txt,"%g",params[4].b);
	gtk_entry_set_text(GTK_ENTRY(box_pc_text_1),txt);
	sprintf(txt,"%g",params[4].c);
	gtk_entry_set_text(GTK_ENTRY(box_pc_text_2),txt);
	sprintf(txt,"%g",params[4].d);
	gtk_entry_set_text(GTK_ENTRY(box_pc_text_3),txt);

	sprintf(txt,"%g",params[5].a);
	gtk_entry_set_text(GTK_ENTRY(box_gc_text_0),txt);
	sprintf(txt,"%g",params[5].b);
	gtk_entry_set_text(GTK_ENTRY(box_gc_text_1),txt);
	sprintf(txt,"%g",params[5].c);
	gtk_entry_set_text(GTK_ENTRY(box_gc_text_2),txt);
	sprintf(txt,"%g",params[5].d);
	gtk_entry_set_text(GTK_ENTRY(box_gc_text_3),txt);

	if (nb_visc > 0)
		gtk_widget_show(box_visco);
	else
		gtk_widget_hide(box_visco);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[0],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc1_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[0].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_0),txt);
	sprintf(txt,"%g",excits[0].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_1),txt);
	sprintf(txt,"%g",excits[0].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_2),txt);
	sprintf(txt,"%g",excits[0].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_3),txt);

	sprintf(txt,"%g",excits[1].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_4),txt);
	sprintf(txt,"%g",excits[1].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_5),txt);
	sprintf(txt,"%g",excits[1].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_6),txt);
	sprintf(txt,"%g",excits[1].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_7),txt);

	sprintf(txt,"%g",excits[2].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_8),txt);
	sprintf(txt,"%g",excits[2].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_9),txt);
	sprintf(txt,"%g",excits[2].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_10),txt);
	sprintf(txt,"%g",excits[2].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc1_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[1],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc2_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[3].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_0),txt);
	sprintf(txt,"%g",excits[3].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_1),txt);
	sprintf(txt,"%g",excits[3].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_2),txt);
	sprintf(txt,"%g",excits[3].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_3),txt);

	sprintf(txt,"%g",excits[4].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_4),txt);
	sprintf(txt,"%g",excits[4].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_5),txt);
	sprintf(txt,"%g",excits[4].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_6),txt);
	sprintf(txt,"%g",excits[4].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_7),txt);

	sprintf(txt,"%g",excits[5].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_8),txt);
	sprintf(txt,"%g",excits[5].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_9),txt);
	sprintf(txt,"%g",excits[5].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_10),txt);
	sprintf(txt,"%g",excits[5].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc2_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[2],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc3_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[6].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_0),txt);
	sprintf(txt,"%g",excits[6].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_1),txt);
	sprintf(txt,"%g",excits[6].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_2),txt);
	sprintf(txt,"%g",excits[6].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_3),txt);

	sprintf(txt,"%g",excits[7].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_4),txt);
	sprintf(txt,"%g",excits[7].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_5),txt);
	sprintf(txt,"%g",excits[7].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_6),txt);
	sprintf(txt,"%g",excits[7].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_7),txt);

	sprintf(txt,"%g",excits[8].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_8),txt);
	sprintf(txt,"%g",excits[8].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_9),txt);
	sprintf(txt,"%g",excits[8].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_10),txt);
	sprintf(txt,"%g",excits[8].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc3_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[3],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc4_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[9].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_0),txt);
	sprintf(txt,"%g",excits[9].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_1),txt);
	sprintf(txt,"%g",excits[9].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_2),txt);
	sprintf(txt,"%g",excits[9].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_3),txt);

	sprintf(txt,"%g",excits[10].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_4),txt);
	sprintf(txt,"%g",excits[10].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_5),txt);
	sprintf(txt,"%g",excits[10].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_6),txt);
	sprintf(txt,"%g",excits[10].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_7),txt);

	sprintf(txt,"%g",excits[11].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_8),txt);
	sprintf(txt,"%g",excits[11].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_9),txt);
	sprintf(txt,"%g",excits[11].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_10),txt);
	sprintf(txt,"%g",excits[11].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc4_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[4],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc5_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[12].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_0),txt);
	sprintf(txt,"%g",excits[12].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_1),txt);
	sprintf(txt,"%g",excits[12].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_2),txt);
	sprintf(txt,"%g",excits[12].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_3),txt);

	sprintf(txt,"%g",excits[13].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_4),txt);
	sprintf(txt,"%g",excits[13].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_5),txt);
	sprintf(txt,"%g",excits[13].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_6),txt);
	sprintf(txt,"%g",excits[13].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_7),txt);

	sprintf(txt,"%g",excits[14].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_8),txt);
	sprintf(txt,"%g",excits[14].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_9),txt);
	sprintf(txt,"%g",excits[14].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_10),txt);
	sprintf(txt,"%g",excits[14].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc5_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[5],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc6_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[15].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_0),txt);
	sprintf(txt,"%g",excits[15].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_1),txt);
	sprintf(txt,"%g",excits[15].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_2),txt);
	sprintf(txt,"%g",excits[15].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_3),txt);

	sprintf(txt,"%g",excits[16].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_4),txt);
	sprintf(txt,"%g",excits[16].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_5),txt);
	sprintf(txt,"%g",excits[16].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_6),txt);
	sprintf(txt,"%g",excits[16].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_7),txt);

	sprintf(txt,"%g",excits[17].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_8),txt);
	sprintf(txt,"%g",excits[17].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_9),txt);
	sprintf(txt,"%g",excits[17].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_10),txt);
	sprintf(txt,"%g",excits[17].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc6_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[6],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc7_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[18].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_0),txt);
	sprintf(txt,"%g",excits[18].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_1),txt);
	sprintf(txt,"%g",excits[18].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_2),txt);
	sprintf(txt,"%g",excits[18].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_3),txt);

	sprintf(txt,"%g",excits[19].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_4),txt);
	sprintf(txt,"%g",excits[19].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_5),txt);
	sprintf(txt,"%g",excits[19].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_6),txt);
	sprintf(txt,"%g",excits[19].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_7),txt);

	sprintf(txt,"%g",excits[20].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_8),txt);
	sprintf(txt,"%g",excits[20].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_9),txt);
	sprintf(txt,"%g",excits[20].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_10),txt);
	sprintf(txt,"%g",excits[20].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc7_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[7],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc8_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[21].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_0),txt);
	sprintf(txt,"%g",excits[21].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_1),txt);
	sprintf(txt,"%g",excits[21].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_2),txt);
	sprintf(txt,"%g",excits[21].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_3),txt);

	sprintf(txt,"%g",excits[22].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_4),txt);
	sprintf(txt,"%g",excits[22].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_5),txt);
	sprintf(txt,"%g",excits[22].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_6),txt);
	sprintf(txt,"%g",excits[22].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_7),txt);

	sprintf(txt,"%g",excits[23].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_8),txt);
	sprintf(txt,"%g",excits[23].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_9),txt);
	sprintf(txt,"%g",excits[23].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_10),txt);
	sprintf(txt,"%g",excits[23].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc8_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[8],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc9_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[24].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_0),txt);
	sprintf(txt,"%g",excits[24].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_1),txt);
	sprintf(txt,"%g",excits[24].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_2),txt);
	sprintf(txt,"%g",excits[24].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_3),txt);

	sprintf(txt,"%g",excits[25].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_4),txt);
	sprintf(txt,"%g",excits[25].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_5),txt);
	sprintf(txt,"%g",excits[25].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_6),txt);
	sprintf(txt,"%g",excits[25].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_7),txt);

	sprintf(txt,"%g",excits[26].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_8),txt);
	sprintf(txt,"%g",excits[26].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_9),txt);
	sprintf(txt,"%g",excits[26].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_10),txt);
	sprintf(txt,"%g",excits[26].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc9_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[9],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc10_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[27].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_0),txt);
	sprintf(txt,"%g",excits[27].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_1),txt);
	sprintf(txt,"%g",excits[27].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_2),txt);
	sprintf(txt,"%g",excits[27].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_3),txt);

	sprintf(txt,"%g",excits[28].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_4),txt);
	sprintf(txt,"%g",excits[28].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_5),txt);
	sprintf(txt,"%g",excits[28].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_6),txt);
	sprintf(txt,"%g",excits[28].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_7),txt);

	sprintf(txt,"%g",excits[29].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_8),txt);
	sprintf(txt,"%g",excits[29].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_9),txt);
	sprintf(txt,"%g",excits[29].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_10),txt);
	sprintf(txt,"%g",excits[29].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc10_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[10],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc11_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[28].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_0),txt);
	sprintf(txt,"%g",excits[28].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_1),txt);
	sprintf(txt,"%g",excits[28].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_2),txt);
	sprintf(txt,"%g",excits[28].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_3),txt);

	sprintf(txt,"%g",excits[29].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_4),txt);
	sprintf(txt,"%g",excits[29].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_5),txt);
	sprintf(txt,"%g",excits[29].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_6),txt);
	sprintf(txt,"%g",excits[29].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_7),txt);

	sprintf(txt,"%g",excits[30].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_8),txt);
	sprintf(txt,"%g",excits[30].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_9),txt);
	sprintf(txt,"%g",excits[30].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_10),txt);
	sprintf(txt,"%g",excits[30].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc11_text_11),txt);

	for (i=0;i<4;i++)
	{
		if (!strcmp(ex[11],models[i])) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc12_model),i);
			break;
		}
	}
	sprintf(txt,"%g",excits[31].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_0),txt);
	sprintf(txt,"%g",excits[31].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_1),txt);
	sprintf(txt,"%g",excits[31].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_2),txt);
	sprintf(txt,"%g",excits[31].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_3),txt);

	sprintf(txt,"%g",excits[32].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_4),txt);
	sprintf(txt,"%g",excits[32].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_5),txt);
	sprintf(txt,"%g",excits[32].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_6),txt);
	sprintf(txt,"%g",excits[32].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_7),txt);

	sprintf(txt,"%g",excits[33].a);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_8),txt);
	sprintf(txt,"%g",excits[33].b);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_9),txt);
	sprintf(txt,"%g",excits[33].c);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_10),txt);
	sprintf(txt,"%g",excits[33].d);
	gtk_entry_set_text(GTK_ENTRY(box_exc12_text_11),txt);

	/* Refreshing the state of the check buttons. */
	for (i=0;i<42;i++)
	{
		if (fixed[i])
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(chk_but[i]),TRUE);
		else
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(chk_but[i]),FALSE);
	}

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc1_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc2_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc3_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc4_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc5_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc6_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc7_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc8_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc9_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc10_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc11_swp_toggle),FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc12_swp_toggle),FALSE);
	for (i=0;i<2;i++)
	{
		switch (swap[i]) {
			case 1:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc1_swp_toggle),TRUE);
				break;
			case 2:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc2_swp_toggle),TRUE);
				break;
			case 3:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc3_swp_toggle),TRUE);
				break;
			case 4:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc4_swp_toggle),TRUE);
				break;
			case 5:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc5_swp_toggle),TRUE);
				break;
			case 6:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc6_swp_toggle),TRUE);
				break;
			case 7:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc7_swp_toggle),TRUE);
				break;
			case 8:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc8_swp_toggle),TRUE);
				break;
			case 9:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc9_swp_toggle),TRUE);
				break;
			case 10:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc10_swp_toggle),TRUE);
				break;
			case 11:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc11_swp_toggle),TRUE);
				break;
			case 12:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exc12_swp_toggle),TRUE);
				break;
			default:
				break;
		}
	}

	/* Viscoelasticity parameters */
	if (nb_visc)
	{
		sprintf(txt,"%g",viscs[0].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_visi_text_0),txt);
		sprintf(txt,"%g",viscs[0].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_visi_text_1),txt);
		sprintf(txt,"%g",viscs[0].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_visi_text_2),txt);
		sprintf(txt,"%g",viscs[0].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_visi_text_3),txt);

		sprintf(txt,"%g",viscs[1].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omega0_text_0),txt);
		sprintf(txt,"%g",viscs[1].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omega0_text_1),txt);
		sprintf(txt,"%g",viscs[1].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omega0_text_2),txt);
		sprintf(txt,"%g",viscs[1].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omega0_text_3),txt);

		sprintf(txt,"%g",viscs[2].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma_text_0),txt);
		sprintf(txt,"%g",viscs[2].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma_text_1),txt);
		sprintf(txt,"%g",viscs[2].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma_text_2),txt);
		sprintf(txt,"%g",viscs[2].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma_text_3),txt);

		sprintf(txt,"%g",viscs[3].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taut_text_0),txt);
		sprintf(txt,"%g",viscs[3].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taut_text_1),txt);
		sprintf(txt,"%g",viscs[3].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taut_text_2),txt);
		sprintf(txt,"%g",viscs[3].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taut_text_3),txt);

		sprintf(txt,"%g",viscs[4].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omegainf_text_0),txt);
		sprintf(txt,"%g",viscs[4].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omegainf_text_1),txt);
		sprintf(txt,"%g",viscs[4].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omegainf_text_2),txt);
		sprintf(txt,"%g",viscs[4].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_omegainf_text_3),txt);

		sprintf(txt,"%g",viscs[5].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_tau1_text_0),txt);
		sprintf(txt,"%g",viscs[5].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_tau1_text_1),txt);
		sprintf(txt,"%g",viscs[5].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_tau1_text_2),txt);
		sprintf(txt,"%g",viscs[5].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_tau1_text_3),txt);

		sprintf(txt,"%g",viscs[6].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma0_text_0),txt);
		sprintf(txt,"%g",viscs[6].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma0_text_1),txt);
		sprintf(txt,"%g",viscs[6].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma0_text_2),txt);
		sprintf(txt,"%g",viscs[6].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_gamma0_text_3),txt);

		sprintf(txt,"%g",viscs[7].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_delta_text_0),txt);
		sprintf(txt,"%g",viscs[7].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_delta_text_1),txt);
		sprintf(txt,"%g",viscs[7].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_delta_text_2),txt);
		sprintf(txt,"%g",viscs[7].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_delta_text_3),txt);

		sprintf(txt,"%g",viscs[8].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taud_text_0),txt);
		sprintf(txt,"%g",viscs[8].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taud_text_1),txt);
		sprintf(txt,"%g",viscs[8].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taud_text_2),txt);
		sprintf(txt,"%g",viscs[8].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_taud_text_3),txt);

		sprintf(txt,"%g",viscs[9].a);
		gtk_entry_set_text(GTK_ENTRY(box_visco_beta_text_0),txt);
		sprintf(txt,"%g",viscs[9].b);
		gtk_entry_set_text(GTK_ENTRY(box_visco_beta_text_1),txt);
		sprintf(txt,"%g",viscs[9].c);
		gtk_entry_set_text(GTK_ENTRY(box_visco_beta_text_2),txt);
		sprintf(txt,"%g",viscs[9].d);
		gtk_entry_set_text(GTK_ENTRY(box_visco_beta_text_3),txt);

		for (i=0;i<10;i++)
		{
			if (visco_fixed[i])
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(visco_chk_but[i]),TRUE);
			else
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(visco_chk_but[i]),FALSE);
		}
	}

	refresh_t_as_param_mode();
}

/* Reads the .inp file */
void read_input_file (char *fname)
{
	char dum[300];
	int n;
	char par[5];
	char tmpstr[100];
	double p1,p2,p3,p4;
	FILE *fil;

	fil = fopen(fname,"r");
	if (!fil)
	{
		print_info("Error: wrong input file [%s]",fname);
		return;
	}
	/* First line */
	fgets(tmpstr,BUFSIZ,fil);
        if (!strcmp(tmpstr,"# Fit28 input file\n"))
	{
		fl_t_as_param = 1;
		fgets(tmpstr,BUFSIZ,fil);
	}
	else
	{
		fl_t_as_param = 0;
	}

	sscanf(tmpstr,"%s",nomefile);
	strcpy(last_data_file,nomefile);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",nomeout);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",nomeris);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",nomenorm);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",model);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",nomedo);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",nomepar);
	if (!fl_t_as_param)
	{
		fgets(tmpstr,BUFSIZ,fil);
		sscanf(tmpstr,"%lf",&temp);
	}
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%lf",&frac);
	fgets(title,BUFSIZ,fil); /* Reading title directly */
	title[strlen(title)-1] = '\0';
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%s",counts); /* Beware: counts is a string */
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%d",&nex);
	if (nex >= 12)
	{
		print_info("Error: too many excitations","");
		return;
	}
	for (i=0;i<nex;i++)
	{
		fgets(tmpstr,BUFSIZ,fil);
		sscanf(tmpstr,"%s",ex[i]);
	}
	ntotpar=3*nex+6;
	nb_visc = 0;
	if (!strcmp(model,"vis"))
		nb_visc = 1;
	/* Reading fit parameters */
	while (fgets(tmpstr,BUFSIZ,fil))
	{
		if (!strncasecmp(tmpstr,"MINUIT",6))
			break;
	}
	for (i=0;i<ntotpar;i++)
	{
		p1=p2=p3=p4=0;
		fgets(tmpstr,BUFSIZ,fil);
		sscanf(tmpstr,"%d %s %lf %lf %lf %lf",&n,par,&p1,&p2,&p3,&p4);
		if (n<1)
		{
			print_info("Error: wrong parameter number.","");
			continue;
		}
		if (i<6)
		{
			params[i].a = p1;
			params[i].b = p2;
			params[i].c = p3;
			params[i].d = p4;
		}
		else if (nb_visc==0)
		{
			excits[i-6].a = p1;
			excits[i-6].b = p2;
			excits[i-6].c = p3;
			excits[i-6].d = p4;
		}
	}
	if (nb_visc)
	{
		for (i=0;i<10;i++)
		{
			p1=p2=p3=p4=0;
			fgets(tmpstr,BUFSIZ,fil);
			if (sscanf(tmpstr,"%d %s %lf %lf %lf %lf",&n,par,&p1,&p2,&p3,&p4)<6)
			{
				print_info("Warning: wrong visco input line.","");
				continue;
			}
			if (n<1)
			{
				print_info("Error: wrong parameter number.","");
				return;
			}
			viscs[i].a = p1;
			viscs[i].b = p2;
			viscs[i].c = p3;
			viscs[i].d = p4;
		}	
	}
	/* Initializing initial changes to parameters (0 = fixed) */
	for (i=0;i<ntotpar;i++)
		delta[i] = 0.1;
	/* Reading the remaining lines... */
/*	strcpy(rest_arg,""); */
	while (fgets(tmpstr,BUFSIZ,fil))
	{
		if (strlen(tmpstr)<=1)
			continue;
		if (!strncasecmp(tmpstr,"FIX",3))
		{
			sscanf(tmpstr,"%s %d",dum,&n);
			if (n<=ntotpar)
			{
				fixed[n-1] = 1;
			}
			else if (nb_visc)
			{
				visco_fixed[n-ntotpar-1] = 1;
			}
			else
			{
				print_info("Error: wrong FIX number.","");
			}
			continue;
		}
/*		strcat(rest_arg,tmpstr); */
	}
	fclose(fil);
	clear_swap();
	refresh_displayed_data();
	gtk_label_set_text(GTK_LABEL(inp_label),g_basename(fname));
}

void write_file (char *fname)
{
	int i,j;
	char txt[1000];
	FILE *fil;

	if (!fname)
		return;
	fil = fopen(fname,"w");
	if (!fil)
	{
		print_info("Error: cannot write file [%s]",fname);
		return;
	}
	capture_data();

	if (fl_t_as_param)
	{
		fputs("# Fit28 input file\n",fil);
	}
	sprintf(txt,"%-20s ! data input file\n",nomefile);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! data output file\n",nomeout);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! resolution file\n",nomeris);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! cnt (c/mon*avmon) or int (c/mon)\n",nomenorm);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! fitting model\n",model);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! file do for c-plotting\n",nomedo);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! fitted parameters file\n",nomepar);
	fputs(txt,fil);
	if (!fl_t_as_param)
	{
		sprintf(txt,"%-20f ! Temperature (real)\n",temp);
		fputs(txt,fil);
	}
	sprintf(txt,"%-20f ! 1/fraction of step used for convolution (higher=slower)\n",frac);
	fputs(txt,fil);
	sprintf(txt,"%s\n",title);
	fputs(txt,fil);
	sprintf(txt,"%-20s ! Integration time\n",counts);
	fputs(txt,fil);
	sprintf(txt,"%-20d ! Number of excitations (MIN 1;MAX 12)\n",nex);
	fputs(txt,fil);
	for (i=0;i<nex;i++)
	{
		if (!is_valid_model(ex[i]))
			strcpy(ex[i],"dho");
		sprintf(txt,"%-20s ! excitation %d\n",ex[i],i+1);
		fputs(txt,fil);
	}
	fputs("MINUIT :\n",fil);
	for (i=0;i<6;i++)
	{
		sprintf(txt,"%10d  %-10s%-10g%-10g%-10g%-10g\n",
			i+1,par_nm[i],params[i].a,params[i].b,params[i].c,params[i].d);
		fputs(txt,fil);
	}
	for (i=0;i<nex;i++)
	{
		j = i*3;
		sprintf(txt,"%10d  P%-9d%-10g%-10g%-10g%-10g\n",
			j+7,i+1,excits[j].a,excits[j].b,excits[j].c,excits[j].d);
		fputs(txt,fil);
		sprintf(txt,"%10d  O%-9d%-10g%-10g%-10g%-10g\n",
			j+8,i+1,excits[j+1].a,excits[j+1].b,excits[j+1].c,excits[j+1].d);
		fputs(txt,fil);
		sprintf(txt,"%10d  G%-9d%-10g%-10g%-10g%-10g\n",
			j+9,i+1,excits[j+2].a,excits[j+2].b,excits[j+2].c,excits[j+2].d);
		fputs(txt,fil);
	}
	if (nb_visc)
	{
		for (i=0;i<10;i++)
		{
			j = i+(nex*3)+7;
			sprintf(txt,"%10d  %-10s%-10g%-10g%-10g%-10g\n",
				j,visc_labels[i],viscs[i].a,viscs[i].b,viscs[i].c,viscs[i].d);
			fputs(txt,fil);
		}
	}
	fputs("\n",fil);
	for (i=0;i<42;i++)
	{
		if (fixed[i])
		{
			sprintf(txt,"FIX       %d\n",i+1);
			fputs(txt,fil);
		}
	}
	if (nb_visc)
	{
		for (i=0;i<10;i++)
		{
			j = i+(nex*3)+7;
			if (visco_fixed[i])
			{
				sprintf(txt,"FIX       %d\n",j);
				fputs(txt,fil);
			}
		}
	}
	fclose(fil);
}

void write_fit_param_file (void)
{
	int i,j;
	char txt[1000];
	FILE *fil;

	fil = fopen(nomepar,"w");
	if (!fil)
		print_info("Error: wrong parameters file [%s]",nomepar);

	sprintf(txt,"%s T=%dK (in %s)\n",title,(int)temp,nomeout);
	fputs(txt,fil);
	sprintf(txt," -model = %s\n",model);
	fputs(txt,fil);
	sprintf(txt," -resolution = %s\n",nomeris);
	fputs(txt,fil);
	sprintf(txt," -average monitor = %.1f\n",avmon);
	fputs(txt,fil);
	sprintf(txt," -chi2 = %.1f\n",real_chi2);
	fputs(txt,fil);
	fputs("\n",fil);
	sprintf(txt," -BG = %g +/- %g\n",params[0].a,err_par[0]);
	fputs(txt,fil);
	sprintf(txt," -SL = %g +/- %g\n",params[1].a,err_par[1]);
	fputs(txt,fil);
	sprintf(txt," -ZR = %g +/- %g\n",params[2].a,err_par[2]);
	fputs(txt,fil);
	if (fl_t_as_param)
	{
		sprintf(txt," -T = %g +/- %g\n",params[3].a,err_par[3]);
	}
	else
	{
		sprintf(txt," -GR = %g +/- %g\n",params[3].a,err_par[3]);
	}
	fputs(txt,fil);
	sprintf(txt," -PC = %g +/- %g\n",params[4].a,err_par[4]);
	fputs(txt,fil);
	sprintf(txt," -GC = %g +/- %g\n",params[5].a,err_par[5]);
	fputs(txt,fil);
	sprintf(txt," -area = %g +/- %g  (%g total)\n",sumcen,sumcen_err,sumcen/sumall);
	fputs(txt,fil);
	for (i=0;i<nex;i++)
	{
		fputs("\n",fil);
		sprintf(txt,"** Excitation %d **********************************\n",i+1);
		fputs(txt,fil);
		j = 6+(3*i);
		sprintf(txt," -P = %g +/- %g\n",excits[j-6].a,err_par[j]);
		fputs(txt,fil);
		j++;
		sprintf(txt," -O = %g +/- %g\n",excits[j-6].a,err_par[j]);
		fputs(txt,fil);
		j++;
		sprintf(txt," -G = %g +/- %g\n",excits[j-6].a,err_par[j]);
		fputs(txt,fil);
		sprintf(txt," -area = %g +/- %g  (%g total)\n",sumexc[i],sumexc_err[i],sumexc[i]/sumall);
		fputs(txt,fil);
	}
	if (nb_visc)
	{
		fputs("\n",fil);
		fputs("** Visco **********************************\n",fil);
		for (i=0;i<10;i++)
		{
			j = 6+(3*nex)+i;
			sprintf(txt," -%s = %g +/- %g\n",visc_labels[i],viscs[i].a,err_par[j]);
			fputs(txt,fil);
		}
		if (fl_visco_e2)
		{
			sprintf(txt," Sum of f(E).EE = %g\n",sume2);
			fputs(txt,fil);
			sprintf(txt," Mean of f(E).EE max = %g\n",meane2);
			fputs(txt,fil);
		}
	}
	fclose(fil);
}

void write_fit_curves (void)
{
	int i,j;
	char txt[1000],tmp[100],filename[300];
	FILE *fil;

	if (fl_preview)
	{
		strcpy(filename,getenv("HOME"));
		strcat(filename,"/.fit28/");
		strcat(filename,"preview.fit");
	}
	else
	{
		strcpy(filename,nomeout);
	}
	fil = fopen(filename,"w");
	if (!fil)
		print_info("Error: wrong fit file [%s]",nomeout);

	if (plot_prgm == 0) /* C-plot */
	{
		for (i=0;i<nn;i++)
		{
			sprintf(txt,"%f %f %f %f %f",frq[i],dat[i],erd[i],tcn[i],tcnc[i]);
			for (j=0;j<nex;j++)
			{
				sprintf(tmp," %f",tcne[j][i]);
				strcat(txt,tmp);
			}
			strcat(txt,"\n");
			fputs(txt,fil);
		}
	}
	else if (plot_prgm == 1) /* Gnuplot */
	{
		for (i=0;i<nn;i++)
		{
			sprintf(txt,"%f %f %f %f %f",frq[i],dat[i],erd[i],tcn[i],tcnc[i]);
			for (j=0;j<nex;j++)
			{
				sprintf(tmp," %f",tcne[j][i]);
				strcat(txt,tmp);
			}
			if (nb_visc)
			{
				sprintf(tmp," %f",tcnv[i]);
				strcat(txt,tmp);
				if (fl_visco_e2)
				{
					sprintf(tmp," %f",tcne2[i]);
					strcat(txt,tmp);
				}
			}
			strcat(txt,"\n");
			fputs(txt,fil);
		}

	}
	fclose(fil);
}

void write_do_file (void)
{
	int j;
	char txt[1000],outname[300],filename[300];
	FILE *fil;

	if (fl_preview)
	{
		strcpy(filename,getenv("HOME"));
		strcat(filename,"/.fit28/");
		strcat(filename,"preview.do");
		fil = fopen(filename,"w");
		strcpy(outname,getenv("HOME"));
		strcat(outname,"/.fit28/");
		strcat(outname,"preview.fit");
	}
	else
	{
		fil = fopen(nomedo,"w");
		strcpy(outname,nomeout);
	}
	if (!fil)
		print_info("Error: wrong do file [%s]",nomedo);

	if (plot_prgm == 0) /* C-plot */
	{
		fputs("re\ntx\n",fil);
		sprintf(txt,"%s T= %g K %s %s\n",title,temp,model,outname);
		fputs(txt,fil);
		fputs("Energy\nmev\nIntensity\n",fil);
		sprintf(txt,"Counts/%ss\n",counts);
		fputs(txt,fil);
		fputs(" \n",fil);
		sprintf(txt,"gd 3 %s\n",outname);
		fputs(txt,fil);
		fputs("1\n2\n3\n \nnp\neb\nft 3\ncs l 5\ncs n 5\nwi 12 10\n \ncs s 2\n",fil);
		fputs("sy 0\n \nra\n \n \n \n \n",fil);
		sprintf(txt,"zi psfilter @lpr @-P%s\n",printer);
		fputs(txt,fil);
		fputs("zi x11\nzztapldw\n \n",fil);
		sprintf(txt,"gd 3 %s\n",outname);
		fputs(txt,fil);
		fputs("1\n4\n0\n \np3\nsy L\nzp\n \np1\n",fil);
		sprintf(txt,"gd 3 %s\n",outname);
		fputs(txt,fil);
		fputs("1\n5\n0\n \nsy L\nzp\n \n",fil);
		for (j=0;j<nex;j++)
		{
			sprintf(txt,"gd 3 %s\n",outname);
			fputs(txt,fil);
			sprintf(txt,"1\n%d\n0\n \nsy L\nzp\n \n",j+6);
			fputs(txt,fil);
		}
		fputs(" \ncs k 2\npk 0.5 0.5\ngk\n",fil);
		sprintf(txt,"-model= %s\n-resol= %s\n-chi2= %.1f\n",model,nomeris,chi2);
		fputs(txt,fil);
		sprintf(txt,"-bg= %g +/- %g\n",params[0].a,err_par[0]);
		fputs(txt,fil);
		sprintf(txt,"-sl= %g +/- %g\n",params[1].a,err_par[1]);
		fputs(txt,fil);
		sprintf(txt,"-zr= %g +/- %g\n",params[2].a,err_par[2]);
		fputs(txt,fil);
		sprintf(txt,"-gr= %g +/- %g\n",params[3].a,err_par[3]);
		fputs(txt,fil);
		sprintf(txt,"-pc= %g +/- %g\n",params[4].a,err_par[4]);
		fputs(txt,fil);
		sprintf(txt,"-gc= %g +/- %g\n",params[5].a,err_par[5]);
		fputs(txt,fil);
		fputs("^D\nzpk\nzs\n",fil);
	}
	else if (plot_prgm == 1) /* Gnuplot */
	{
		sprintf(txt,"set title \"%s\"\n",title);
		fputs(txt,fil);
		sprintf(txt,"set xlabel \"Energy (meV)\"\n");
		fputs(txt,fil);
		sprintf(txt,"set ylabel \"Intensity (counts/%ss)\"\n",counts);
		fputs(txt,fil);
		sprintf(txt,"plot \"%s\" using 1:2:3 title 'Data' with yerrorbars lt -1 pt 3, \\\n",outname);
		fputs(txt,fil);
		sprintf(txt,"     \"%s\" using 1:4 title 'Fit' with lines lt 1 lw 2, \\\n",outname);
		fputs(txt,fil);
		sprintf(txt,"     \"%s\" using 1:5 title 'Central line' with lines lt 2",outname);
		fputs(txt,fil);
		if (nex>0)
		{
			fputs(", \\\n",fil);
			for (j=0;j<nex;j++)
			{
				sprintf(txt,"     \"%s\" using 1:%d title 'Exc %d' with lines lt 3",outname,6+j,j+1);
				if (j<(nex-1))
					strcat(txt,", \\\n");
				fputs(txt,fil);
			}
		}
		if (nb_visc)
		{
			if (nex == 0)
				fputs(", \\\n",fil);
			sprintf(txt,"     \"%s\" using 1:%d title 'Visc. model' with lines lt 7",outname,6+nex);
			fputs(txt,fil);
			if (fl_visco_e2)
			{
				fputs(", \\\n",fil);
				sprintf(txt,"     \"%s\" using 1:%d title 'f(E).EE' with lines lt 4",nomeout,7+nex);
				fputs(txt,fil);
			}
		}
		fputs("\n",fil);
	}

	fclose(fil);
}

void check_info_box (void)
{
	struct timeb this_timeb;
	float diff;

	ftime(&this_timeb);
	diff = this_timeb.time-info_timeb.time;
	if (diff >= 10)
	{
		gtk_widget_hide(GTK_WIDGET(main_info_box));
	}
	if (diff >= 60)
	{
		GtkTextBuffer *buffer;
		GtkTextIter iter,iter2;
		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info_text));
		gtk_text_buffer_get_start_iter(buffer,&iter);
		gtk_text_buffer_get_end_iter(buffer,&iter2);
		gtk_text_buffer_delete(buffer,&iter,&iter2);
	}
	g_source_remove(check_info_id);
	check_info_id = g_timeout_add(5*1000,(GtkFunction)check_info_box,NULL);
	return FALSE;
}

void print_info (char *text, char *arg)
{
	gint nb;
	char tmp[BUFSIZ],tmp2[300];
	GtkTextBuffer *buffer;
	GtkTextIter iter,iter2;

	strcpy(tmp,"");
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info_text));
	while (gtk_text_buffer_get_line_count(buffer)>=5)
	{
		gtk_text_buffer_get_iter_at_line_index(buffer,&iter,0,0);
		gtk_text_buffer_get_iter_at_line_index(buffer,&iter2,1,0);
		gtk_text_buffer_delete(buffer,&iter,&iter2);
	}
	gtk_text_buffer_get_end_iter(buffer,&iter);
	if (gtk_text_iter_get_offset(&iter)!=0)
	{
		strcat(tmp,"\n");
	}
	sprintf(tmp2,text,arg);
	strcat(tmp,tmp2);
/*	gtk_text_buffer_set_text(buffer,tmp,-1); */
	gtk_text_buffer_insert(buffer,&iter,tmp,-1);
	printf("%s\n",tmp);
	ftime(&info_timeb);
	gtk_widget_show(GTK_WIDGET(main_info_box));
}

void capture_data (void)
{
	strcpy(nomefile,gtk_entry_get_text(GTK_ENTRY(box_files1_text_0)));
	strcpy(nomeout,gtk_entry_get_text(GTK_ENTRY(box_files1_text_1)));
	strcpy(nomeris,gtk_entry_get_text(GTK_ENTRY(box_res_file_text)));
	strcpy(nomedo,gtk_entry_get_text(GTK_ENTRY(box_files2_text_1)));
	strcpy(nomepar,gtk_entry_get_text(GTK_ENTRY(box_files2_text_2)));
	temp = atof(gtk_entry_get_text(GTK_ENTRY(box_data1_text_0)));
	frac = atof(gtk_entry_get_text(GTK_ENTRY(box_data1_text_1)));
	strcpy(counts,gtk_entry_get_text(GTK_ENTRY(box_data1_text_2)));
	psv_gamma1 = atof(gtk_entry_get_text(GTK_ENTRY(box_convol_lor_width)));
	psv_gamma2 = atof(gtk_entry_get_text(GTK_ENTRY(box_convol_gau_width)));
	psv_eta = atof(gtk_entry_get_text(GTK_ENTRY(box_convol_eta)));
	nex = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(box_data2_spin_0));
	strcpy(title,gtk_entry_get_text(GTK_ENTRY(text_title)));
/*	strcpy(rest_arg,gtk_editable_get_chars(GTK_EDITABLE(text_rest),0,-1)); */
	if (fl_live)
	{
		live_interval = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(live_spin));
	}

	/* Get parameters from GTK text entries. */
	params[0].a = atof(gtk_entry_get_text(GTK_ENTRY(box_bg_text_0)));
	params[0].b = atof(gtk_entry_get_text(GTK_ENTRY(box_bg_text_1)));
	params[0].c = atof(gtk_entry_get_text(GTK_ENTRY(box_bg_text_2)));
	params[0].d = atof(gtk_entry_get_text(GTK_ENTRY(box_bg_text_3)));
	params[1].a = atof(gtk_entry_get_text(GTK_ENTRY(box_sl_text_0)));
	params[1].b = atof(gtk_entry_get_text(GTK_ENTRY(box_sl_text_1)));
	params[1].c = atof(gtk_entry_get_text(GTK_ENTRY(box_sl_text_2)));
	params[1].d = atof(gtk_entry_get_text(GTK_ENTRY(box_sl_text_3)));
	params[2].a = atof(gtk_entry_get_text(GTK_ENTRY(box_zr_text_0)));
	params[2].b = atof(gtk_entry_get_text(GTK_ENTRY(box_zr_text_1)));
	params[2].c = atof(gtk_entry_get_text(GTK_ENTRY(box_zr_text_2)));
	params[2].d = atof(gtk_entry_get_text(GTK_ENTRY(box_zr_text_3)));
	params[3].a = atof(gtk_entry_get_text(GTK_ENTRY(box_gr_text_0)));
	params[3].b = atof(gtk_entry_get_text(GTK_ENTRY(box_gr_text_1)));
	params[3].c = atof(gtk_entry_get_text(GTK_ENTRY(box_gr_text_2)));
	params[3].d = atof(gtk_entry_get_text(GTK_ENTRY(box_gr_text_3)));
	params[4].a = atof(gtk_entry_get_text(GTK_ENTRY(box_pc_text_0)));
	params[4].b = atof(gtk_entry_get_text(GTK_ENTRY(box_pc_text_1)));
	params[4].c = atof(gtk_entry_get_text(GTK_ENTRY(box_pc_text_2)));
	params[4].d = atof(gtk_entry_get_text(GTK_ENTRY(box_pc_text_3)));
	params[5].a = atof(gtk_entry_get_text(GTK_ENTRY(box_gc_text_0)));
	params[5].b = atof(gtk_entry_get_text(GTK_ENTRY(box_gc_text_1)));
	params[5].c = atof(gtk_entry_get_text(GTK_ENTRY(box_gc_text_2)));
	params[5].d = atof(gtk_entry_get_text(GTK_ENTRY(box_gc_text_3)));

	/* Get excitations from GTK text entries. */
	excits[0].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_0)));
	excits[0].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_1)));
	excits[0].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_2)));
	excits[0].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_3)));
	excits[1].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_4)));
	excits[1].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_5)));
	excits[1].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_6)));
	excits[1].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_7)));
	excits[2].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_8)));
	excits[2].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_9)));
	excits[2].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_10)));
	excits[2].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc1_text_11)));

	excits[3].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_0)));
	excits[3].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_1)));
	excits[3].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_2)));
	excits[3].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_3)));
	excits[4].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_4)));
	excits[4].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_5)));
	excits[4].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_6)));
	excits[4].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_7)));
	excits[5].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_8)));
	excits[5].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_9)));
	excits[5].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_10)));
	excits[5].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc2_text_11)));

	excits[6].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_0)));
	excits[6].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_1)));
	excits[6].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_2)));
	excits[6].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_3)));
	excits[7].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_4)));
	excits[7].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_5)));
	excits[7].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_6)));
	excits[7].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_7)));
	excits[8].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_8)));
	excits[8].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_9)));
	excits[8].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_10)));
	excits[8].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc3_text_11)));

	excits[9].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_0)));
	excits[9].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_1)));
	excits[9].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_2)));
	excits[9].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_3)));
	excits[10].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_4)));
	excits[10].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_5)));
	excits[10].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_6)));
	excits[10].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_7)));
	excits[11].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_8)));
	excits[11].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_9)));
	excits[11].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_10)));
	excits[11].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc4_text_11)));

	excits[12].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_0)));
	excits[12].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_1)));
	excits[12].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_2)));
	excits[12].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_3)));
	excits[13].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_4)));
	excits[13].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_5)));
	excits[13].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_6)));
	excits[13].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_7)));
	excits[14].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_8)));
	excits[14].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_9)));
	excits[14].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_10)));
	excits[14].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc5_text_11)));

	excits[15].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_0)));
	excits[15].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_1)));
	excits[15].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_2)));
	excits[15].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_3)));
	excits[16].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_4)));
	excits[16].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_5)));
	excits[16].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_6)));
	excits[16].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_7)));
	excits[17].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_8)));
	excits[17].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_9)));
	excits[17].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_10)));
	excits[17].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc6_text_11)));

	excits[18].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_0)));
	excits[18].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_1)));
	excits[18].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_2)));
	excits[18].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_3)));
	excits[19].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_4)));
	excits[19].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_5)));
	excits[19].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_6)));
	excits[19].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_7)));
	excits[20].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_8)));
	excits[20].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_9)));
	excits[20].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_10)));
	excits[20].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc7_text_11)));

	excits[21].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_0)));
	excits[21].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_1)));
	excits[21].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_2)));
	excits[21].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_3)));
	excits[22].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_4)));
	excits[22].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_5)));
	excits[22].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_6)));
	excits[22].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_7)));
	excits[23].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_8)));
	excits[23].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_9)));
	excits[23].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_10)));
	excits[23].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc8_text_11)));

	excits[24].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_0)));
	excits[24].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_1)));
	excits[24].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_2)));
	excits[24].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_3)));
	excits[25].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_4)));
	excits[25].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_5)));
	excits[25].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_6)));
	excits[25].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_7)));
	excits[26].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_8)));
	excits[26].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_9)));
	excits[26].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_10)));
	excits[26].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc9_text_11)));

	excits[27].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_0)));
	excits[27].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_1)));
	excits[27].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_2)));
	excits[27].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_3)));
	excits[28].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_4)));
	excits[28].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_5)));
	excits[28].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_6)));
	excits[28].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_7)));
	excits[29].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_8)));
	excits[29].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_9)));
	excits[29].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_10)));
	excits[29].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc10_text_11)));

	excits[30].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_0)));
	excits[30].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_1)));
	excits[30].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_2)));
	excits[30].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_3)));
	excits[31].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_4)));
	excits[31].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_5)));
	excits[31].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_6)));
	excits[31].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_7)));
	excits[32].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_8)));
	excits[32].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_9)));
	excits[32].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_10)));
	excits[32].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc11_text_11)));

	excits[33].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_0)));
	excits[33].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_1)));
	excits[33].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_2)));
	excits[33].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_3)));
	excits[34].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_4)));
	excits[34].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_5)));
	excits[34].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_6)));
	excits[34].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_7)));
	excits[35].a = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_8)));
	excits[35].b = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_9)));
	excits[35].c = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_10)));
	excits[35].d = atof(gtk_entry_get_text(GTK_ENTRY(box_exc12_text_11)));

	/* Get viscosity from GTK text entries. */
	viscs[0].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_visi_text_0)));
	viscs[0].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_visi_text_1)));
	viscs[0].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_visi_text_2)));
	viscs[0].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_visi_text_3)));
	viscs[1].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omega0_text_0)));
	viscs[1].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omega0_text_1)));
	viscs[1].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omega0_text_2)));
	viscs[1].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omega0_text_3)));
	viscs[2].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma_text_0)));
	viscs[2].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma_text_1)));
	viscs[2].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma_text_2)));
	viscs[2].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma_text_3)));
	viscs[3].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taut_text_0)));
	viscs[3].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taut_text_1)));
	viscs[3].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taut_text_2)));
	viscs[3].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taut_text_3)));
	viscs[4].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omegainf_text_0)));
	viscs[4].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omegainf_text_1)));
	viscs[4].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omegainf_text_2)));
	viscs[4].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_omegainf_text_3)));
	viscs[5].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_tau1_text_0)));
	viscs[5].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_tau1_text_1)));
	viscs[5].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_tau1_text_2)));
	viscs[5].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_tau1_text_3)));
	viscs[6].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma0_text_0)));
	viscs[6].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma0_text_1)));
	viscs[6].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma0_text_2)));
	viscs[6].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_gamma0_text_3)));
	viscs[7].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_delta_text_0)));
	viscs[7].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_delta_text_1)));
	viscs[7].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_delta_text_2)));
	viscs[7].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_delta_text_3)));
	viscs[8].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taud_text_0)));
	viscs[8].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taud_text_1)));
	viscs[8].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taud_text_2)));
	viscs[8].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_taud_text_3)));
	viscs[9].a = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_beta_text_0)));
	viscs[9].b = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_beta_text_1)));
	viscs[9].c = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_beta_text_2)));
	viscs[9].d = atof(gtk_entry_get_text(GTK_ENTRY(box_visco_beta_text_3)));
}

/* Open a window for long text display. */
void display_text (char *file, char *title, char *lab_txt)
{
	GtkWidget *vbox,*hbox,*text_win,*button,*scr,*txt,*label;
	char buffer[BUFSIZ];
	char full_name[300];
	int nchars;
	FILE *ff;

	strcpy(full_name,getenv("HOME"));
	strcat(full_name,"/.fit28/");
	strcat(full_name,file);

	text_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(text_win),title);
	gtk_signal_connect(GTK_OBJECT(text_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(text_win));
	gtk_signal_connect(GTK_OBJECT(text_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));

	vbox=gtk_vbox_new(FALSE,2);
	gtk_container_add(GTK_CONTAINER(text_win),vbox);

	scr=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scr),GTK_POLICY_NEVER,
				      GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start_defaults(GTK_BOX(vbox),scr);

	txt=gtk_text_new(NULL,NULL);
	gtk_widget_set_usize(txt,400,400);
	gtk_container_add(GTK_CONTAINER(scr),txt);
	gtk_text_set_editable(txt,FALSE);

	hbox=gtk_hbox_new(TRUE,2);
	gtk_box_pack_start_defaults(GTK_BOX(vbox),hbox);

	label=gtk_label_new(lab_txt);
	gtk_box_pack_start_defaults(GTK_BOX(hbox),label);

	button=gtk_button_new_with_label("OK");
	gtk_signal_connect_object(GTK_OBJECT(button),"clicked",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));
	gtk_box_pack_start_defaults(GTK_BOX(hbox),button);

	ff=fopen(full_name,"r");
	if (!ff)
	{
		print_info("Error: can not find file [%s]",full_name);
		return;
	}
	while (1)
	{
		nchars=fread(buffer,1,1024,ff);
		gtk_text_insert(txt,NULL,NULL,NULL,buffer,nchars);
		if (nchars<1024)
			break;
	}
	fclose(ff);
	gtk_widget_show_all(text_win);
} 

void edit_text_file (GtkWidget *widget, gpointer data)
{
	GtkWidget *vbox,*hbox,*text_win,*button_save,*button_done,*scr,*txt,*label;
	GtkWidget *button_print;
	char buffer[BUFSIZ];
	char fname[300];
	int nchars;
	FILE *ff;
	char command[10000];
	
	strcpy(fname,gtk_entry_get_text(GTK_ENTRY(data)));
	
	sprintf(command,"emacs %s &", fname);
	printf(" now running %s\n", command);
	system(command);
	return ; 
	
	text_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(text_win),"Edit file...");
	gtk_signal_connect(GTK_OBJECT(text_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(text_win));
	gtk_signal_connect(GTK_OBJECT(text_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));

	vbox=gtk_vbox_new(FALSE,2);
	gtk_container_add(GTK_CONTAINER(text_win),vbox);

	scr=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scr),GTK_POLICY_NEVER,
				      GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start_defaults(GTK_BOX(vbox),scr);

	txt=gtk_text_new(NULL,NULL);
	gtk_widget_set_usize(txt,400,400);
	gtk_container_add(GTK_CONTAINER(scr),txt);
	gtk_text_set_editable(txt,TRUE);

	hbox=gtk_hbox_new(TRUE,2);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,0);

	label=gtk_label_new(fname);
	gtk_box_pack_start_defaults(GTK_BOX(hbox),label);

	gtk_widget_set_name(txt,fname);

	button_save=gtk_button_new_with_label("Save");
	gtk_signal_connect(GTK_OBJECT(button_save),"clicked",(GtkSignalFunc)save_edited_file,
		      txt);
	gtk_box_pack_start(GTK_BOX(hbox),button_save,TRUE,TRUE,0);

	button_print=gtk_button_new_with_label("Print");
	gtk_signal_connect(GTK_OBJECT(button_print),"clicked",(GtkSignalFunc)print_edited_file,
		      txt);
	gtk_box_pack_start(GTK_BOX(hbox),button_print,TRUE,TRUE,0);

	button_done=gtk_button_new_with_label("Done");
	gtk_signal_connect_object(GTK_OBJECT(button_done),"clicked",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));
	gtk_box_pack_start(GTK_BOX(hbox),button_done,TRUE,TRUE,0);

	ff=fopen(fname,"r");
	if (!ff)
	{
		print_info("Error: can not find file [%s]",fname);
		return;
	}
	while (1)
	{
		nchars=fread(buffer,1,1024,ff);
		gtk_text_insert(txt,NULL,NULL,NULL,buffer,nchars);
		if (nchars<1024)
			break;
	}
	fclose(ff);
	gtk_widget_show_all(text_win);
}

void save_edited_file (GtkWidget *widget, gpointer data)
{
	gchar *fname;
	gchar *t_data;
	guint len;
	FILE *ff;
	GtkWidget *txt = (GtkWidget *)data;

	fname = gtk_widget_get_name(txt);
	len = gtk_text_get_length(txt);
	t_data = gtk_editable_get_chars(GTK_EDITABLE(txt),0,len);

	ff=fopen(fname,"w");
	if (!ff)
	{
		print_info("Error: can not open file [%s]",fname);
		return;
	}
	fwrite(t_data,sizeof(char),len,ff);
	fclose(ff);
}

void print_edited_file (GtkWidget *widget, gpointer data)
{
	gchar *fname;
	char cmd[300];
	GtkWidget *txt = (GtkWidget *)data;

	save_edited_file(widget,data);
	fname = gtk_widget_get_name(txt);
	strcpy(cmd,"lpr -P");
	strcat(cmd,printer);
	strcat(cmd," ");
	strcat(cmd,fname);
	system(cmd);
}

void init_excitations_boxes (void)
{
	int i;

	for (i=0;i<12;i++)
	{
		strcpy(ex[i],"dho");
		excits[i*3].a = 0.1;
		excits[i*3].b = 0.01;
		excits[i*3].c = 0.0001;
		excits[i*3].d = 100;
	
		excits[i*3+1].a = 2;
		excits[i*3+1].b = 0.01;
		excits[i*3+1].c = 0.1;
		excits[i*3+1].d = 20;

		excits[i*3+2].a = 0.1;
		excits[i*3+2].b = 0.01;
		excits[i*3+2].c = 0.1;
		excits[i*3+2].d = 10;
	}
}

void show_excitations_boxes (int n)
{
	if (n>=1) {
		gtk_widget_show(box_exc1);
	} else {
		gtk_widget_hide(box_exc1);
	}
	if (n>=2) {
		gtk_widget_show(box_exc2);
	} else {
		gtk_widget_hide(box_exc2);
	}
	if (n>=3) {
		gtk_widget_show(box_exc3);
	} else {
		gtk_widget_hide(box_exc3);
	}
	if (n>=4) {
		gtk_widget_show(box_exc4);
	} else {
		gtk_widget_hide(box_exc4);
	}
	if (n>=5) {
		gtk_widget_show(box_exc5);
	} else {
		gtk_widget_hide(box_exc5);
	}
	if (n>=6) {
		gtk_widget_show(box_exc6);
	} else {
		gtk_widget_hide(box_exc6);
	}
	if (n>=7) {
		gtk_widget_show(box_exc7);
	} else {
		gtk_widget_hide(box_exc7);
	}
	if (n>=8) {
		gtk_widget_show(box_exc8);
	} else {
		gtk_widget_hide(box_exc8);
	}
	if (n>=9) {
		gtk_widget_show(box_exc9);
	} else {
		gtk_widget_hide(box_exc9);
	}
	if (n>=10) {
		gtk_widget_show(box_exc10);
	} else {
		gtk_widget_hide(box_exc10);
	}
	if (n>=11) {
		gtk_widget_show(box_exc11);
	} else {
		gtk_widget_hide(box_exc11);
	}
	if (n>=12) {
		gtk_widget_show(box_exc12);
	} else {
		gtk_widget_hide(box_exc12);
	}
}

void on_box_data2_adj_0_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	int i,j;
	nex = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(box_data2_spin_0));
	for (i=0;i<12;i++)
	{
		j = 6+(i*3)+2;
		if (i>=nex)
		{
			fixed[j] = 0;
		}
		else
		{
			if ((fl_convol_type==1) || (fl_convol_type==3))
				fixed[j] = 1;
		}
	}
	show_excitations_boxes(nex);
/*	refresh_displayed_data(); */
}

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  display_text("about.txt","About Fit28",VERSION);
}

void
on_example_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  display_text("exnex.inp","Example file for Fit28",VERSION);
}

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	gtk_main_quit();
}

void save_really (void)
{
	write_file(file_inp);
	strcpy(last_data_file,nomefile);
	refresh_t_as_param_mode();
}

void
on_save_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	FILE *fil;
	fil = fopen(file_inp,"r");
	if (fil)
	{
		fclose(fil);
		if (!fl_t_as_param)
		{
			dia_type = 4;
			dialog_window();
		}
		else
		{
			if (strcmp(gtk_entry_get_text(GTK_ENTRY(box_files1_text_0)),last_data_file))
			{
				dia_type = 3;
				dialog_window();
			}
			else
			{
				dia_type = 1;
				dialog_window();
			}
		}
	}
/*	else
		save_really(); */
}

void on_load_activate (GtkWidget *item, gpointer user_data)
{
	file_selection = gtk_file_chooser_dialog_new ("Select an input file...",
	      main_window,
	      GTK_FILE_CHOOSER_ACTION_OPEN,
	      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
	      NULL);
	if (gtk_dialog_run(GTK_DIALOG(file_selection)) == GTK_RESPONSE_ACCEPT)
	{
		file_inp = g_strdup(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_selection)));
		read_input_file(file_inp);
	}

	gtk_widget_destroy(file_selection);
}

void on_save_as_activate (GtkWidget *item, gpointer user_data)
{
	fl_t_as_param = 1;

	file_selection = gtk_file_chooser_dialog_new ("Select a save file...",
	      main_window,
	      GTK_FILE_CHOOSER_ACTION_SAVE,
	      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
	      NULL);
	gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(file_selection),file_inp);

	if (gtk_dialog_run(GTK_DIALOG(file_selection)) == GTK_RESPONSE_ACCEPT)
	{
		file_inp=g_strdup(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_selection)));
		gtk_label_set_text(GTK_LABEL(inp_label),g_basename(file_inp));
		save_really();
	}

	gtk_widget_destroy (file_selection);
}

void remove_undo_files (void)
{
	char dirname[BUFSIZ],tmpstr[BUFSIZ];
	DIR *dir;
	FILE *fil;
	struct dirent *file;

	strcpy(dirname,getenv("HOME"));
	strcat(dirname,"/.fit28/undo_files/");
	dir = opendir(dirname);
	if (!dir)
		return;
	while (file = readdir(dir))
	{
		if (!strcmp(file->d_name,"."))
			continue;
		if (!strcmp(file->d_name,".."))
			continue;
		strcpy(tmpstr,"rm ");
		strcat(tmpstr,dirname);
		strcat(tmpstr,file->d_name);
		system(tmpstr);
	}
	closedir(dir);
}

void write_undo_file (void)
{
	char dirname[BUFSIZ],tmpstr[BUFSIZ],oldfile[BUFSIZ];
	DIR *dir;
	FILE *fil;
	struct dirent *file;
	struct stat fstat;
	time_t ftime,oldtime = 0;
	int n;

	if (!undo_depth)
		return;
	strcpy(dirname,getenv("HOME"));
	strcat(dirname,"/.fit28/undo_files/");
	dir = opendir(dirname);
	if (!dir)
	{
		printf("===> No undo files directory. Creating it...\n");
		strcpy(tmpstr,"mkdir ");
		strcat(tmpstr,dirname);
		system(tmpstr);
		dir = opendir(dirname);
		if (!dir)
		{
			printf("===> Can not create directory [%s]. Aborting.\n",dirname);
			return;
		}
	}
	n = 0;
	while (file = readdir(dir))
	{
		if (!strcmp(file->d_name,"."))
			continue;
		if (!strcmp(file->d_name,".."))
			continue;
		strcpy(tmpstr,dirname);
		strcat(tmpstr,file->d_name);
		if (!stat(tmpstr,&fstat))
		{
			ftime = fstat.st_mtime;
			if ((oldtime>ftime) || !oldtime)
			{
				oldtime = ftime;
				strcpy(oldfile,tmpstr);
			}
			n++;
		}
	}
	closedir(dir);
	if (n>=undo_depth) /* Remove the oldest file */
	{
		strcpy(tmpstr,"rm ");
		strcat(tmpstr,oldfile);
		system(tmpstr);
	}
	ftime = time(NULL);
	sprintf(tmpstr,"%sfit28_tmp_%ld.inp",dirname,ftime);
	write_file(tmpstr);
}

void on_undo_activate (GtkWidget *item, gpointer user_data)
{
	char dirname[BUFSIZ],tmpstr[BUFSIZ],myfile[BUFSIZ],second[BUFSIZ];
	DIR *dir;
	FILE *fil;
	struct dirent *file;
	struct stat fstat;
	time_t ftime,mytime = 0;
	int n;

	strcpy(dirname,getenv("HOME"));
	strcat(dirname,"/.fit28/undo_files/");
	dir = opendir(dirname);
	if (!dir)
	{
		printf("===> No undo files directory. Aborting.\n");
		return;
	}
	n = 0;
	while (file = readdir(dir))
	{
		if (!strcmp(file->d_name,"."))
			continue;
		if (!strcmp(file->d_name,".."))
			continue;
		strcpy(tmpstr,dirname);
		strcat(tmpstr,file->d_name);
		if (!stat(tmpstr,&fstat))
		{
			ftime = fstat.st_mtime;
			if (ftime>mytime)
			{
				if (mytime!=0)
				{
					strcpy(second,myfile);
				}
				mytime = ftime;
				strcpy(myfile,tmpstr);
			}
			n++;
		}
	}
	closedir(dir);
	if (n<=1) /* No file in directory */
	{
		printf("===> No undo file in directory.\n");
		return;
	}
	read_input_file(second);
	strcpy(tmpstr,"rm ");
	strcat(tmpstr,myfile);
	system(tmpstr);
}

void on_preview_activate (GtkWidget *item, gpointer user_data)
{
	char cmd[1000],filename[300];

	if (fit_running)
		return;
	capture_data();
	/* if (!strlen(file_inp))
		return; */
	fl_stop_fit = 0;
	fl_preview = 1;
	write_undo_file();
	main_fit();
	plot_do_file();
	fl_preview = 0;
	strcpy(filename,getenv("HOME"));
	strcat(filename,"/.fit28/");
	strcat(filename,"preview.do");
	sprintf(cmd,"rm -rf %s",filename);
	system(cmd);
	strcpy(filename,getenv("HOME"));
	strcat(filename,"/.fit28/");
	strcat(filename,"preview.fit");
	sprintf(cmd,"rm -rf %s",filename);
	system(cmd);
}

void on_fit_activate (GtkWidget *item, gpointer user_data)
{
	if (!strlen(file_inp))
		return;
	capture_data();
	check_data();
	if (!fit_running)
	{
		fit_really();
	}
	else
	{
		dia_type = 2;
		dialog_window();
	}
}

void fit_really (void)
{
/*	GdkColor color; */

	if (!file_inp)
		return;
	fl_stop_fit = 0;

/*	gdk_color_parse("#7777FF",&color);
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_NORMAL,&color);
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_ACTIVE,&color);
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_PRELIGHT,&color); */

	/* We're not in the preview case: we run the fit in parallel. */
	if (pthread_create(&fit_thread, NULL, (void *) &main_fit, NULL))
	{
		print_info("Error: could not run the fitting thread.","");
	}
	else
	{
		fit_running = 1;
	}
}

void check_data (void)
{
	int i,j,k;

	for (i=0;i<6;i++)
	{
		if (params[i].d<params[i].a)
		{
			printf("Warning: higher limit of parameter %d set to %f.\n",i,params[i].a+sqrt(params[i].a));
			params[i].d=params[i].a+sqrt(params[i].a);
			refresh_displayed_data();
		}
	}
	for (i=0;i<nex;i++)
	{
		for (j=0;j<3;j++)
		{
			k = 3*i+j;
			if (excits[k].d<excits[k].a)
			{
				printf("Warning: higher limit of excitation %d(%d) set to %f.\n",i+1,j+1,excits[k].a+sqrt(excits[k].a));
				excits[k].d=excits[k].a+sqrt(excits[k].a);
				refresh_displayed_data();
			}
		}
	}
}

void make_x11_do_file (void)
{
	char cmd[300];

	sprintf(cmd,"chg %s x11",nomedo);
	system(cmd);
}

void make_ps_do_file (void)
{
	char cmd[300];

	sprintf(cmd,"chg %s ps",nomedo);
	system(cmd);
}

void make_log_do_file (void)
{
	char cmd[300];

	sprintf(cmd,"log_do %s on",nomedo);
	system(cmd);
}

void make_linear_do_file (void)
{
	char cmd[300];

	sprintf(cmd,"log_do %s off",nomedo);
	system(cmd);
}

void
on_kill_fit                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	kill_current_fit();
}

void
on_report_bug_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *vbox,*hbox,*text_win,*button_send,*button_cancel,*scr,*txt,*label;

	text_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(text_win),"Bug report");
	gtk_signal_connect(GTK_OBJECT(text_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(text_win));
	gtk_signal_connect(GTK_OBJECT(text_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));

	vbox=gtk_vbox_new(FALSE,2);
	gtk_container_add(GTK_CONTAINER(text_win),vbox);

	label=gtk_label_new("Write the bug report here:");
	gtk_box_pack_start_defaults(GTK_BOX(vbox),label);

	scr=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scr),GTK_POLICY_NEVER,
				      GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start_defaults(GTK_BOX(vbox),scr);

	txt=gtk_text_new(NULL,NULL);
	gtk_widget_set_usize(txt,400,400);
	gtk_container_add(GTK_CONTAINER(scr),txt);
	gtk_text_set_editable(txt,TRUE);

	hbox=gtk_hbox_new(TRUE,2);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,0);

	button_send=gtk_button_new_with_label("Send");
	gtk_signal_connect(GTK_OBJECT(button_send),"clicked",(GtkSignalFunc)send_bug_report,
		      txt);
	gtk_box_pack_start(GTK_BOX(hbox),button_send,TRUE,TRUE,0);

	button_cancel=gtk_button_new_with_label("Cancel");
	gtk_signal_connect_object(GTK_OBJECT(button_cancel),"clicked",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(text_win));
	gtk_box_pack_start(GTK_BOX(hbox),button_cancel,TRUE,TRUE,0);

	gtk_widget_show_all(text_win);
}

void send_bug_report (GtkWidget *widget, gpointer data)
{
	char *t_data;
	guint len;
	GtkWidget *txt = (GtkWidget *)data;
	GdkWindow *parwin;
	FILE *ff;

	len = gtk_text_get_length(txt);
	t_data = gtk_editable_get_chars(GTK_EDITABLE(txt),0,len);

	ff=fopen(".tmpbugrep","w");
	if (!ff)
	{
		print_info("Error: can not open file [.tmpbugrep]","");
		return;
	}
	fwrite(t_data,sizeof(char),len,ff);
	fclose(ff);
	system("mail -s 'Fit28 bug report' aberaud@esrf.fr < .tmpbugrep");
	system("rm .tmpbugrep");
	parwin = gtk_widget_get_parent_window(txt);
	gdk_window_destroy(parwin);
}

void on_fit_type_activate (GtkWidget *item, gpointer data)
{
	int nb = (int)data;
	if (!fit_types_loaded)
		return;

	if (GTK_TOGGLE_BUTTON(item)->active) 
		fit_types[nb] = 1;
	else
		fit_types[nb] = 0;
}
void on_fft_activate (GtkWidget *item, gpointer data)
{
	if (GTK_TOGGLE_BUTTON(item)->active) 
		fft_active = 1;
	else
		fft_active = 0;
}

void on_visco_check_e2_activate (GtkWidget *item, gpointer data)
{
	if (GTK_TOGGLE_BUTTON(item)->active) 
		fl_visco_e2 = 1;
	else
		fl_visco_e2 = 0;
}

void on_pv_load_activate (GtkWidget *item, gpointer user_data)
{
	char full_name[BUFSIZ],tmpstr[BUFSIZ];
	int tst = 0;
	FILE *fil;

	strcpy(full_name,getenv("HOME"));
	strcat(full_name,"/.fit28/pv_param.dat");

	fil = fopen(full_name,"r");
	if (!fil)
	{
		print_info("Error: Pseudo Voigt data file does not exist [%s]",full_name);
		return;
	}

	while (fgets(tmpstr,BUFSIZ,fil))
	{
		if (!strncasecmp(tmpstr,nomeris,strlen(nomeris)))
		{
			tst = 1;
			break;
		}
	}
	if (!tst)
	{
		print_info("Error: could not find the required data in file [%s]",full_name);
		return;
	}
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%lf",&psv_eta);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%lf",&psv_gamma1);
	fgets(tmpstr,BUFSIZ,fil);
	sscanf(tmpstr,"%lf",&psv_gamma2);
	refresh_displayed_data();
}

void refresh_t_as_param_mode (void)
{
	if (fl_t_as_param)
	{
		gtk_widget_hide(box_temperature);
		gtk_label_set_text(gr_label,"T");
		par_nm[3] = "T";
		GR = 2;
	}
	else
	{
		gtk_widget_show(box_temperature);
		gtk_label_set_text(gr_label,"GR");
		par_nm[3] = "GR";
	}
}

