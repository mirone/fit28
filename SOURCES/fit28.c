/*
 * fitfront.c - A. Beraud (March 2003)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <glib.h>

#include "interface.h"
#include "callbacks.h"
#include "options.h"
#include<locale.h>

int main (int argc, char *argv[])
{
  putenv("LC_NUMERIC=en_US");
  // setlocale(LC_ALL, "");
  //  setlocale(LC_NUMERIC, "en_US");
 
	int i,fl_toread;
	char tmp[300],toread[300];

	fft_active=1;

	fl_toread = 0;
	for (i=1; i<argc; i++)
	{
		if (!strcmp(argv[i], "--help"))
		{
			printf("%s\n\n",VERSION);
			printf("Usage: fit28 [options ...] (input_file)\n\n");
			printf("Available options:\n");
			printf(" --help    : this help\n");
			printf(" --version : version number\n");
			printf("\n");
			exit(0);
		}
		else if (!strcmp(argv[i], "--version"))
		{
			printf("%s\n",VERSION);
			exit(0);
		}
		else if (i==argc-1) /* Last argument */
		{
			strcpy(toread,argv[1]);
			fl_toread = 1;
		}
	}

	disp=XOpenDisplay(NULL);

	gtk_set_locale ();
	gtk_init (&argc, &argv);

	strcpy(printer,"a4bid28");
	strcpy(resol_dir,"/users/opid28/resolutions/validresolutions/");
	plot_prgm = 1;
	fit_types[0] = 1;
	fit_types[1] = 1;
	fl_cen_bose = 0;
	fl_live = 0;
	fl_t_as_param = 1;
	fl_convol_type = 0;
	strcpy(last_data_file,"");
	nb_visc = 0;
	fit_running = 0;
	undo_depth = 10;
	strcpy(centtype,"lor");
	clear_swap();
	main_window = create_main_window ();

	gtk_widget_show (main_window);

	strcpy(tmp,getenv("HOME"));
	if (fl_t_as_param)
	{
		strcat(tmp,"/.fit28/start_t.inp");
	}
	else
	{
		strcat(tmp,"/.fit28/start.inp");
	}
	file_inp = g_strdup(tmp);
	read_input_file(file_inp);
	file_inp = g_strdup("");
	init_vis_params();
	remove_undo_files();
	load_preferences();
	if (fl_toread) {
		read_input_file(toread);
	}
	gtk_main ();

	return 0;
}

