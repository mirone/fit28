#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <strings.h>
#include <complex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <sys/timeb.h>

#include "fitvie.h"
#include "callbacks.h"
#include "interface.h"

#include "cfortran.h"
#include "minuitfcn.h"
#include "minuit.h"
#include <fftw3.h>

extern char *par_nm[6];
extern char *visc_labels[10];

void kill_current_fit (void)
{
	int dummy;
	char *command = NULL;
	command = (char *)malloc(300*sizeof(char));

	if (fit_running)
	{
		fl_stop_fit = 1;
		sprintf(command,"EXIT");
		MNCOMD(minuitfcn,command,dummy,NULL);
	}
}

gboolean no_convolution (void)
{
	if ((fl_convol_type==1) || (fl_convol_type==3))
		return TRUE;
	return FALSE;
}

void sync_curves (int last)
{
	int i,j,lmi,lmj;
	double e;
	double *xval,*fcnval=0;
	xval = (double *)malloc(100*sizeof(double));

	if (!fl_preview && last)
	{
		xval[0] = params[0].a+err_par[0];
		xval[1] = params[1].a+err_par[1];
		xval[2] = params[2].a+err_par[2];
		xval[3] = params[3].a+err_par[3];
		xval[4] = params[4].a+err_par[4];
		xval[5] = params[5].a+err_par[5];
	}

	if (last)
	{
		sumcen = 0;
		sumvis = 0;
		for (i=0;i<nex;i++)
		{
			sumexc[i] = 0;
			if (!fl_preview)
			{
				for (j=0;j<3;j++)
					xval[6+3*i+j] = excits[3*i+j].a+err_par[6+3*i+j];
			}
		}
		for (i=0;i<nn-1;i++)
		{
			sumcen += tcnc[i]*(frq[i+1]-frq[i]);
			sumvis += tcnv[i]*(frq[i+1]-frq[i]);
			for (j=0;j<nex;j++)
				sumexc[j] += tcne[j][i]*(frq[i+1]-frq[i]);
		}
		sumall = sumcen+sumvis;
		for (i=0;i<nex;i++)
			sumall += sumexc[i];
		sume2 = 0;
		if (nb_visc && !fl_preview && last)
		{
			xval[6+nex] = viscs[0].a+err_par[6+nex];
			xval[6+nex+1] = viscs[1].a+err_par[6+nex+1];
			xval[6+nex+2] = viscs[2].a+err_par[6+nex+2];
			xval[6+nex+3] = viscs[3].a+err_par[6+nex+3];
			xval[6+nex+4] = viscs[4].a+err_par[6+nex+4];
			xval[6+nex+5] = viscs[5].a+err_par[6+nex+5];
			xval[6+nex+6] = viscs[6].a+err_par[6+nex+6];
			xval[6+nex+7] = viscs[7].a+err_par[6+nex+7];
			xval[6+nex+8] = viscs[8].a+err_par[6+nex+8];
			xval[6+nex+9] = viscs[9].a+err_par[6+nex+9];
		}
		if (nb_visc && fl_visco_e2)
		{
			meane2 = 0;
			for (i=-100000;i<99999;i++)
			{
				e = i*0.01;
				sume2 += visco_calc(e)*e*e*0.01;
			}
			lmi = lmj = 0;
			for (i=0;i<nn;i++)
			{
				if (visco_local_max(i))
				{
					if (!lmi)
					{
						lmi = i;
					}
					else if (!lmj)
					{
						lmj = i;
					}
				}
			}
			if (lmi)
			{
				if (lmj)
				{
					meane2 = (fabs(frq[lmj])+fabs(frq[lmj]))/2;
				}
				else
				{
					meane2 = frq[lmi];
				}
			}
		}
	}
	for (i=0;i<nn;i++)
	{
		tcnc[i] += BG+frq[i]*SL;
		tcnv[i] += BG+frq[i]*SL;
		for (j=0;j<nex;j++)
			tcne[j][i] += BG+frq[i]*SL;
	}
	/* Now we write the fitted curves so that we can display them (plot or preview). */
	write_fit_curves();
	write_do_file();
	real_chi2 = chi2;
	if (!fl_preview && last)
	{
		/* Now, recompute once in order to get the errors on the sums */
		fcn(0,0,&fcnval,xval,0,NULL);
		sumcen_err = 0;
		sumvis_err = 0;
		for (i=0;i<nex;i++)
		{
			sumexc_err[i] = 0;
		}
		for (i=0;i<nn-1;i++)
		{
			sumcen_err += tcnc[i]*(frq[i+1]-frq[i]);
			sumvis_err += tcnv[i]*(frq[i+1]-frq[i]);
			for (j=0;j<nex;j++)
				sumexc_err[j] += tcne[j][i]*(frq[i+1]-frq[i]);
		}
		sumall_err = sumcen_err+sumvis_err;
		for (i=0;i<nex;i++)
			sumall_err += sumexc_err[i];
		sumall_err = fabs(sumall_err-sumall);
		sumcen_err = fabs(sumcen_err-sumcen);
		sumvis_err = fabs(sumvis_err-sumvis);
		for (i=0;i<nex;i++)
		{
			sumexc_err[i] = fabs(sumexc_err[i]-sumexc[i]);
		}
		write_fit_param_file();
	}
}

/*
void close_fit_procedure (void)
{
	gtk_widget_restore_default_style(GTK_WIDGET(GTK_BIN(fit_button)->child));
	gtk_label_set_text(GTK_LABEL(GTK_BIN(fit_button)->child),"Fit");
	gtk_widget_draw_default(GTK_WIDGET(fit_button));
}
*/

void reset_fit_button (void)
{
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_NORMAL,NULL);
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_ACTIVE,NULL);
	gtk_widget_modify_bg(GTK_WIDGET(fit_button),GTK_STATE_PRELIGHT,NULL);
}

void main_fit (void)
{
	int i,j,n,dummy;
	FILE *fil;
	double ddum,larg;
	char dum[300];
	char tmpstr[100];
	char *exc_nm[3] = {"P","O","G"};
	double par;
	char *command = NULL;
	command = (char *)malloc(300*sizeof(char));

	printf("[ %-30s : %15s ]\n","Reading data file",nomefile);
	fil = fopen(nomefile,"r");
	if (!fil)
	{
		if (fl_preview)
			print_info("Error: wrong data file [%s]",nomefile);
		else
			printf("Error: wrong data file [%s]\n",nomefile);
		if (fit_running)
		{
			fit_running = 0;
			printf("\n\n *** Fit aborted ***\n");
			pthread_exit(0);
		}
		else
			return;
	}
	avmon = 0;
	memset(frq,0,600*sizeof(double));
	memset(dat,0,600*sizeof(double));
	memset(erd,0,600*sizeof(double));
	memset(mon,0,600*sizeof(double));
	memset(fff,0,(2*10000+1)*sizeof(double));
	memset(ris,0,(2*10000+1)*sizeof(double));
	for (i=0;i<600;i++)
	{
		if (fgets(tmpstr,BUFSIZ,fil) == NULL) break;
		if (!strcasecmp(nomenorm,"cnt"))
			sscanf(tmpstr,"%lf %lf %lf %s %s %lf",&frq[i],&dat[i],&erd[i],dum,dum,&mon[i]);
		else
		{
			sscanf(tmpstr,"%lf %s %s %s %lf %lf %lf",
			&frq[i],dum,dum,dum,&dat[i],&mon[i],&erd[i]);
			erd[i]=dat[i]*(erd[i]/dat[i]+sqrt(mon[i]+1.)/mon[i])/mon[i];
			dat[i] /= mon[i];
		}
		avmon += mon[i];
	}
	nn = i; /* Because we start at 0 */
	avmon /= nn;
	fclose(fil);

	pig = atan(1)*4;
	bol = .0861735;

	if ((fl_convol_type==2) || (fl_convol_type==3))
	{
		if (!psv_gamma1 || !psv_gamma2 || !psv_eta)
		{
			if (fl_preview)
				print_info("Error: pseudo-voigt parameters are zero.","");
			else
				printf("Error: pseudo-voigt parameters are zero.\n");
			if (fit_running)
			{
				fit_running = 0;
				printf("\n\n *** Fit aborted ***\n");
				pthread_exit(0);
			}
			else
				return;
		}
	}

	if (!no_convolution())
	{
		if (fl_t_as_param)
		{
			GR = 2;
		}
		else
		{
			GR = params[3].a;
		}
		/* Define resolution step. */
		stpr = 0;
		for (i=0;i<nn-1;i++)
		{
			stpr += frq[i+1]-frq[i];
		}
		stepr = (stpr/(nn-1))/frac;
		stepq = stepr*stepr;
		nc = (int)(100*GR/stepr);
		if (nc > 10000)
		{
			if (fl_preview)
				print_info("Error: nc > 10000","");
			else
				printf("Error: nc > 10000\n");
			if (fit_running)
			{
				fit_running = 0;
				pthread_exit(0);
			}
			else
				return;
		}
		for (i=-nc;i<=nc;i++)
		{
			j = i+nc;
			fff[j] = (double)(i)*stepr;
		}

		if (fl_convol_type==0) /* Convolution with resolution file */
		{
			printf("[ %-30s : %15s ]\n","Reading resolution file",nomeris);
			fil = fopen(nomeris,"r");
			if (!fil)
			{
				printf("Warning: no resolution file [%s] in current directory.\n",nomeris);
				printf("         Trying the user defined resolutions directory.\n");
				strcpy(dum,resol_dir);
				strcat(dum,nomeris);
				fil = fopen(dum,"r");
			}
			if (!fil)
			{
				printf("Warning: no resolution file [%s] in user defined resolutions directory.\n",dum);
				printf("         Searching the parameters directory.\n");
				strcpy(dum,getenv("HOME"));
				strcat(dum,"/.fit28/resolutions/");
				strcat(dum,nomeris);
				fil = fopen(dum,"r");
			}
			if (!fil)
			{
				printf("Warning: no resolution file [%s] in the parameters directory.\n",dum);
				printf("If you want to perform a fit without resolution, check the dedicated button.\n");
				if (fit_running)
				{
					fit_running = 0;
					printf("\n\n *** Fit aborted ***\n");
					pthread_exit(0);
				}
				else
					return;
			}
			else
			{
				i = 0;
				while (fgets(tmpstr,BUFSIZ,fil))
				{
					sscanf(tmpstr,"%lf %lf",&fris[i],&dris[i]);
					i++;
				}
				fclose(fil);

				nris = i;
				frisi = fris[0];
				frise = fris[nris-1];
				larg = fabs(frisi);
				if (fabs(frise)<larg)
					larg = fabs(frise);
				dfris = 0;
				for (i=0;i<nris;i++)
					dfris += fris[1]-fris[0];
				dfris /= nris-1;
				/* Normalizing resolution data to unity. */
				sum = 0;
				for (i=0;i<nris;i++)
				{
					sum += dris[i];
				}
				/*				sum /= nris*dfris; */
				sum *= dfris;
				for (i=0;i<nris;i++)
				{
					dris[i] /= sum;
				}
				/* Array of resolution variations. */
				sum=0.0;
				for (i=0;i<nris-1;i++)
				{
					ddris[i] = dris[i+1]-dris[i];
					sum+=dris[i];
				}


				/* Compute position of the maximum/zero-energy. */
				res_max = 0;
				res_z1 = res_z2 = 0;
				for (i=0;i<nris;i++)
					if (dris[i]>res_max)
						res_max = dris[i];
				res_max /= 2; /* Half maximum. */
				for (i=0;i<nris-1;i++)
				{
					if ((dris[i]<=res_max) && (dris[i+1]>=res_max))
						res_z1 = i;
					if ((dris[i]>=res_max) && (dris[i+1]<res_max))
						res_z2 = i;
				}
				res_zero = (int)((res_z1+res_z2)/2);

				/* Calculate the resolution function */
				for (i=-nc;i<=nc;i++)
				{
					j = i+nc;
					ak = res_zero+(fff[j]/dfris);
					if ((ak > 0) && (ak < nris-1))
					{
						ik = (int)ak;
						ris[j] = dris[ik];
						dak = ak-(double)ik;
						ris[j] += dak*ddris[ik];
					}
					else
						ris[j] = 0;
				}
			}
		}
		if (fl_convol_type==2) /* Convolution with P-V */
		{
			for (i=-nc;i<=nc;i++)
			{
				j = i+nc;
				ris[j] = pseudo_voigt(fff[j]);
			}
		}
	}
	else if (fl_convol_type == 1) /* Pure no convol -> then cheat */
	{
		nc = 0;
		fff[0] = 0;
		ris[0] = 1;
		stepr = 1;
	}
	ftime(&live_timeb);

	MNINIT (5,6,7);
	n = 0;
	for (i=0;i<6;i++)
	{
		n++;
		MNPARM(n,par_nm[i],params[i].a,params[i].b,params[i].c,params[i].d,dummy);
	}
	for (i=0;i<nex;i++)
	{
		for (j=0;j<3;j++)
		{
			sprintf(dum,"%s%i",exc_nm[j],i+1);
			n++;
			MNPARM(n,dum,excits[i*3+j].a,excits[i*3+j].b,excits[i*3+j].c,excits[i*3+j].d,dummy);
		}
	}
	if (nb_visc)
	{
		for (j=0;j<10;j++)
		{
			sprintf(dum,"%s",visc_labels[j]);
			n++;
			MNPARM(n,dum,viscs[j].a,viscs[j].b,viscs[j].c,viscs[j].d,dummy);
		}
	}
	if (fl_preview)
	{
		sprintf(command, "EXIT");
		par = 0;
		MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
	}
	else
	{
		/* Send the FIX commands */
		for (i=0;i<42;i++)
		{
			if (fixed[i])
			{
				sprintf(command, "FIX");
				par = i+1;
				MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
			}
		}
		if (nb_visc)
		{
			for (i=0;i<10;i++)
			{
				j = i+(nex*3)+7;
				if (visco_fixed[i])
				{
					sprintf(command, "FIX");
					par = j;
					MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
				}
			}
		}

		/* Don't mess the console up. */
		sprintf(command, "SET PRI");
		par = 1;
		MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
		/* Set the error value. */
		sprintf(command, "SET ERR");
		par = 1;
		MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
		/* Start with Simplex minimization. */
		if (fit_types[0])
		{
			sprintf(command,"SIMPLEX");
			MNCOMD(minuitfcn,command,dummy,NULL);
			if (fl_stop_fit) /* Brutal exit. */
				if (fit_running)
				{
					fit_running = 0;
					printf("\n\n *** Fit aborted ***\n");
					pthread_exit(0);
				}
			/* Calculate errors matrix. */
			sprintf(command,"HESSE");
			par = 0;
			MNEXCM(minuitfcn, command, &par, 1, dummy, NULL);
			if (fl_stop_fit) /* Brutal exit. */
				if (fit_running)
				{
					fit_running = 0;
					printf("\n\n *** Fit aborted ***\n");
					pthread_exit(0);
				}
		}
		/* Continue with Migrad minimization. */
		if (fit_types[1])
		{
			sprintf(command,"MIGRAD");
			MNCOMD(minuitfcn,command,dummy,NULL);
			if (fl_stop_fit) /* Brutal exit. */
				if (fit_running)
				{
					fit_running = 0;
					printf("\n\n *** Fit aborted ***\n");
					pthread_exit(0);
				}
		}

		/* Get new data. */
		n = 0;
		for (i=0;i<6;i++)
		{
			n++;
			MNPOUT(n,tmpstr,params[i].a,err_par[i],ddum,ddum,dummy);
		}
		for (i=0;i<nex;i++)
		{
			for (j=0;j<3;j++)
			{
				n++;
				MNPOUT(n,tmpstr,excits[i*3+j].a,err_par[6+i*3+j],ddum,ddum,dummy);
			}
		}
		if (nb_visc)
		{
			for (j=0;j<10;j++)
			{
				n++;
				MNPOUT(n,tmpstr,viscs[j].a,err_par[6+nex*3+j],ddum,ddum,dummy);
			}
		}
	}

	sync_curves(1);
	if (fit_running)
	{
		printf("\n\n *** Fit ended ***\n");
		fit_running = 0;
	}
}

int visco_local_max (int i)
{
	int j;
	double v;
	int l = 3;
	if (i<l)
		return 0;
	if (i>=nn-l)
		return 0;
	v = tcne2[i];
	for (j=i-1;j>=i-l;j--)
	{
		if (tcne2[j]>=v)
			return 0;
		v = tcne2[j];
	}
	v = tcne2[i];
	for (j=i+1;j<=i+l;j++)
	{
		if (tcne2[j]>=v)
			return 0;
		v = tcne2[j];
	}
	return 1;
}

double visco_calc (double dw)
{
	double thc;

	visb = (V[1]*V[1]*(V[2]-1)*V[3])/(1+dw*dw*V[3]*V[3]);
	visb += ((V[4]*V[4]-V[2]*V[1]*V[1])*V[5])/(1+dw*dw*V[5]*V[5]) + V[6];
	visb += V[7]*(1/(dw*pow((1+dw*dw*V[8]*V[8]),V[9]/2)))
		*sin(V[9]*atan(dw*V[8]));
	visa = (dw*dw*V[1]*V[1]*(V[2]-1)*V[3]*V[3])/(1+dw*dw*V[3]*V[3]);
	visa += (dw*dw*(V[4]*V[4]-V[2]*V[1]*V[1])*V[5]*V[5])/(1+dw*dw*V[5]*V[5]);
	visa += V[7]*(1-cos(V[9]*atan(dw*V[8]))/pow((1+dw*dw*V[8]*V[8]),V[9]/2));
	thc = V[0]*visb/(pow(dw*dw-V[1]*V[1]-visa,2)+dw*dw*visb*visb);
	return thc;
}

double pseudo_voigt (double w)
{
	double thc;

	thc = (psv_eta*pow(psv_gamma1/2,2)/(pow(w,2)+pow(psv_gamma1/2,2))
		+ (1-psv_eta)*exp(-0.69314718*pow(w,2)/pow(psv_gamma2/2,2)));
	return thc;
}


double  cal_av_step(double * f ,int n) {
  int i;
  double res;
  res=f[n-1]-f[0];
  
  
  res=res/(n-1);
  return res;
}







void FFT_convolve(int SG_N, double *  SG_calc, double * SG_ris, fftw_complex * fftin,
		  fftw_plan fftp_for, fftw_plan fftp_rev, double  centerE     ) {
  

  int i;
  double a1,a2,b1,b2;

  for(i=0; i<SG_N; i++ ) {
    ((double *) fftin)[2*i] =   SG_calc[  i] ;
    ((double *) fftin)[2*i+1] =   0.0           ;
  }
  
  fftw_execute( fftp_for );
  
  for(i=0; i<SG_N; i++ ) {
    SG_calc[  i] =  ((double *) fftin)[i]   ;
  }
  
  
  for(i=0; i<SG_N; i+=2 ) {
    
    a1= SG_calc[  i    ];
    b1= SG_calc[  i+1  ];
    
    a2= SG_ris[  i    ];
    b2= SG_ris[  i+1  ];
    
    
    ((double *) fftin)[i] =   a1*a2-b1*b2 ;
    ((double *) fftin)[i+1] = a1*b2+b1*a2    ;
    if(i!=0) {
      ((double *) fftin)[2*SG_N -i]  =      ((double *) fftin)[i] ;
      ((double *) fftin)[2*SG_N -i+1  ] = -   ((double *) fftin)[i+1]  ;
    }
  }
  
  fftw_execute( fftp_rev );
  
  for(i=0; i<SG_N; i++ ) {
    SG_calc[  i] =  ((double *) fftin)[2*i]  / SG_N * stepr;
  }
}






void interpola( int nn ,double * frq, double *tcnv, double centerE, double ZR, double zerores, int SG_N, double *SG_calc, double SG_step) {
  int i, i1,i2;
  double wj,a1,a2, x;
  for (i=0;i<nn;i++)
    {      
      wj = frq[i]-centerE-ZR  -  zerores  ;
      if( wj>=0) {
	
	i1 = (int)( (wj )/SG_step )       ; 
	i2 = (int)( (wj )/SG_step )+1     ;
	a1=SG_calc[i1];
	a2=SG_calc[i2];
      }else {
	if ( (-wj) < SG_step ) {
	  i1 =  SG_N-1;
	  i2=0;
	  a1=SG_calc[i1];
	  a2=SG_calc[i2];
	  i1= -1;
	  i2=0;
	} else {
	  i1 = SG_N  - (int)( (-wj )/SG_step)-1  ; 
	  i2 = SG_N  - (int)( (-wj )/SG_step)  ;
	  a1=SG_calc[i1];
	  a2=SG_calc[i2];
	  i1 = - (int)( (-wj )/SG_step)-1 ; 
	  i2 = - (int)( (-wj )/SG_step)   ;
	}
      }
      x = wj / SG_step;
      tcnv[i] =       a1*(i2-x) +a2*(x-i1) ;
    }
}



/* #define USEFFT 0   ; fft_active used in place */

void fcn (int npar, double* grad, double* fcnval, double* xval, int iflag, void* futil)
{
  
	int j,k,iex,i, ka, kb;
	double thc;
	struct timeb this_timeb;
	static icountcall=0;

	double avstepE, avstepR, extE, centerE ;  /* average energy step experiment, average step Resolution function, extended energy range  */
	int SG_N ; /* stepped grid n of points */
	double SG_step;
	double *SG_ris ; 
	double *SG_calc ; 
	double *SG_freq ; 
	double SG_E1 ; 
	
	fftw_complex *fftin;
	fftw_plan fftp_for;
	fftw_plan fftp_rev;
	double a1,b1,a2,b2,x;
	int i1,i2;
	double zerores;
	int was_fft_active;

	printf(" chiamata n %d \n", icountcall++ );


	was_fft_active= fft_active;

	if(was_fft_active) {
	  avstepE=cal_av_step( frq , nn);
	  avstepR=cal_av_step( fff , 2*nc+1);
	  
	  /* printf(" il passo medio nei dati est %e e i dati sono %d \n" , avstepE,nn );
	     printf(" il passo medio nella  risoluzione  est %e e i dati sono %d \n",avstepR,2*nc+1); */
	  
	  extE=    nc*  avstepR +    avstepE*(nn-1) +   nc*  avstepR ; 
	  
	  centerE = (frq[0]+frq[nn-1])/2;

	  SG_step = avstepR ;

	  SG_N=1;
	  while(SG_N< ( extE / SG_step + 1 )  ) {
	    SG_N*=2;
	  }
	  SG_ris = malloc( SG_N    * sizeof(double) );
	  SG_calc= malloc( SG_N    * sizeof(double) );
	  SG_freq= malloc( SG_N* sizeof(double) );
	  memset(SG_ris, 0, SG_N    * sizeof(double) );
	  for(i=0; i<=nc ; i++) {
	    SG_ris[i] = ris[nc+i] ;   
	  }
	  for(i=1; i<=nc ; i++) {
	    SG_ris[SG_N-i] = ris[nc-i]; 
	  }





	  zerores =    fff[nc]   ; 
	  /* printf(" zerores=%e \n", zerores ); */

	  for(i=0; i<=SG_N/2 ; i++) {
	    SG_freq[i] =  centerE + i* SG_step  ; 
	  }
	  for(i=1; i<SG_N/2 ; i++) {
	    SG_freq[SG_N-i] = centerE  -i* SG_step  ; 
	  }

	  /* ---------------FFT SG_ris; -------------------- */


	  fftin     =  (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * SG_N );
	  fftp_for  =   fftw_plan_dft_1d(SG_N, fftin, fftin, FFTW_FORWARD  , FFTW_ESTIMATE);
	  fftp_rev  =   fftw_plan_dft_1d(SG_N, fftin, fftin, FFTW_BACKWARD , FFTW_ESTIMATE);

	  for(i=0; i<SG_N; i++ ) {
	    ((double *) fftin)[2*i] =   SG_ris[  i] ; 
	    ((double *) fftin)[2*i+1] =   0.0           ; 
	  }

	  fftw_execute( fftp_for ); 

	  for(i=0; i<SG_N; i++ ) {
	    SG_ris[  i] =  ((double *) fftin)[i]   ; 
	  }
	  
	  /* ----------------------- */

	}

/*	for (j=0;j<ntotpar;j++)
		MNERRS(j+1,err_p[j],err_m[j],err_par[j],gcor[j]); */

	if (fl_stop_fit)
	{
		*fcnval = 0;
		return;
	}

	BG = xval[0];
	SL = xval[1];
	ZR = xval[2];
	if (fl_t_as_param)
	{
		temp = xval[3];
		GR = 2;
	}
	else
	{
		GR = xval[3];
	}
	PC = xval[4];
	_GC = xval[5];
	for (i=0;i<nex;i++)
	{
	   j=6+3*i;
	   P[i] = xval[j];
	   O[i] = xval[j+1];
	   G[i] = xval[j+2];
	}
	if (nb_visc)
	{
		for (j=0;j<10;j++)
		{
			V[j] = xval[6+3*nex+j];
		}
	}
	TK = bol*temp;

	/* Initialization */
	memset(tcn,0,nn*sizeof(double));
	memset(tcnc,0,nn*sizeof(double));
	memset(tcnv,0,nn*sizeof(double));
	memset(tcne2,0,nn*sizeof(double));
	for (i=0;i<nex;i++)
		memset(tcne[i],0,nn*sizeof(double));


	

	if (fl_convol_type==3) /* P-V */
	{
		for (i=0;i<nn;i++)
		  {
		    wj = frq[i]-ZR;
		    if (wj == 0)
		      bose = 1;
		    else
		      bose = wj/(1-exp(-wj/TK));
		    tcnc[i] = PC*bose*pseudo_voigt(wj);
		  }
	}
	else
	  {
	    /* Convolution - Central line */
	    dummy = GR/1000000000;
	    if (_GC < dummy)
	      {
		fatt = pig*PC*_GC;
		for (i=0;i<nn;i++)
		  {
		    wj = frq[i]-ZR;
		    tcnc[i] = fatt*GR/pig/(GR*GR+wj*wj);
		  }
	      }
	    else
	      {

			  
		if(was_fft_active  &&  strcasecmp(centtype,"del")     ){
		  for(i=0; i< SG_N  ; i++) {
		    dw = SG_freq[i];
		    if (dw == 0)
		      bose = 1;
		    else
		      bose = dw/(1-exp(-dw/TK));
		    
		    if (!strcasecmp(centtype,"lor")) {
		      thc=PC*_GC*_GC/(dw*dw+_GC*_GC);
		    } else if (!strcasecmp(centtype,"del")) {
		      thc *= ris[k]*PC*stepr;  /* meaningless */
		    }
		    if (fl_cen_bose)
		      thc *= bose;
		    SG_calc[i] = thc ; 
		  }
		  
		  
		  
		  FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ;
		  
		  interpola( nn , frq, tcnc, centerE, ZR, zerores, SG_N, SG_calc, SG_step);
		  
		}else {
		  if (!strcasecmp(centtype,"lor"))
		    {
		      
		      for (i=0;i<nn;i++)
			{
			  
			  wj = frq[i]-ZR;
			  for (j=-nc;j<=nc;j++)
			    {
			      k = j+nc;
			      wk = fff[k];
			      dw = wj-wk;
			      if (dw == 0)
				bose = 1;
			      else
				bose = dw/(1-exp(-dw/TK));
			      thc=PC*_GC*_GC/(dw*dw+_GC*_GC);
			      thc *= ris[k]*stepr;
			      if (fl_cen_bose)
				thc *= bose;
			      tcnc[i] += thc;
			    }
			}
		    }
		  else if (!strcasecmp(centtype,"del"))
		    {


		      if( was_fft_active) {

			for (j=0;j<nn;j++) /* Loop over all experimental points */
			  {
			    wj=frq[j]-ZR;
			
			    wk = wj;
			    k  = (wk-fff[0])*(2.0*nc)/(fff[2*nc]-fff[0]); 
			    
			    			   
			    if(k>=0 && k<=2*nc) {
			      ka=k;
			      while(fff[ka]>wk) {
				ka--;
				if(ka<0) break;
			      }
			      if(ka<0) break;
			      kb=ka;
			      while(fff[kb]<wk) {
				kb++;
				if(kb>2*nc) break;
			      }
			      if(kb>2*nc) break;
			      thc = PC* ( ris[ka]*(fff[kb]-wk)+  ris[kb] *(wk-fff[ka]))/(fff[kb]-fff[ka])*stepr;
			      tcnc[j] += thc;
			    }
			  } 
		      } else {
			for (i=0;i<nn;i++)
			  {
			  
			    wj=frq[i]-ZR;
			    for (j=-nc;j<=nc;j++)
			      {
				k = j+nc;
				wk = fff[k];
				dw = wj-wk;
				if (dw == 0)
				  bose = 1;
				else
				  bose = dw/(1-exp(-dw/TK));
				if (dw*dw <= stepq)
				  {
				    thc *= ris[k]*PC*stepr;
				    if (fl_cen_bose)
				      thc *= bose;
				    tcnc[i] += thc;
				  }
			      }
			  }
		      }
		    }
		}
	      }
	  }
	    
	if (!no_convolution() && nb_visc) {
	  
	  if(was_fft_active){
	    for(i=0; i< SG_N  ; i++) {
	      dw = SG_freq[i];
	      if (dw == 0)
		bose = 1;
	      else
		bose = dw/(1-exp(-dw/TK));
	      thc = visco_calc(dw);
	      thc *= bose ;
	      SG_calc[i] = thc ; 
	    }
	    


	    FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ;

	    interpola( nn , frq, tcnv, centerE, ZR, zerores, SG_N, SG_calc, SG_step);

	    for (i=0;i<nn;i++)
	      {      
		if (fl_visco_e2) {
		  wj = frq[i]-ZR;
		  tcne2[i] = visco_calc(wj)*wj*wj;
		}
	      }

	  }else {
	    for (i=0;i<nn;i++)
	      {      
		wj = frq[i]-ZR;
		sum=0.0;
		for (j=-nc;j<=nc;j++)
		  {
		    k = j+nc;
		    wk = fff[k];
		    dw = wj-wk;
		    if (dw == 0)
		      bose = 1;
		    else
		      bose = dw/(1-exp(-dw/TK));
		    thc = visco_calc(dw);
		    thc *= ris[k]*bose*stepr;
		    tcnv[i] += thc;
		    sum+= ris[k]*stepr;
		  }

		if(0) {
		  dw = wj-0;
		  if (dw == 0)
		    bose = 1;
		  else
		    bose = dw/(1-exp(-dw/TK));
		  thc = visco_calc(dw);
		  thc *= bose;
		  tcnv[i] = thc;
		}
		if (fl_visco_e2)
		  tcne2[i] = visco_calc(wj)*wj*wj;
	      }
	  } 
	}

	/* Convolution with the excitations */
	for (iex=0;iex<nex;iex++) /* Loop over each excitation */
	{
	  if (fl_convol_type==3)
	    {
	      for (i=0;i<nn;i++)
		{
		  wj = frq[i]-ZR;
		  dw = O[iex]-fabs(wj);
		  if (wj == 0)
		    bose = 1;
		  else
		    bose = wj/(1-exp(-wj/TK));
		  tcne[iex][i] +=P[iex]*pseudo_voigt(dw)*bose;
		}
	    }
	  else
	    {
	      /* DHO */
	      if (!strcasecmp(ex[iex],"dho"))
		{
		  if(was_fft_active){
		    for(i=0; i< SG_N  ; i++) {
		      dw = SG_freq[i];
		      if (dw == 0)
			bose = 1;
		      else
			bose = dw/(1-exp(-dw/TK));
		      thc = P[iex]*G[iex]*O[iex]/
			(pow(O[iex]*O[iex]-dw*dw,2)+G[iex]*G[iex]*dw*dw);
		      thc *= bose ;
		      SG_calc[i] = thc ; 
		    }
		    FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ;
		    interpola( nn , frq,&(tcne[iex][0]) , centerE, ZR, zerores, SG_N, SG_calc, SG_step);
		  }else {
		    
		    for (j=0;j<nn;j++) /* Loop over all experimental points */
		      {
			wj=frq[j]-ZR;
			for (i=-nc;i<=nc;i++)
			  {
			    k = i+nc;
			    wk = fff[k];
			    dw = wj-wk;
			    if (dw == 0)
			      bose = 1;
			    else
			      bose = dw/(1-exp(-dw/TK));
			    thc = P[iex]*G[iex]*O[iex]/
			      (pow(O[iex]*O[iex]-dw*dw,2)+G[iex]*G[iex]*dw*dw);
			    thc *= ris[k]*bose*stepr;
			    tcne[iex][j] += thc;
			  }
		      }
		  }
		}
	      /* Lorenzian */
	      else if (!strcasecmp(ex[iex],"lor"))
		{
		  if(was_fft_active){
		    for(i=0; i< SG_N  ; i++) {
		      dw = SG_freq[i];
		      if (dw == 0)
			bose = 1;
		      else
			bose = dw/(1-exp(-dw/TK));
		      thc = (P[iex]*G[iex]*G[iex]/
			     (pow(dw-O[iex],2)+G[iex]*G[iex])
			     +P[iex]*G[iex]*G[iex]/
			     (pow(dw+O[iex],2)+G[iex]*G[iex]));
		      thc *= bose ;
		      SG_calc[i] = thc ; 
		    }
		    FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ;
		    interpola( nn , frq,&(tcne[iex][0]) , centerE, ZR, zerores, SG_N, SG_calc, SG_step);
		  }else {
		    for (j=0;j<nn;j++) /* Loop over all experimental points */
		      {
			tcne[iex][j]=0;
			wj=frq[j]-ZR;
			for (i=-nc;i<=nc;i++)
			  {
			    k = i+nc;
			    wk = fff[k];
			    dw = wj-wk;
			    if (dw == 0)
			      bose = 1;
			    else
			      bose = dw/(1-exp(-dw/TK));
			    thc = (P[iex]*G[iex]*G[iex]/
				   (pow(dw-O[iex],2)+G[iex]*G[iex])
				   +P[iex]*G[iex]*G[iex]/
				   (pow(dw+O[iex],2)+G[iex]*G[iex]));
			    thc *= ris[k]*bose*stepr;
			    tcne[iex][j] += thc;
			  }
		      }
		  }
		}
	      /* Gaussian */
	      else if (!strcasecmp(ex[iex],"gau"))
		{ 
		  if(was_fft_active){
		    for(i=0; i< SG_N  ; i++) {
		      dw = SG_freq[i];
		      if (dw == 0)
			bose = 1;
		      else
			bose = dw/(1-exp(-dw/TK));

		      thc= (P[iex]/G[iex]/sqrt(pig/2)*
				   exp(-pow(dw-O[iex],2)/(2.0*G[iex]*G[iex]))
				   + P[iex]/G[iex]/sqrt(pig/2)*
				   exp(-pow(dw+O[iex],2)/(2.0*G[iex]*G[iex])));

		      thc *= bose ;
		      SG_calc[i] = thc ; 
		    }
		    FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ;
		    interpola( nn , frq,&(tcne[iex][0]) , centerE, ZR, zerores, SG_N, SG_calc, SG_step);
		  }else {
		    for (j=0;j<nn;j++) /* Loop over all experimental points */
		      {
			
			tcne[iex][j]=0;
			wj=frq[j]-ZR;
			for (i=-nc;i<=nc;i++)
			  {
			    k = i+nc;
			    wk = fff[k];
			    dw = wj-wk;
			    if (dw == 0)
			      bose = 1;
			    else
			      bose = dw/(1-exp(-dw/TK));
			    thc = (P[iex]/G[iex]/sqrt(pig/2)*
				   exp(-pow(dw-O[iex],2)/(2.0*G[iex]*G[iex]))
				   + P[iex]/G[iex]/sqrt(pig/2)*
				   exp(-pow(dw+O[iex],2)/(2.0*G[iex]*G[iex])));
			    thc *= ris[k]*bose*stepr;
			    tcne[iex][j] += thc;
			  }
		      }
		  }
		}
	      
	      /* Delta */
	      else if (!strcasecmp(ex[iex],"del"))
		{
		  if( was_fft_active){
		    for (j=0;j<nn;j++) /* Loop over all experimental points */
		      {
			tcne[iex][j]=0;
			wj=frq[j]-ZR;


			dw = O[iex];
			if (dw == 0)
			  bose = 1;
			else
			  bose = dw/(1-exp(-dw/TK));
			
			wk = wj-dw;
			k  = (wk-fff[0])*(2.0*nc)/(fff[2*nc]-fff[0]); 



			if(k>=0 && k<=2*nc) {
			  ka=k;
			  while(fff[ka]>wk) {
			    ka--;
			    if(ka<0) break;
			  }
			  if(ka<0) break;

			  kb=ka;
			  while(fff[kb]<wk) {
			    kb++;
			    if(kb>2*nc) break;
			  }
			  if(kb>2*nc) break;

			  thc = P[iex]*( ris[ka]*(fff[kb]-wk)+  ris[kb] *(wk-fff[ka]))/(fff[kb]-fff[ka])*bose*stepr;
			  tcne[iex][j] += thc;
			}

			dw = -O[iex];
			if (dw == 0)
			  bose = 1;
			else
			  bose = dw/(1-exp(-dw/TK));
			
			wk = wj-dw;
			k  = (wk-fff[0])*(2.0*nc)/(fff[2*nc]-fff[0]); 

			if(k>=0 && k<=2*nc) {

			  ka=k;
			  while(fff[ka]>wk) {
			    ka--;
			    if(ka<0) break;
			  }
			  if(ka<0) break;
			  
			  kb=ka;
			  while(fff[kb]<wk) {
			    kb++;
			    if(kb>2*nc) break;
			  }
			  if(kb>2*nc) break;

			  thc = P[iex]*(ris[ka]*(fff[kb]-wk)+ris[kb]*(wk-fff[ka]))/(fff[kb]-fff[ka])*bose*stepr;
			  tcne[iex][j] += thc;
			}
		      } 

/* 		    for(i=0; i< SG_N  ; i++) { */
/* 		      dw = SG_freq[i]; */
/* 		      if (dw == 0) */
/* 			bose = 1; */
/* 		      else */
/* 			bose = dw/(1-exp(-dw/TK)); */

/* 		      dist = pow(dw+O[iex],2); */
/* 		      thc=0; */
/* 		      if (dist <= stepq) */
/* 			{ */
/* 			  thc = P[iex]*bose; */
/* 			} */
/* 		      dist = pow(dw-O[iex],2); */
/* 		      if (dist <= stepq) */
/* 			{ */
/* 			  thc = P[iex]*bose; */
/* 			} */

/* 		      thc *= bose ; */
/* 		      SG_calc[i] = thc ;  */
/* 		    } */
/* 		    FFT_convolve(SG_N,   SG_calc,SG_ris,  fftin, fftp_for, fftp_rev, centerE     ) ; */
/* 		    interpola( nn , frq,&(tcne[iex][0]) , centerE, ZR, zerores, SG_N, SG_calc, SG_step); */
		  }else {
		    for (j=0;j<nn;j++) /* Loop over all experimental points */
		      {
			tcne[iex][j]=0;
			wj=frq[j]-ZR;



			for (i=-nc;i<=nc;i++)
			  {
			    k = i+nc;
			    wk = fff[k];
			    dw = wj-wk;
			    if (dw == 0)
			      bose = 1;
			    else
			      bose = dw/(1-exp(-dw/TK));
			    dist = pow(dw+O[iex],2);
			    if (dist <= stepq)
			      {
				thc = P[iex];
			      }
			    dist = pow(dw-O[iex],2);
			    if (dist <= stepq)
			      {
				thc = P[iex];
			      }
			    thc *= ris[k]*bose*stepr;
			    tcne[iex][j] += thc;
			  }



		      }
		  }
		}
	    }
	}
	chi2 = 0;
	/* Setup theoretical function */
	for (j=0;j<nn;j++)
	  {
	    tcn[j] = tcnc[j]+tcnv[j]+BG+frq[j]*SL;
	    for (iex=0;iex<nex;iex++)
	      tcn[j] += tcne[iex][j];
	    chi2 += pow(dat[j]-tcn[j],2)/pow(erd[j],2);
	  }
	
	ftime(&this_timeb);
	if (fl_live && !no_convolution() &&
	    ((1000*this_timeb.time+this_timeb.millitm)-(1000*live_timeb.time+live_timeb.millitm)
	     >= 1000*live_interval))
	  {
	    sync_curves(0);
		plot_do_file();
		ftime(&live_timeb);
	  }
	
	if (fl_preview)
	  printf("Preview chi2 = %.1f\n",chi2);
	*fcnval = chi2;
	
	if(was_fft_active) {
	  free(SG_ris );
	  free(SG_calc );
	  free( SG_freq );
	  fftw_destroy_plan(fftp_for);
	  fftw_destroy_plan(fftp_rev);
	  fftw_free(fftin); 
	  
	}
}

