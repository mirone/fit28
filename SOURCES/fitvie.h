
void kill_current_fit (void);
void sync_curves (int last);
double visco_calc (double dw);
int visco_local_max (int i);
double pseudo_voigt (double dw);
void close_fit_procedure (void);
void main_fit (void);
gboolean no_convolution (void);
void reset_fit_button (void);
