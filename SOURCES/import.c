/*
 * import.c - A. BERAUD  (September 2004)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <errno.h>

#include "import.h"
#include "interface.h"
#include "callbacks.h"
#include "simplex.h"

static double imp_enrg[5] = {13.840,15.816,17.794,21.7477,25.75};
static int imp_hkl[5] = {7,8,9,11,13};


GtkWidget *import_win,*import_vbox,*import_label;
GtkWidget *import_hbox_files,*import_files_clist,*import_hbox_files_buttons;
GtkWidget *import_vbox_files_tools,*import_load_button,*import_remove_button;
GtkWidget *import_hbox_shift,*import_shift_entry,*import_guess_button;
GtkWidget *import_hbox_buttons,*import_close_button;
GtkWidget *import_file_selection;
GtkWidget *import_hbox_reflex,*import_reflex_menu,*import_toggle_norm;
GtkWidget *import_hbox_ana,*import_ana1,*import_ana2,*import_ana3,*import_ana4,*import_ana5;
GtkWidget *import_hbox_actions_buttons,*import_center_button,*import_sum_button;
GtkWidget *import_hbox_sname,*import_sname_entry;
import_bloc *imp_data;
char *import_fname,*import_small_fname;
int imp_refl,imp_ana,imp_norm,imp_nb,imp_nb_sum,imp_show_fit,imp_show_sum;
int blocs_size;
double imp_de[10000],imp_cts[10000],imp_mon[10000];
double *imp_de_sum,*imp_cts_sum,*imp_depth_sum,*imp_mon_sum;
double imp_par[3],imp_delta[3],imp_step;


/* Self explanatory. */
void create_import_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	char tmpstr[100];

/*	if (import_win && (GTK_WIDGET_REALIZED(import_win)))
		return; */

	blocs_size = 0;
	imp_ana = 1;
	imp_norm = 0;
	imp_show_fit = 0;
	imp_show_sum = 0;

	import_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(import_win),"Import file from NEWPLOT");
	gtk_signal_connect(GTK_OBJECT(import_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(import_win));
	gtk_signal_connect(GTK_OBJECT(import_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(import_win));

	import_vbox=gtk_vbox_new(FALSE,2);
	gtk_widget_ref(import_vbox);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_vbox",import_vbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_container_add(GTK_CONTAINER(import_win),import_vbox);

	import_hbox_files = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_files);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_files",import_hbox_files,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_files);
	gtk_box_pack_start(GTK_BOX(import_vbox),import_hbox_files,FALSE,FALSE,0);

	import_files_clist = gtk_clist_new(1);
	gtk_widget_ref(import_files_clist);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_files_clist",import_files_clist,(GtkDestroyNotify)gtk_widget_unref);
	gtk_clist_set_selection_mode(import_files_clist,GTK_SELECTION_MULTIPLE);
	gtk_clist_set_shadow_type(import_files_clist,GTK_SHADOW_IN);
	gtk_widget_set_usize(import_files_clist,100,100);
	gtk_signal_connect(GTK_OBJECT(import_files_clist),"select_row",
                   GTK_SIGNAL_FUNC(import_select_row),1);
	gtk_signal_connect(GTK_OBJECT(import_files_clist),"unselect_row",
                   GTK_SIGNAL_FUNC(import_select_row),0);
	gtk_widget_show(import_files_clist);
	gtk_box_pack_start(GTK_BOX(import_hbox_files),import_files_clist,TRUE,TRUE,0);

	import_vbox_files_tools = gtk_vbox_new(FALSE,0);
	gtk_widget_ref(import_vbox_files_tools);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_vbox_files_tools",import_vbox_files_tools,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_vbox_files_tools);
	gtk_box_pack_start(GTK_BOX(import_hbox_files),import_vbox_files_tools,FALSE,FALSE,0);

	import_hbox_files_buttons = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_files_buttons);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_files_buttons",import_hbox_files_buttons,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_files_buttons);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_files_buttons,FALSE,FALSE,0);

	import_load_button=gtk_button_new_with_label("Add file");
	gtk_widget_ref(import_load_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_load_button",import_load_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_load_button);
	gtk_signal_connect(GTK_OBJECT(import_load_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_load_activate),NULL);
	gtk_box_pack_start(GTK_BOX(import_hbox_files_buttons),import_load_button,TRUE,TRUE,0);

	import_remove_button=gtk_button_new_with_label("Remove file");
	gtk_widget_ref(import_remove_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_remove_button",import_remove_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_remove_button);
	gtk_signal_connect(GTK_OBJECT(import_remove_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_remove_activate),NULL);
	gtk_box_pack_start(GTK_BOX(import_hbox_files_buttons),import_remove_button,TRUE,TRUE,0);

	import_hbox_shift = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_shift);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_shift",import_hbox_shift,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_shift);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_shift,FALSE,FALSE,0);

	import_label=gtk_label_new("Shift:");
	gtk_box_pack_start(GTK_BOX(import_hbox_shift),import_label,FALSE,FALSE,0);

	import_shift_entry = gtk_entry_new();
	gtk_widget_ref(import_shift_entry);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_shift_entry",import_shift_entry,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)import_shift_entry,"0");
	gtk_widget_show(import_shift_entry);
	gtk_box_pack_start(GTK_BOX(import_hbox_shift),import_shift_entry,FALSE,FALSE,0);

	import_guess_button=gtk_button_new_with_label("Guess");
	gtk_widget_ref(import_guess_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_guess_button",import_guess_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_guess_button);
	gtk_signal_connect(GTK_OBJECT(import_guess_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_guess_activate),NULL);
	gtk_box_pack_start(GTK_BOX(import_hbox_shift),import_guess_button,TRUE,TRUE,0);

	import_hbox_reflex = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_reflex);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_reflex",import_hbox_reflex,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_reflex);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_reflex,FALSE,FALSE,0);

	import_label=gtk_label_new("Reflexion:");
	gtk_box_pack_start(GTK_BOX(import_hbox_reflex),import_label,FALSE,FALSE,0);

	import_reflex_menu = gtk_option_menu_new();
	gtk_widget_ref(import_reflex_menu);
	menu = gtk_menu_new();

	menu_item = gtk_menu_item_new_with_label ("7 7 7");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_import_reflex_activate,"0");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("8 8 8");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_import_reflex_activate,"1");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("9 9 9");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_import_reflex_activate,"2");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("11 11 11");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_import_reflex_activate,"3");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("13 13 13");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)on_import_reflex_activate,"4");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);

	gtk_option_menu_set_menu(GTK_OPTION_MENU(import_reflex_menu),menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(import_reflex_menu),0);
	gtk_widget_show(import_reflex_menu);
	gtk_box_pack_start(GTK_BOX(import_hbox_reflex),import_reflex_menu,FALSE,FALSE,0);

	import_toggle_norm = gtk_toggle_button_new_with_label("Norm.");
	gtk_widget_ref(import_toggle_norm);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_toggle_norm",import_toggle_norm,(GtkDestroyNotify)gtk_widget_unref);
	gtk_signal_connect(GTK_OBJECT(import_toggle_norm),"clicked",
		GTK_SIGNAL_FUNC(on_import_norm_toggle),"2");
	gtk_widget_show(import_toggle_norm);
	gtk_box_pack_start(GTK_BOX(import_hbox_reflex),import_toggle_norm,TRUE,TRUE,0);

	import_hbox_ana = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_ana);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_ana",import_hbox_ana,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_ana);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_ana,FALSE,FALSE,0);

	import_label=gtk_label_new("Analyzer:");
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_label,FALSE,FALSE,0);

	import_ana1 = gtk_radio_button_new_with_label(NULL,"1");
	gtk_signal_connect(GTK_OBJECT(import_ana1),"clicked",
		GTK_SIGNAL_FUNC(on_import_ana_toggle),"1");
	if (imp_ana == 1)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(import_ana1),TRUE);
	gtk_widget_show(import_ana1);
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_ana1,TRUE,TRUE,0);

	import_ana2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(import_ana1),"2");
	gtk_signal_connect(GTK_OBJECT(import_ana2),"clicked",
		GTK_SIGNAL_FUNC(on_import_ana_toggle),"2");
	if (imp_ana == 2)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(import_ana2),TRUE);
	gtk_widget_show(import_ana2);
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_ana2,TRUE,TRUE,0);

	import_ana3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(import_ana1),"3");
	gtk_signal_connect(GTK_OBJECT(import_ana3),"clicked",
		GTK_SIGNAL_FUNC(on_import_ana_toggle),"3");
	if (imp_ana == 3)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(import_ana3),TRUE);
	gtk_widget_show(import_ana3);
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_ana3,TRUE,TRUE,0);

	import_ana4 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(import_ana1),"4");
	gtk_signal_connect(GTK_OBJECT(import_ana4),"clicked",
		GTK_SIGNAL_FUNC(on_import_ana_toggle),"4");
	if (imp_ana == 4)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(import_ana4),TRUE);
	gtk_widget_show(import_ana4);
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_ana4,TRUE,TRUE,0);

	import_ana5 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(import_ana1),"5");
	gtk_signal_connect(GTK_OBJECT(import_ana5),"clicked",
		GTK_SIGNAL_FUNC(on_import_ana_toggle),"5");
	if (imp_ana == 5)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(import_ana5),TRUE);
	gtk_widget_show(import_ana5);
	gtk_box_pack_start(GTK_BOX(import_hbox_ana),import_ana5,TRUE,TRUE,0);

	import_hbox_sname = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_sname);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_sname",import_hbox_sname,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_sname);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_sname,FALSE,FALSE,0);

	import_label=gtk_label_new("Target file:");
	gtk_box_pack_start(GTK_BOX(import_hbox_sname),import_label,FALSE,FALSE,0);

	import_sname_entry = gtk_entry_new();
	gtk_widget_ref(import_sname_entry);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_sname_entry",import_sname_entry,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)import_sname_entry,"dummy.dat");
	gtk_widget_show(import_sname_entry);
	gtk_box_pack_start(GTK_BOX(import_hbox_sname),import_sname_entry,FALSE,FALSE,0);

	import_hbox_actions_buttons = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_actions_buttons);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_actions_buttons",import_hbox_actions_buttons,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_actions_buttons);
	gtk_box_pack_start(GTK_BOX(import_vbox_files_tools),import_hbox_actions_buttons,FALSE,FALSE,0);

	import_center_button=gtk_button_new_with_label("Center");
	gtk_widget_ref(import_center_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_center_button",import_center_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_center_button);
	gtk_signal_connect(GTK_OBJECT(import_center_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_center_activate),NULL);
	gtk_box_pack_start(GTK_BOX(import_hbox_actions_buttons),import_center_button,TRUE,TRUE,0);

	import_sum_button=gtk_button_new_with_label("Sum");
	gtk_widget_ref(import_sum_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_sum_button",import_sum_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_sum_button);
	gtk_signal_connect(GTK_OBJECT(import_sum_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_sum_activate),NULL);
	gtk_box_pack_start(GTK_BOX(import_hbox_actions_buttons),import_sum_button,TRUE,TRUE,0);

	import_hbox_buttons = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(import_hbox_buttons);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_hbox_buttons",import_hbox_buttons,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_hbox_buttons);
	gtk_box_pack_start(GTK_BOX(import_vbox),import_hbox_buttons,FALSE,FALSE,0);

	import_close_button=gtk_button_new_with_label("Close");
	gtk_widget_ref(import_close_button);
	gtk_object_set_data_full(GTK_OBJECT(import_win),"import_close_button",import_close_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(import_close_button);
	gtk_signal_connect(GTK_OBJECT(import_close_button),"clicked",
		GTK_SIGNAL_FUNC(on_import_close_activate),import_win);
	gtk_box_pack_start(GTK_BOX(import_hbox_buttons),import_close_button,TRUE,TRUE,0);

	gtk_widget_show_all(import_win);

	open_gnuplot();
} 

void on_import_close_activate (GtkWidget *item, gpointer user_data)
{
	close_gnuplot();
	gtk_widget_destroy(GTK_WIDGET(import_win));
}

void on_import_center_activate (GtkWidget *item, gpointer user_data)
{
	int i,j;
	double shift;
	char shtxt[300];

	strcpy(shtxt,gtk_entry_get_text(GTK_ENTRY(import_shift_entry)));
	shift = atof(shtxt);
	for (i=0;i<blocs_size;i++)
	{
		if (!imp_data[i].used)
			continue;
		if (!imp_data[i].selected)
			continue;
		for (j=0;j<imp_data[i].size;j++)
		{
			imp_data[i].point[j].dt[imp_ana-1] -= shift;
		}
	}
	import_display_curves();
}

void import_compute_average_step (void)
{
	int i,j,n;
	double step = 0;

	n = 0;
	for (i=0;i<blocs_size;i++)
	{
		if (!imp_data[i].used)
			continue;
		if (!imp_data[i].selected)
			continue;
		for (j=0;j<imp_data[i].size-1;j++)
		{
			step += energy_scale(imp_data[i].point[j+1].dt[imp_ana-1])
				- energy_scale(imp_data[i].point[j].dt[imp_ana-1]);
			n++;
		}
	}

	imp_step = step/n;
}

/*void import_compute_smallest_step (void)
{
	int i;

	imp_step = imp_de[1]-imp_de[0];
	for (i=1;i<imp_nb-1;i++)
	{
		if ((imp_de[i+1]-imp_de[i])<imp_step)
			imp_step = imp_de[i+1]-imp_de[i];
	}
} */

void on_import_sum_activate (GtkWidget *item, gpointer user_data)
{
	int i,j,n,nb,tst;
	char fname[300],tmpstr[300];
	FILE *fil;
	double tmpde,tmpcts,tmpmon,tmpdiff,meani1;
	typedef struct {
		double de,cts,mon;
		int set;
	} sum_point;
	sum_point imp_point[10000];
	sum_point tmp_point;

	meani1 = 0;
	n = 0;
	for (i=0;i<blocs_size;i++)
	{
		if (!imp_data[i].used)
			continue;
		if (!imp_data[i].selected)
			continue;
		for (j=0;j<imp_data[i].size;j++)
		{
			imp_point[n].de = energy_scale(imp_data[i].point[j].dt[imp_ana-1]);
			imp_point[n].mon = imp_data[i].point[j].i1;
			imp_point[n].cts = imp_data[i].point[j].deta[imp_ana-1]
				/imp_point[n].mon;
			imp_point[n].set = i;
			meani1 += imp_point[n].mon;
			n++;
		}
	}
	imp_nb = n;
	meani1 /= imp_nb;
	tst = 1;
	while (tst == 1)
	{
		tst = 0;
		for (i=0;i<imp_nb-1;i++)
		{
			if (imp_point[i].de>imp_point[i+1].de)
			{
				memcpy(&tmp_point,&imp_point[i],sizeof(sum_point));
				memcpy(&imp_point[i],&imp_point[i+1],sizeof(sum_point));
				memcpy(&imp_point[i+1],&tmp_point,sizeof(sum_point));
				tst = 1;
			}
		}	
	}
	import_compute_average_step();
	imp_nb_sum = (int) ((imp_point[imp_nb-1].de-imp_point[0].de)/imp_step);
	imp_de_sum = (double *) malloc(imp_nb_sum*sizeof(double));
	imp_cts_sum = (double *) malloc(imp_nb_sum*sizeof(double));
	memset(imp_cts_sum,0,imp_nb_sum*sizeof(double));
	imp_depth_sum = (double *) malloc(imp_nb_sum*sizeof(double));
	memset(imp_depth_sum,0,imp_nb_sum*sizeof(double));
	imp_mon_sum = (double *) malloc(imp_nb_sum*sizeof(double));
	memset(imp_mon_sum,0,imp_nb_sum*sizeof(double));
	for (i=0;i<imp_nb_sum;i++)
		imp_de_sum[i] = imp_point[0].de+i*imp_step;

	for (i=0;i<imp_nb-1;i++)
	{
		j = (int) ((imp_point[i].de-imp_point[0].de)/imp_step);
		if (j>=imp_nb_sum)
			j = imp_nb_sum - 1;
		imp_cts_sum[j] += imp_point[i].cts;
		imp_mon_sum[j] += imp_point[i].mon;
		/* Find previous of same set */
		n = i-1;
		while (n>=0)
		{
			if (imp_point[n].set == imp_point[i].set)
				break;
			n--;
		}
		if (n>=0)
		{
			tmpde = imp_point[i].de-imp_de_sum[j];
			tmpdiff = imp_point[i].de-imp_point[n].de;
			if (tmpdiff == 0)
				tmpdiff = 1E-6;
			tmpcts = tmpde*(imp_point[i].cts-imp_point[n].cts)/tmpdiff;
			imp_cts_sum[j] -= tmpcts;
			tmpmon = tmpde*(imp_point[i].mon-imp_point[n].mon)/tmpdiff;
			imp_mon_sum[j] -= tmpmon;
		}
		imp_depth_sum[j]++;
	}

/*	imp_cts_sum[0] = imp_point[0].cts;
	imp_cts_sum[imp_nb_sum-1] = imp_point[imp_nb-1].cts;
	imp_mon_sum[0] = imp_point[0].mon;
	imp_mon_sum[imp_nb_sum-1] = imp_point[imp_nb-1].mon; */
	for (i=0;i<imp_nb_sum;i++)
	{
		if (imp_depth_sum[i]>0)
		{
			imp_cts_sum[i] *= meani1/imp_depth_sum[i];
			imp_mon_sum[i] /= imp_depth_sum[i];
		}
	}
	/* Fill in the empty cells. */
	j = -1;
	n = 0;
	for (i=1;i<imp_nb_sum;i++)
	{
		if ((imp_cts_sum[i]<=0) && (i!=imp_nb_sum-1))
		{
			if (j==-1)
			{
				j = i;
				n = 1;
				tmpde = imp_cts_sum[i-1];
				tmpmon = imp_mon_sum[i-1];
			}
			else
				n++;
		}
		else
		{
			if (j!=-1)
			{
				if (n<=5)
				{
					while (j<i)
					{
						imp_cts_sum[j] =
							imp_cts_sum[j-1] +
							(imp_cts_sum[i]-tmpde)/n;
						imp_mon_sum[j] =
							imp_mon_sum[j-1] +
							(imp_mon_sum[i]-tmpmon)/n;
						j++;
					}
				}
			}
			j = -1;
			n = 0;
		}
	}

	strcpy(fname,gtk_entry_get_text(GTK_ENTRY(import_sname_entry)));
	fil = fopen(fname,"w");
	for (i=0;i<imp_nb_sum;i++)
	{
/*		if (!imp_cts_sum[i])
			continue; */
		sprintf(tmpstr,"%g %g %g 0 0 %g\n",imp_de_sum[i],imp_cts_sum[i],
			sqrt(fabs(imp_cts_sum[i])+1),imp_mon_sum[i]);
		fputs(tmpstr,fil);
	}
	fclose(fil);
	printf("File [%s] written.\n",fname);
	imp_show_sum = 1;
	import_display_curves();
}

void import_load_select_ok (GtkWidget *w, GtkFileSelection *fs)
{
	char *tmp1,*tmp2,tmp3[300];

	import_fname=g_strdup(gtk_file_selection_get_filename(fs));
	gtk_widget_destroy(GTK_WIDGET(import_file_selection));
	import_small_fname = g_basename(import_fname);
	if (check_list_for_file(import_small_fname))
		return;
	tmp1 = g_dirname(import_fname);
	tmp2 = g_get_current_dir();
	g_rel2abs(tmp1,tmp2,tmp3,500);
	strcat(tmp3,"/");
	strcat(tmp3,import_small_fname);
	import_fname =g_strdup(tmp3);
	gtk_clist_append(GTK_CLIST(import_files_clist),&import_small_fname);
	import_load_file_into_data(import_fname,import_small_fname);
}

/* This function looks for free blocs in between used blocs. */
int find_free_import_data (void)
{
	int i;
	for (i=0;i<blocs_size;i++)
	{
		if (imp_data[i].used == 0)
			return i;
	}
	blocs_size++;
	imp_data = (import_bloc *) realloc(imp_data,blocs_size*sizeof(import_bloc));
	return blocs_size-1;
}

int find_import_data_from_fname (char *fname)
{
	for (i=0;i<blocs_size;i++)
	{
		if (imp_data[i].used == 0)
			continue;
		if (!strcasecmp(imp_data[i].fname,fname))
			return i;
	}
	return -1;
}

void import_load_file_into_data (char *fname, char *sfname)
{
	int pt,i=0;
	char tmpstr[BUFSIZ];
	FILE *fil;

	fil = fopen(fname,"r");
	if (!fil)
		return;
	pt = find_free_import_data();
	if (pt == -1)
		return;
	while (fgets(tmpstr,BUFSIZ,fil))
	{
		sscanf(tmpstr,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
			&imp_data[pt].point[i].i0,&imp_data[pt].point[i].i1,
			&imp_data[pt].point[i].tm,&imp_data[pt].point[i].dt[0],
			&imp_data[pt].point[i].dt[1],&imp_data[pt].point[i].dt[2],
			&imp_data[pt].point[i].dt[3],&imp_data[pt].point[i].dt[4],
			&imp_data[pt].point[i].deta[0],&imp_data[pt].point[i].deta[1],
			&imp_data[pt].point[i].deta[2],&imp_data[pt].point[i].deta[3],
			&imp_data[pt].point[i].deta[4]);
		i++;
	}
	imp_data[pt].size = i;
	imp_data[pt].used = 1;
	imp_data[pt].selected = 0;
	strcpy(imp_data[pt].fname,sfname);
	fclose(fil);
	return pt;
}

void on_import_load_activate (GtkWidget *item, gpointer user_data)
{
	import_file_selection=gtk_file_selection_new ("Select a file...");
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(import_file_selection)->ok_button),
                      "clicked",(GtkSignalFunc)import_load_select_ok,import_file_selection);
	gtk_signal_connect_object (GTK_OBJECT(GTK_FILE_SELECTION(import_file_selection)->cancel_button),
                             "clicked",(GtkSignalFunc)gtk_widget_destroy,
                             GTK_OBJECT(import_file_selection));
	gtk_widget_show(import_file_selection);
}

void on_import_remove_activate (GtkWidget *item, gpointer user_data)
{
	int i,n;
	char *txt;
	for (i=0;i<GTK_CLIST(import_files_clist)->rows;i++)
	{
		gtk_clist_get_text(GTK_CLIST(import_files_clist),i,0,&txt);
		n = find_import_data_from_fname(txt);
		if (n==-1)
			continue;
		if (!imp_data[n].used)
			continue;
		if (!imp_data[n].selected)
			continue;
		if ((n+1)==blocs_size)
		{
			blocs_size--;
			imp_data = (import_bloc *) realloc(imp_data,blocs_size*sizeof(import_bloc));
		}
		else
		{
			imp_data[n].used = 0;
		}
		gtk_clist_remove(GTK_CLIST(import_files_clist),i);
		i--;
	}
}

void on_import_reflex_activate (GtkWidget *item, gpointer user_data)
{
	char txt[100];
	strcpy(txt,user_data);
	imp_refl = atoi(txt);
}

void on_import_ana_toggle (GtkWidget *item, gpointer user_data)
{
	char txt[100];
/*	if (!opt_loaded)
		return; */

	strcpy(txt,user_data);
	imp_ana = atoi(txt);
	import_display_curves();
}

void on_import_norm_toggle (GtkWidget *item, gpointer user_data)
{
	if (GTK_TOGGLE_BUTTON(import_toggle_norm)->active)
		imp_norm = 1;
	else
		imp_norm = 0;
	import_display_curves();
}

void import_select_row (GtkWidget *widget,gint row,gint column,
			GdkEventButton *event,gpointer data)
{
	int n;
	char *txt;
	gtk_clist_get_text(GTK_CLIST(import_files_clist),row,0,&txt);
	n = find_import_data_from_fname(txt);
	if (n==-1)
		return;
	if (data)
		imp_data[n].selected = 1;
	else
		imp_data[n].selected = 0;
	import_display_curves();
}

void import_display_curves (void)
{
	int i,j,tst=0;
	double dat,dw;
	char *txt,*fit_tcname;
	char tmpstr[1000];
	FILE *fil;

	fil = fopen(tcname,"w");
	for (i=0;i<blocs_size;i++)
	{
		if (!imp_data[i].used)
			continue;
		if (!imp_data[i].selected)
			continue;
		tst = 1;
		for (j=0;j<imp_data[i].size;j++)
		{
			dat = imp_data[i].point[j].deta[imp_ana-1];
			if (imp_norm)
				dat /= imp_data[i].point[j].i1;
			sprintf(tmpstr,"%g %g\n",
				imp_data[i].point[j].dt[imp_ana-1],dat);
			fputs(tmpstr,fil);
		}
	}
	fclose(fil);
	if (tst)
	{
		fprintf(gnuplot,"set xlabel \"dT\"\n");
		fprintf(gnuplot,"set ylabel \"Counts\"\n");
		fprintf(gnuplot,"plot \"%s\" using 1:2 title 'Ana #%d' lt -1 pt 3 with lines\n",tcname,imp_ana);
		fflush(gnuplot);
		if (imp_show_fit)
		{
			fit_tcname = tmpnam((char *)0);
			fil = fopen(fit_tcname,"w");
			for (i=0;i<imp_nb;i++)
			{
				dw = imp_de[i]-imp_par[2];
				dat = imp_par[0]*imp_par[1]*imp_par[1]/(dw*dw+imp_par[1]*imp_par[1]);
				sprintf(tmpstr,"%g %g\n",imp_de[i],dat);
				fputs(tmpstr,fil);
			}
			fclose(fil);
			fprintf(gnuplot,"replot \"%s\" using 1:2 title 'Fit' with lines lt 1\n",fit_tcname);
			fflush(gnuplot);
		}
		if (imp_show_sum)
		{
			fit_tcname = tmpnam((char *)0);
			fil = fopen(fit_tcname,"w");
			for (i=0;i<imp_nb_sum;i++)
			{
				dat = imp_de_sum[i];
				if (imp_norm)
					dat /= imp_mon_sum[i];
				sprintf(tmpstr,"%g %g\n",
					imp_de_sum[i]/(2.58*imp_enrg[imp_refl]),
					imp_cts_sum[i]);
				fputs(tmpstr,fil);
			}
			fclose(fil);
			fprintf(gnuplot,"replot \"%s\" using 1:2 title 'Fit' with lines lt 1\n",fit_tcname);
			fflush(gnuplot);
		}
	}
	imp_show_fit = 0;
	imp_show_sum = 0;
}

int check_list_for_file (char *fn)
{
	int i,ok;
	char *row_txt;

	for (i=0;;i++)
	{
		ok = gtk_clist_get_text(GTK_CLIST(import_files_clist),
			i,0,&row_txt);
		if (ok == NULL)
			return 0;
		if (!strcasecmp(fn,row_txt))
			return 1;
	}
	return 0;
}

void on_import_guess_activate (GtkWidget *item, gpointer user_data)
{
	int i,j,n,nb,tst;
	double tmpde,tmpcts;
	double max,hm1,hm2;
	char tmpstr[300];

	n = 0;
	for (i=0;i<blocs_size;i++)
	{
		if (imp_data[i].used == 0)
			continue;
		for (j=0;j<imp_data[i].size;j++)
		{
			imp_de[n] = imp_data[i].point[j].dt[imp_ana-1];
			imp_cts[n] = imp_data[i].point[j].deta[imp_ana-1];
			if (imp_norm)
				imp_cts[n] /= imp_data[i].point[j].i1;
			n++;
		}
	}
	imp_nb = n;
	tst = 1;
	while (tst == 1)
	{
		tst = 0;
		for (i=0;i<imp_nb-1;i++)
		{
			if (imp_de[i]>imp_de[i+1])
			{
				tmpde = imp_de[i];
				tmpcts = imp_cts[i];
				imp_de[i] = imp_de[i+1];
				imp_cts[i] = imp_cts[i+1];
				imp_de[i+1] = tmpde;
				imp_cts[i+1] = tmpcts;
				tst = 1;
			}
		}	
	}
	/* Initialize simplex. */
	max = 0;
	for (i=0;i<imp_nb;i++)
	{
		if (imp_cts[i]>max)
			max = imp_cts[i];
	}
	if (max<=0)
	{
		printf("Impossible guess: negative maximum\n");
		return;
	}
	hm1 = imp_de[0];
	hm2 = imp_de[imp_nb-1];
	for (i=0;i<imp_nb-1;i++)
	{
		if ((imp_cts[i]<=max/2) && (imp_cts[i+1]>max/2))
			hm1 = imp_de[i];
		if ((imp_cts[i]>max/2) && (imp_cts[i+1]<=max/2))
			hm2 = imp_de[i];
	}
	if (hm1==hm2)
	{
		printf("Impossible guess: could not find width\n");
		return;
	}
	imp_par[0] = max;
	imp_par[1] = (hm2-hm1)/2;
	imp_par[2] = (hm1+hm2)/2;
	imp_delta[0] = 1;
	imp_delta[1] = 0.01;
	imp_delta[2] = 0.1;
	simplex(import_mini_func,3,imp_par,imp_delta,1,10000);
	printf("[P=%g, G=%g, O=%g]\n",imp_par[0],imp_par[1],imp_par[2]);
	sprintf(tmpstr,"%g",imp_par[2]);
	gtk_entry_set_text(GTK_ENTRY(import_shift_entry),tmpstr);
	imp_show_fit = 1;
	import_display_curves();
}

/* Parameters: P, G, O */
double import_mini_func (double *p)
{
	int i;
	double dw,c2;
	double dat[imp_nb];

	c2 = 0;
	for (i=0;i<imp_nb;i++)
	{
		dw = imp_de[i]-p[2];
		dat[i] = p[0]*p[1]*p[1]/(dw*dw+p[1]*p[1]);
		c2 += (dat[i]-imp_cts[i])*(dat[i]-imp_cts[i]);
	}
/*	printf("Chi2 = %g  [P=%g, G=%g, O=%g]\n",c2,p[0],p[1],p[2]); */
	return c2;
}

double energy_scale (double t)
{
	double pf,factor;
	double asi = 5.43102088;
	double hbarc = 12398.483903; /* Planck/2PI*speed of light */
	pf = 2*asi/sqrt(3.0)*sin(1.57044726094);
	factor = 1000*hbarc/(pf/imp_hkl[imp_refl]);
	return (double) (t*(2.581e-6-t*0.008e-6)*factor);
}

