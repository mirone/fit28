
#include <gtk/gtk.h>



typedef struct {
	double i0,i1,tm,dt[5],deta[5];
} import_point;

typedef struct {
	import_point point[10000];
	int used,selected,size;
	char fname[300];
} import_bloc;



extern GtkWidget *import_win,*import_vbox,*import_label;
extern GtkWidget *import_hbox_files,*import_files_clist,*import_hbox_files_buttons;
extern GtkWidget *import_vbox_files_tools,*import_load_button,*import_remove_button;
extern GtkWidget *import_hbox_shift,*import_shift_entry,*import_guess_button;
extern GtkWidget *import_hbox_buttons,*import_close_button;
extern GtkWidget *import_file_selection;
extern GtkWidget *import_hbox_reflex,*import_reflex_menu,*import_toggle_norm;
extern GtkWidget *import_hbox_ana,*import_ana1,*import_ana2,*import_ana3,*import_ana4,*import_ana5;
extern GtkWidget *import_hbox_actions_buttons,*import_center_button,*import_sum_button;
extern GtkWidget *import_hbox_sname,*import_sname_entry;
extern import_bloc *imp_data;
extern char *import_fname,*import_small_fname;
extern int imp_refl,imp_ana,imp_norm,imp_nb,imp_nb_sum,imp_show_fit,imp_show_sum;
extern int blocs_size;
extern double imp_de[10000],imp_cts[10000],imp_mon[10000];
extern double *imp_de_sum,*imp_cts_sum,*imp_depth_sum,*imp_mon_sum;
extern double imp_par[3],imp_delta[3],imp_step;


void create_import_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void on_import_close_activate (GtkWidget *item, gpointer user_data);
void on_import_center_activate (GtkWidget *item, gpointer user_data);
void import_compute_average_step (void);
void import_compute_smallest_step (void);
void on_import_sum_activate (GtkWidget *item, gpointer user_data);
void import_load_select_ok (GtkWidget *w, GtkFileSelection *fs);
int find_free_import_data (void);
int find_import_data_from_fname (char *fname);
void import_load_file_into_data (char *fname, char *sfname);
void on_import_load_activate (GtkWidget *item, gpointer user_data);
void on_import_remove_activate (GtkWidget *item, gpointer user_data);
void on_import_reflex_activate (GtkWidget *item, gpointer user_data);
void import_select_row (GtkWidget *widget,gint row,gint column,
			GdkEventButton *event,gpointer data);
void import_display_curves (void);
void on_import_ana_toggle (GtkWidget *item, gpointer user_data);
void on_import_norm_toggle (GtkWidget *item, gpointer user_data);
int check_list_for_file (char *fn);
void on_import_guess_activate (GtkWidget *item, gpointer user_data);
double import_mini_func (double *p);
double energy_scale (double t);
