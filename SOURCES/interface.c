/*
 * interface.c - A. BERAUD  (March 2003,2004)
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <X11/Xlib.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "callbacks.h"
#include "interface.h"
#include "options.h"
#include "manual.h"
#include "ana_win.h"
#include "wavevec.h"
#include "resol_win.h"
#include "fitvie.h"
#include "import.h"

struct_abcd params[10];
struct_abcd excits[36];
struct_abcd viscs[10];
Display *disp;
char *file_inp,*file_dat,*file_fit,*file_resol,*file_do,*file_par;
char fit_model[5],visc[5],t_exci[5];
int temper,fstep,int_time,n_exci,fixed[42],visco_fixed[10];
char printer[300],resol_dir[1000];
int plot_prgm,plot_prgm_now,undo_depth;
int anim_id;
gint anim_func_id, check_info_id;
int dia_type, nb_visc;
int fit_types_loaded;

GtkWidget *create_main_window (void);
GtkWidget *main_window;
GtkWidget *vbox1;
GtkWidget *menubar1;
GtkWidget *general;
GtkWidget *general_menu;
GtkAccelGroup *general_menu_accels;
GtkWidget *load;
GtkWidget *save;
GtkWidget *save_as;
GtkWidget *undo;
GtkWidget *preferences;
GtkWidget *example;
GtkWidget *separator1;
GtkWidget *about;
GtkWidget *quit;
GtkWidget *help;
GtkWidget *help_menu;
GtkAccelGroup *help_menu_accels;
GtkWidget *report_bug;
GtkWidget *manual;
GtkWidget *tools;
GtkWidget *tools_menu;
GtkAccelGroup *tools_menu_accels;
GtkWidget *print_ana_state;
GtkWidget *wavevector;
GtkWidget *kill_fit;
GtkWidget *refresh;
GtkWidget *print_to_file;
GtkWidget *import;
GtkWidget *file_selection;
GtkWidget *inp_button;
GtkWidget *inp_label;
GtkWidget *main_info_box;
GtkWidget *info_text;
GtkWidget *gen_par_frame;
GtkWidget *fit_seq_frame;
GtkWidget *excit_frame;
GtkWidget *text_title;
GtkWidget *box_title;
GtkWidget *label;
GtkWidget *menu;
GtkWidget *menu_item;
GtkWidget *over_hbox_files;
GtkWidget *over_vbox_files;
GtkWidget *box_files1;
GtkWidget *box_files1_text_0;
GtkWidget *box_files1_button_0;
GtkWidget *box_files1_text_1;
GtkWidget *box_files1_button_1;
GtkWidget *box_files2;
GtkWidget *box_files2_text_1;
GtkWidget *box_files2_button_1;
GtkWidget *box_files2_text_2;
GtkWidget *box_files2_button_2;
GtkWidget *box_data1;
GtkWidget *box_data1_text_0;
GtkWidget *box_data1_text_1;
GtkWidget *box_data1_text_2;
GtkWidget *box_temperature;
GtkWidget *box_data2;
GtkObject *box_data2_adj_0;
GtkWidget *box_data2_spin_0;
GtkWidget *box_data2_list_0;
GtkWidget *box_data2_list_1;
GtkWidget *box_convol;
GtkWidget *box_convol_lor_width;
GtkWidget *box_convol_gau_width;
GtkWidget *box_convol_eta;
GtkWidget *box_convol_choices_hbox;
GtkWidget *box_convol_choices;
GtkWidget *pv_load_button;
GtkWidget *pv_load_label;
GtkWidget *box_res_file;
GtkWidget *box_res_file_text;
GtkWidget *box_res_file_button;
GtkWidget *convol_radio0, *convol_radio1, *convol_radio2, *convol_radio3;
GtkWidget *box_param_h;
GtkWidget *box_param_v;
GtkWidget *box_bg;
GtkWidget *box_bg_text_0;
GtkWidget *box_bg_text_1;
GtkWidget *box_bg_text_2;
GtkWidget *box_bg_text_3;
GtkWidget *box_bg_check_0;
GtkWidget *box_sl;
GtkWidget *box_sl_text_0;
GtkWidget *box_sl_text_1;
GtkWidget *box_sl_text_2;
GtkWidget *box_sl_text_3;
GtkWidget *box_sl_check_0;
GtkWidget *box_zr;
GtkWidget *box_zr_text_0;
GtkWidget *box_zr_text_1;
GtkWidget *box_zr_text_2;
GtkWidget *box_zr_text_3;
GtkWidget *box_zr_check_0;
GtkWidget *box_gr;
GtkWidget *box_gr_text_0;
GtkWidget *box_gr_text_1;
GtkWidget *box_gr_text_2;
GtkWidget *box_gr_text_3;
GtkWidget *box_gr_check_0;
GtkWidget *gr_label;
GtkWidget *box_pc;
GtkWidget *box_pc_text_0;
GtkWidget *box_pc_text_1;
GtkWidget *box_pc_text_2;
GtkWidget *box_pc_text_3;
GtkWidget *box_pc_check_0;
GtkWidget *box_gc;
GtkWidget *box_gc_text_0;
GtkWidget *box_gc_text_1;
GtkWidget *box_gc_text_2;
GtkWidget *box_gc_text_3;
GtkWidget *box_gc_check_0;
GtkWidget *box_sa;
GtkWidget *box_sa_text_0;
GtkWidget *box_sa_text_1;
GtkWidget *box_sa_text_2;
GtkWidget *box_sa_text_3;
GtkWidget *box_sa_check_0;
GtkWidget *box_ta;
GtkWidget *box_ta_text_0;
GtkWidget *box_ta_text_1;
GtkWidget *box_ta_text_2;
GtkWidget *box_ta_text_3;
GtkWidget *box_ta_check_0;
GtkWidget *box_aa;
GtkWidget *box_aa_text_0;
GtkWidget *box_aa_text_1;
GtkWidget *box_aa_text_2;
GtkWidget *box_aa_text_3;
GtkWidget *box_aa_check_0;
GtkWidget *box_bb;
GtkWidget *box_bb_text_0;
GtkWidget *box_bb_text_1;
GtkWidget *box_bb_text_2;
GtkWidget *box_bb_text_3;
GtkWidget *box_bb_check_0;
GtkWidget *vbox_excit;
GtkWidget *box_exc1;
GtkWidget *box_exc1_model;
GtkWidget *box_exc1_text_0;
GtkWidget *box_exc1_text_1;
GtkWidget *box_exc1_text_2;
GtkWidget *box_exc1_text_3;
GtkWidget *box_exc1_check_0;
GtkWidget *box_exc1_text_4;
GtkWidget *box_exc1_text_5;
GtkWidget *box_exc1_text_6;
GtkWidget *box_exc1_text_7;
GtkWidget *box_exc1_check_1;
GtkWidget *box_exc1_text_8;
GtkWidget *box_exc1_text_9;
GtkWidget *box_exc1_text_10;
GtkWidget *box_exc1_text_11;
GtkWidget *box_exc1_check_2;
GtkWidget *box_exc2;
GtkWidget *box_exc2_model;
GtkWidget *box_exc2_text_0;
GtkWidget *box_exc2_text_1;
GtkWidget *box_exc2_text_2;
GtkWidget *box_exc2_text_3;
GtkWidget *box_exc2_check_0;
GtkWidget *box_exc2_text_4;
GtkWidget *box_exc2_text_5;
GtkWidget *box_exc2_text_6;
GtkWidget *box_exc2_text_7;
GtkWidget *box_exc2_check_1;
GtkWidget *box_exc2_text_8;
GtkWidget *box_exc2_text_9;
GtkWidget *box_exc2_text_10;
GtkWidget *box_exc2_text_11;
GtkWidget *box_exc2_check_2;
GtkWidget *box_exc3;
GtkWidget *box_exc3_model;
GtkWidget *box_exc3_text_0;
GtkWidget *box_exc3_text_1;
GtkWidget *box_exc3_text_2;
GtkWidget *box_exc3_text_3;
GtkWidget *box_exc3_check_0;
GtkWidget *box_exc3_text_4;
GtkWidget *box_exc3_text_5;
GtkWidget *box_exc3_text_6;
GtkWidget *box_exc3_text_7;
GtkWidget *box_exc3_check_1;
GtkWidget *box_exc3_text_8;
GtkWidget *box_exc3_text_9;
GtkWidget *box_exc3_text_10;
GtkWidget *box_exc3_text_11;
GtkWidget *box_exc3_check_2;
GtkWidget *box_exc4;
GtkWidget *box_exc4_model;
GtkWidget *box_exc4_text_0;
GtkWidget *box_exc4_text_1;
GtkWidget *box_exc4_text_2;
GtkWidget *box_exc4_text_3;
GtkWidget *box_exc4_check_0;
GtkWidget *box_exc4_text_4;
GtkWidget *box_exc4_text_5;
GtkWidget *box_exc4_text_6;
GtkWidget *box_exc4_text_7;
GtkWidget *box_exc4_check_1;
GtkWidget *box_exc4_text_8;
GtkWidget *box_exc4_text_9;
GtkWidget *box_exc4_text_10;
GtkWidget *box_exc4_text_11;
GtkWidget *box_exc4_check_2;
GtkWidget *box_exc5;
GtkWidget *box_exc5_model;
GtkWidget *box_exc5_text_0;
GtkWidget *box_exc5_text_1;
GtkWidget *box_exc5_text_2;
GtkWidget *box_exc5_text_3;
GtkWidget *box_exc5_check_0;
GtkWidget *box_exc5_text_4;
GtkWidget *box_exc5_text_5;
GtkWidget *box_exc5_text_6;
GtkWidget *box_exc5_text_7;
GtkWidget *box_exc5_check_1;
GtkWidget *box_exc5_text_8;
GtkWidget *box_exc5_text_9;
GtkWidget *box_exc5_text_10;
GtkWidget *box_exc5_text_11;
GtkWidget *box_exc5_check_2;
GtkWidget *box_exc6;
GtkWidget *box_exc6_model;
GtkWidget *box_exc6_text_0;
GtkWidget *box_exc6_text_1;
GtkWidget *box_exc6_text_2;
GtkWidget *box_exc6_text_3;
GtkWidget *box_exc6_check_0;
GtkWidget *box_exc6_text_4;
GtkWidget *box_exc6_text_5;
GtkWidget *box_exc6_text_6;
GtkWidget *box_exc6_text_7;
GtkWidget *box_exc6_check_1;
GtkWidget *box_exc6_text_8;
GtkWidget *box_exc6_text_9;
GtkWidget *box_exc6_text_10;
GtkWidget *box_exc6_text_11;
GtkWidget *box_exc6_check_2;
GtkWidget *box_exc7;
GtkWidget *box_exc7_model;
GtkWidget *box_exc7_text_0;
GtkWidget *box_exc7_text_1;
GtkWidget *box_exc7_text_2;
GtkWidget *box_exc7_text_3;
GtkWidget *box_exc7_check_0;
GtkWidget *box_exc7_text_4;
GtkWidget *box_exc7_text_5;
GtkWidget *box_exc7_text_6;
GtkWidget *box_exc7_text_7;
GtkWidget *box_exc7_check_1;
GtkWidget *box_exc7_text_8;
GtkWidget *box_exc7_text_9;
GtkWidget *box_exc7_text_10;
GtkWidget *box_exc7_text_11;
GtkWidget *box_exc7_check_2;
GtkWidget *box_exc8;
GtkWidget *box_exc8_model;
GtkWidget *box_exc8_text_0;
GtkWidget *box_exc8_text_1;
GtkWidget *box_exc8_text_2;
GtkWidget *box_exc8_text_3;
GtkWidget *box_exc8_check_0;
GtkWidget *box_exc8_text_4;
GtkWidget *box_exc8_text_5;
GtkWidget *box_exc8_text_6;
GtkWidget *box_exc8_text_7;
GtkWidget *box_exc8_check_1;
GtkWidget *box_exc8_text_8;
GtkWidget *box_exc8_text_9;
GtkWidget *box_exc8_text_10;
GtkWidget *box_exc8_text_11;
GtkWidget *box_exc8_check_2;
GtkWidget *box_exc9;
GtkWidget *box_exc9_model;
GtkWidget *box_exc9_text_0;
GtkWidget *box_exc9_text_1;
GtkWidget *box_exc9_text_2;
GtkWidget *box_exc9_text_3;
GtkWidget *box_exc9_check_0;
GtkWidget *box_exc9_text_4;
GtkWidget *box_exc9_text_5;
GtkWidget *box_exc9_text_6;
GtkWidget *box_exc9_text_7;
GtkWidget *box_exc9_check_1;
GtkWidget *box_exc9_text_8;
GtkWidget *box_exc9_text_9;
GtkWidget *box_exc9_text_10;
GtkWidget *box_exc9_text_11;
GtkWidget *box_exc9_check_2;
GtkWidget *box_exc10;
GtkWidget *box_exc10_model;
GtkWidget *box_exc10_text_0;
GtkWidget *box_exc10_text_1;
GtkWidget *box_exc10_text_2;
GtkWidget *box_exc10_text_3;
GtkWidget *box_exc10_check_0;
GtkWidget *box_exc10_text_4;
GtkWidget *box_exc10_text_5;
GtkWidget *box_exc10_text_6;
GtkWidget *box_exc10_text_7;
GtkWidget *box_exc10_check_1;
GtkWidget *box_exc10_text_8;
GtkWidget *box_exc10_text_9;
GtkWidget *box_exc10_text_10;
GtkWidget *box_exc10_text_11;
GtkWidget *box_exc10_check_2;
GtkWidget *box_exc11;
GtkWidget *box_exc11_model;
GtkWidget *box_exc11_text_0;
GtkWidget *box_exc11_text_1;
GtkWidget *box_exc11_text_2;
GtkWidget *box_exc11_text_3;
GtkWidget *box_exc11_check_0;
GtkWidget *box_exc11_text_4;
GtkWidget *box_exc11_text_5;
GtkWidget *box_exc11_text_6;
GtkWidget *box_exc11_text_7;
GtkWidget *box_exc11_check_1;
GtkWidget *box_exc11_text_8;
GtkWidget *box_exc11_text_9;
GtkWidget *box_exc11_text_10;
GtkWidget *box_exc11_text_11;
GtkWidget *box_exc11_check_2;
GtkWidget *box_exc12;
GtkWidget *box_exc12_model;
GtkWidget *box_exc12_text_0;
GtkWidget *box_exc12_text_1;
GtkWidget *box_exc12_text_2;
GtkWidget *box_exc12_text_3;
GtkWidget *box_exc12_check_0;
GtkWidget *box_exc12_text_4;
GtkWidget *box_exc12_text_5;
GtkWidget *box_exc12_text_6;
GtkWidget *box_exc12_text_7;
GtkWidget *box_exc12_check_1;
GtkWidget *box_exc12_text_8;
GtkWidget *box_exc12_text_9;
GtkWidget *box_exc12_text_10;
GtkWidget *box_exc12_text_11;
GtkWidget *box_exc12_check_2;
GtkWidget *box_visco;
GtkWidget *box_visco_visi;
GtkWidget *box_visco_visi_text_0;
GtkWidget *box_visco_visi_text_1;
GtkWidget *box_visco_visi_text_2;
GtkWidget *box_visco_visi_text_3;
GtkWidget *box_visco_visi_check_0;
GtkWidget *box_visco_omega0;
GtkWidget *box_visco_omega0_text_0;
GtkWidget *box_visco_omega0_text_1;
GtkWidget *box_visco_omega0_text_2;
GtkWidget *box_visco_omega0_text_3;
GtkWidget *box_visco_omega0_check_0;
GtkWidget *box_visco_gamma;
GtkWidget *box_visco_gamma_text_0;
GtkWidget *box_visco_gamma_text_1;
GtkWidget *box_visco_gamma_text_2;
GtkWidget *box_visco_gamma_text_3;
GtkWidget *box_visco_gamma_check_0;
GtkWidget *box_visco_taut;
GtkWidget *box_visco_taut_text_0;
GtkWidget *box_visco_taut_text_1;
GtkWidget *box_visco_taut_text_2;
GtkWidget *box_visco_taut_text_3;
GtkWidget *box_visco_taut_check_0;
GtkWidget *box_visco_omegainf;
GtkWidget *box_visco_omegainf_text_0;
GtkWidget *box_visco_omegainf_text_1;
GtkWidget *box_visco_omegainf_text_2;
GtkWidget *box_visco_omegainf_text_3;
GtkWidget *box_visco_omegainf_check_0;
GtkWidget *box_visco_tau1;
GtkWidget *box_visco_tau1_text_0;
GtkWidget *box_visco_tau1_text_1;
GtkWidget *box_visco_tau1_text_2;
GtkWidget *box_visco_tau1_text_3;
GtkWidget *box_visco_tau1_check_0;
GtkWidget *box_visco_gamma0;
GtkWidget *box_visco_gamma0_text_0;
GtkWidget *box_visco_gamma0_text_1;
GtkWidget *box_visco_gamma0_text_2;
GtkWidget *box_visco_gamma0_text_3;
GtkWidget *box_visco_gamma0_check_0;
GtkWidget *box_visco_delta;
GtkWidget *box_visco_delta_text_0;
GtkWidget *box_visco_delta_text_1;
GtkWidget *box_visco_delta_text_2;
GtkWidget *box_visco_delta_text_3;
GtkWidget *box_visco_delta_check_0;
GtkWidget *box_visco_taud;
GtkWidget *box_visco_taud_text_0;
GtkWidget *box_visco_taud_text_1;
GtkWidget *box_visco_taud_text_2;
GtkWidget *box_visco_taud_text_3;
GtkWidget *box_visco_taud_check_0;
GtkWidget *box_visco_beta;
GtkWidget *box_visco_beta_text_0;
GtkWidget *box_visco_beta_text_1;
GtkWidget *box_visco_beta_text_2;
GtkWidget *box_visco_beta_text_3;
GtkWidget *box_visco_beta_check_0;
GtkWidget *box_visco_check_e2;
GtkWidget *visco_check_e2;
GtkWidget *exc1_swp_toggle;
GtkWidget *exc2_swp_toggle;
GtkWidget *exc3_swp_toggle;
GtkWidget *exc4_swp_toggle;
GtkWidget *exc5_swp_toggle;
GtkWidget *exc6_swp_toggle;
GtkWidget *exc7_swp_toggle;
GtkWidget *exc8_swp_toggle;
GtkWidget *exc9_swp_toggle;
GtkWidget *exc10_swp_toggle;
GtkWidget *exc11_swp_toggle;
GtkWidget *exc12_swp_toggle;
GtkWidget *box_fits_v;
GtkWidget *fit_type_simplex;
GtkWidget *fit_type_migrad;
GtkWidget *calc_type_fft;
GtkWidget *scr_rest;
GtkWidget *text_rest;
GtkWidget *box_bottom_buttons;
GtkWidget *fit_button;
GtkWidget *plot_button;
GtkWidget *print_button;
GtkWidget *preview_button;
GtkWidget *log_fix;
GtkWidget *central_bose;
GtkWidget *box_central_type;
GtkWidget *central_type;
GtkWidget *central_list;
GtkWidget *live_chk;
GtkObject *live_adj;
GtkWidget *live_spin;
GtkWidget *dia_win;
GtkWidget *logo;
GdkPixmap *logo_pixmap;
GdkPixmap *logo_anim[10];
GdkBitmap *mask;
GtkStyle *style;



int randnum (float max)
{
	int j;
	j=(int) (max*rand()/(RAND_MAX+1.0));
	return j;
}

void load_anim (void)
{
	int i;
	char tmp[500];

	for (i=0;i<10;i++)
	{
		sprintf(tmp,"%s/.fit28/gfx/logo_anim%d.xpm",getenv("HOME"),i+1);
		logo_anim[i] = gdk_pixmap_create_from_xpm(main_window->window,&mask,
			&style->bg[GTK_STATE_NORMAL],tmp);
	}
	anim_id = 0;
	anim_func_id = g_timeout_add(((5+randnum(5))*60+randnum(60))*1000,(GtkFunction)draw_anim,NULL); /* First call to animation */
}

void draw_anim (void)
{
	if (anim_id==0)
		g_source_remove(anim_func_id);
	gtk_pixmap_set(GTK_PIXMAP(logo),logo_anim[anim_id],NULL);
	if (anim_id==0)
		anim_func_id = g_timeout_add(100,(GtkFunction)draw_anim,NULL);
	anim_id++;
	if (anim_id>=10)
	{
		gtk_pixmap_set(GTK_PIXMAP(logo),logo_pixmap,NULL);
		anim_id = 0;
		g_source_remove(anim_func_id);
		anim_func_id = g_timeout_add(((5+randnum(5))*60+randnum(60))*1000,(GtkFunction)draw_anim,NULL);
		return TRUE;
	}
}

void dialog_yes (GtkWidget *item, gpointer user_data)
{
	if (dia_type == 1)
		save_really();
	if (dia_type == 2)
	{
		kill_current_fit();
		fit_really();
	}
	if (dia_type == 3)
		save_really();
	if (dia_type == 4)
		save_really();
	gtk_widget_destroy(dia_win);
}

void dialog_no (GtkWidget *item, gpointer user_data)
{
	if (dia_type == 4)
	{
		fl_t_as_param = 1;
		save_really();
	}
	gtk_widget_destroy(dia_win);
}

void dialog_window (void)
{
	char *txt;
	GtkWidget *label,*yes_but,*no_but;
	GtkAccelGroup *dialog_accels;

	txt = g_strdup("This is a bug. Please report.");
	if (dia_type == 1)
		txt = g_strdup("This file already exists. Do you want to save anyway ?");
	if (dia_type == 2)
		txt = g_strdup("There is a fit running already. Replace with new fit ?");
	if (dia_type == 3)
		txt = g_strdup("*** WARNING ! ***\n\nData file changed. Save anyway ?");
	if (dia_type == 4)
		txt = g_strdup("*** WARNING ! ***\n\nYou are currently using the old file format (T is not a fit parameter)\nDo you want to keep this format ?");
	dia_win = gtk_dialog_new();
	gtk_window_set_modal(GTK_WINDOW(dia_win),TRUE);

	dialog_accels = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(dia_win),dialog_accels);

	label = gtk_label_new(txt);
	yes_but = gtk_button_new_with_label("Yes");
	gtk_signal_connect(GTK_OBJECT (yes_but), "clicked",
		GTK_SIGNAL_FUNC(dialog_yes),NULL);
	gtk_widget_add_accelerator(yes_but, "clicked", dialog_accels, GDK_Return, 0, 0);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dia_win)->action_area), yes_but);
	no_but = gtk_button_new_with_label("No");
	gtk_signal_connect_object (GTK_OBJECT(no_but), "clicked",
		GTK_SIGNAL_FUNC(dialog_no),NULL);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dia_win)->action_area), no_but);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dia_win)->vbox), label);
	gtk_widget_show_all(dia_win);
}

void init_model_menu (GtkWidget *menu, GtkSignalFunc func)
{
	menu_item = gtk_menu_item_new_with_label ("dho");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"dho");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("lor");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"lor");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("gau");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"gau");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("del");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"del");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
}

void init_model_main_menu (GtkWidget *menu, GtkSignalFunc func)
{
	menu_item = gtk_menu_item_new_with_label ("dho");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"dho");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("lor");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"lor");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("gau");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"gau");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("del");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"del");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("vis");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)func,"vis");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
}


GtkWidget * create_main_window (void)
{
  GtkAccelGroup *general_menu_accels;
  GdkColor color;
  char tmp[500];

  srand((int)time((long *)NULL));

  /* Main window */
  main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (main_window), "main_window", main_window);
  gtk_window_set_title (GTK_WINDOW (main_window), VERSION);
  gtk_widget_set_usize (main_window,900,0);
  gtk_window_set_policy(GTK_WINDOW(main_window), FALSE, FALSE, TRUE);
  gtk_widget_show(main_window);

  vbox1 = gtk_vbox_new (FALSE, 5);
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (main_window), vbox1);

  menubar1 = gtk_menu_bar_new ();
  gtk_widget_ref (menubar1);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "menubar1", menubar1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (menubar1);
  gtk_box_pack_start (GTK_BOX (vbox1), menubar1, FALSE, FALSE, 0);

  general_menu_accels = gtk_accel_group_new();

  general = gtk_menu_item_new_with_label ("General");
  gtk_widget_ref (general);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "general", general,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (general);
  gtk_container_add (GTK_CONTAINER (menubar1), general);

  general_menu = gtk_menu_new ();
  gtk_widget_ref (general_menu);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "general_menu", general_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (general), general_menu);
  gtk_window_add_accel_group(GTK_WINDOW(main_window),general_menu_accels);

  load = gtk_menu_item_new_with_label ("Load");
  gtk_widget_ref (load);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "load", load,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (load);
  gtk_container_add (GTK_CONTAINER (general_menu), load);

  save = gtk_menu_item_new_with_label ("Save");
  gtk_widget_ref (save);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "save", save,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_add_accelerator(save,"activate",general_menu_accels,'s',GDK_CONTROL_MASK,GTK_ACCEL_VISIBLE);
  gtk_widget_show (save);
  gtk_container_add (GTK_CONTAINER (general_menu), save);

  save_as = gtk_menu_item_new_with_label ("Save as...");
  gtk_widget_ref (save_as);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "save_as", save_as,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_add_accelerator(save_as,"activate",general_menu_accels,'s',GDK_CONTROL_MASK|GDK_SHIFT_MASK,GTK_ACCEL_VISIBLE);
  gtk_widget_show (save_as);
  gtk_container_add (GTK_CONTAINER (general_menu), save_as);

  undo = gtk_menu_item_new_with_label ("Undo");
  gtk_widget_ref (undo);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "undo", undo,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_add_accelerator(undo,"activate",general_menu_accels,'z',GDK_CONTROL_MASK,GTK_ACCEL_VISIBLE);
  gtk_widget_show (undo);
  gtk_container_add (GTK_CONTAINER (general_menu), undo);

  preferences = gtk_menu_item_new_with_label ("Preferences");
  gtk_widget_ref (preferences);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "preferences", preferences,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (preferences);
  gtk_container_add (GTK_CONTAINER (general_menu), preferences);

  example = gtk_menu_item_new_with_label ("Example file");
  gtk_widget_ref (example);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "example", example,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (example);
  gtk_container_add (GTK_CONTAINER (general_menu), example);

  separator1 = gtk_menu_item_new ();
  gtk_widget_ref (separator1);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "separator1", separator1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (separator1);
  gtk_container_add (GTK_CONTAINER (general_menu), separator1);
  gtk_widget_set_sensitive (separator1, FALSE);

  about = gtk_menu_item_new_with_label ("About");
  gtk_widget_ref (about);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "about", about,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (about);
  gtk_container_add (GTK_CONTAINER (general_menu), about);

  quit = gtk_menu_item_new_with_label ("Quit");
  gtk_widget_ref (quit);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "quit", quit,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (quit);
  gtk_container_add (GTK_CONTAINER (general_menu), quit);

  tools = gtk_menu_item_new_with_label ("Tools");
  gtk_widget_ref (tools);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "tools", tools,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (tools);
  gtk_container_add (GTK_CONTAINER (menubar1), tools);

  tools_menu = gtk_menu_new ();
  gtk_widget_ref (tools_menu);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "tools_menu", tools_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (tools), tools_menu);

  import = gtk_menu_item_new_with_label ("Import data file");
  gtk_widget_ref (import);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "import", import,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (import);
  gtk_container_add (GTK_CONTAINER (tools_menu), import);

  print_ana_state = gtk_menu_item_new_with_label ("Analyzers state");
  gtk_widget_ref (print_ana_state);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "print_ana_state", print_ana_state,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (print_ana_state);
  gtk_container_add (GTK_CONTAINER (tools_menu), print_ana_state);

  wavevector = gtk_menu_item_new_with_label ("Wave vector");
  gtk_widget_ref (wavevector);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "wavevector", wavevector,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (wavevector);
  gtk_container_add (GTK_CONTAINER (tools_menu), wavevector);

  kill_fit = gtk_menu_item_new_with_label ("Kill current fitting");
  gtk_widget_ref (kill_fit);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "kill_fit", kill_fit,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_add_accelerator(kill_fit,"activate",general_menu_accels,'k',GDK_CONTROL_MASK,GTK_ACCEL_VISIBLE);
  gtk_widget_show (kill_fit);
  gtk_container_add (GTK_CONTAINER (tools_menu), kill_fit);

  refresh = gtk_menu_item_new_with_label ("Refresh data");
  gtk_widget_ref (refresh);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "refresh", refresh,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_add_accelerator(refresh,"activate",general_menu_accels,'r',GDK_CONTROL_MASK,GTK_ACCEL_VISIBLE);
  gtk_widget_show (refresh);
  gtk_container_add (GTK_CONTAINER (tools_menu), refresh);

  print_to_file = gtk_menu_item_new_with_label ("Print plot to file");
  gtk_widget_ref (print_to_file);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "print_to_file", print_to_file,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (print_to_file);
  gtk_container_add (GTK_CONTAINER (tools_menu), print_to_file);

  help = gtk_menu_item_new_with_label ("Help");
  gtk_widget_ref (help);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "help", help,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_right_justify((GtkMenuItem *)help);
  gtk_widget_show (help);
  gtk_container_add (GTK_CONTAINER (menubar1), help);

  help_menu = gtk_menu_new ();
  gtk_widget_ref (help_menu);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "help_menu", help_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (help), help_menu);

  report_bug = gtk_menu_item_new_with_label ("Report bug");
  gtk_widget_ref (report_bug);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "report_bug", report_bug,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (report_bug);
  gtk_container_add (GTK_CONTAINER (help_menu), report_bug);

  manual = gtk_menu_item_new_with_label ("Manual");
  gtk_widget_ref (manual);
  gtk_object_set_data_full (GTK_OBJECT (main_window), "manual", manual,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (manual);
  gtk_container_add (GTK_CONTAINER (help_menu), manual);

  /* End of menus */


  over_hbox_files = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(over_hbox_files);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"over_hbox_files",over_hbox_files,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(over_hbox_files);
  gtk_box_pack_start(GTK_BOX(vbox1),over_hbox_files,FALSE,FALSE,0);

  strcpy(tmp,getenv("HOME"));
  strcat(tmp,"/.fit28/gfx/logo.xpm");
  style = gtk_widget_get_style(main_window);
  logo_pixmap = gdk_pixmap_create_from_xpm(main_window->window,&mask,
    &style->bg[GTK_STATE_NORMAL],tmp);
  logo = gtk_pixmap_new(logo_pixmap,mask);
  gtk_widget_show(logo);
  gtk_box_pack_start(GTK_BOX(over_hbox_files),logo,FALSE,FALSE,0);
  load_anim();

  over_vbox_files = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref(over_vbox_files);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"over_vbox_files",over_vbox_files,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(over_vbox_files);
  gtk_box_pack_start(GTK_BOX(over_hbox_files),over_vbox_files,TRUE,TRUE,0);

  box_files1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_files1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files1",box_files1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files1);
  gtk_box_pack_start(GTK_BOX(over_vbox_files),box_files1,TRUE,TRUE,0);

  inp_button = gtk_button_new();
  inp_label = gtk_label_new("Input file");
  gtk_container_add(GTK_CONTAINER(inp_button),inp_label);
  gtk_widget_show(inp_label);
  gtk_widget_show(inp_button);
  gtk_box_pack_start(GTK_BOX(box_files1),inp_button,TRUE,TRUE,0);


  label = gtk_label_new("Data");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_files1),label,TRUE,TRUE,0);

  box_files1_text_0 = gtk_entry_new();
  gtk_widget_ref(box_files1_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files1_text_0",box_files1_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_files1_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_files1_text_0,"?");
  gtk_signal_connect (GTK_OBJECT(box_files1_text_0),"activate",
                        (GtkSignalFunc)homogene_names,box_files1_text_0);
  gtk_widget_show(box_files1_text_0);
  gtk_box_pack_start(GTK_BOX(box_files1),box_files1_text_0,TRUE,TRUE,0);

  box_files1_button_0=gtk_button_new_with_label("Edit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files1_button_0",box_files1_button_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files1_button_0);
  gtk_box_pack_start(GTK_BOX(box_files1),box_files1_button_0,FALSE,FALSE,0);

  label = gtk_label_new("Fit");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_files1),label,TRUE,TRUE,0);

  box_files1_text_1 = gtk_entry_new();
  gtk_widget_ref(box_files1_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files1_text_1",box_files1_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_files1_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_files1_text_1,"?");
  gtk_signal_connect (GTK_OBJECT(box_files1_text_1),"activate",
                        (GtkSignalFunc)homogene_names,box_files1_text_1);
  gtk_widget_show(box_files1_text_1);
  gtk_box_pack_start(GTK_BOX(box_files1),box_files1_text_1,TRUE,TRUE,0);

  box_files1_button_1=gtk_button_new_with_label("Edit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files1_button_1",box_files1_button_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files1_button_1);
  gtk_box_pack_start(GTK_BOX(box_files1),box_files1_button_1,FALSE,FALSE,0);


  box_files2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_files2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files2",box_files2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files2);
  gtk_box_pack_start(GTK_BOX(over_vbox_files),box_files2,TRUE,TRUE,0);

  label = gtk_label_new("Do");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_files2),label,TRUE,TRUE,0);

  box_files2_text_1 = gtk_entry_new();
  gtk_widget_ref(box_files2_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files2_text_1",box_files2_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_files2_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_files2_text_1,"?");
  gtk_signal_connect (GTK_OBJECT(box_files2_text_1),"activate",
                        (GtkSignalFunc)homogene_names,box_files2_text_1);
  gtk_widget_show(box_files2_text_1);
  gtk_box_pack_start(GTK_BOX(box_files2),box_files2_text_1,TRUE,TRUE,0);

  box_files2_button_1=gtk_button_new_with_label("Edit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files2_button_1",box_files2_button_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files2_button_1);
  gtk_box_pack_start(GTK_BOX(box_files2),box_files2_button_1,FALSE,FALSE,0);

  label = gtk_label_new("Parameters");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_files2),label,TRUE,TRUE,0);

  box_files2_text_2 = gtk_entry_new();
  gtk_widget_ref(box_files2_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files2_text_2",box_files2_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_files2_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_files2_text_2,"?");
  gtk_signal_connect (GTK_OBJECT(box_files2_text_2),"activate",
                        (GtkSignalFunc)homogene_names,box_files2_text_2);
  gtk_widget_show(box_files2_text_2);
  gtk_box_pack_start(GTK_BOX(box_files2),box_files2_text_2,TRUE,TRUE,0);

  box_files2_button_2=gtk_button_new_with_label("Edit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_files2_button_2",box_files2_button_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_files2_button_2);
  gtk_box_pack_start(GTK_BOX(box_files2),box_files2_button_2,FALSE,FALSE,0);


  box_data1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_data1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_data1",box_data1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_data1);
  gtk_box_pack_start(GTK_BOX(vbox1),box_data1,FALSE,FALSE,0);

  box_temperature = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_data1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_temperature",box_temperature,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_temperature);
  gtk_box_pack_start(GTK_BOX(box_data1),box_temperature,TRUE,TRUE,0);

  label = gtk_label_new("T");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_temperature),label,TRUE,TRUE,0);

  box_data1_text_0 = gtk_entry_new();
  gtk_widget_ref(box_data1_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_data1_text_0",box_data1_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_data1_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_data1_text_0,"300");
  gtk_widget_show(box_data1_text_0);
  gtk_box_pack_start(GTK_BOX(box_temperature),box_data1_text_0,TRUE,TRUE,0);

  label = gtk_label_new("Frac. step");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_data1),label,TRUE,TRUE,0);

  box_data1_text_1 = gtk_entry_new();
  gtk_widget_ref(box_data1_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_data1_text_1",box_data1_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_data1_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_data1_text_1,"5");
  gtk_widget_show(box_data1_text_1);
  gtk_box_pack_start(GTK_BOX(box_data1),box_data1_text_1,TRUE,TRUE,0);

  label = gtk_label_new("Integ time");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_data1),label,TRUE,TRUE,0);

  box_data1_text_2 = gtk_entry_new();
  gtk_widget_ref(box_data1_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_data1_text_2",box_data1_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_data1_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_data1_text_2,"100");
  gtk_widget_show(box_data1_text_2);
  gtk_box_pack_start(GTK_BOX(box_data1),box_data1_text_2,TRUE,TRUE,0);


  box_data2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_data2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_data2",box_data2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_data2);
  gtk_box_pack_start(GTK_BOX(vbox1),box_data2,FALSE,FALSE,0);

  label = gtk_label_new("Nb of exc.");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_data2),label,TRUE,TRUE,0);

  box_data2_adj_0 = gtk_adjustment_new(1,0,12,1,1,0);
  box_data2_spin_0 = gtk_spin_button_new(GTK_ADJUSTMENT(box_data2_adj_0),1,0);
  gtk_widget_ref(box_data2_spin_0);
  gtk_widget_show(box_data2_spin_0);
  gtk_box_pack_start(GTK_BOX(box_data2),box_data2_spin_0,TRUE,TRUE,0);

  label = gtk_label_new("model");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_data2),label,TRUE,TRUE,0);

  box_data2_list_0 = gtk_option_menu_new();
  gtk_widget_ref(box_data2_list_0);
  menu = gtk_menu_new();
  init_model_main_menu(menu,general_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_data2_list_0),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_data2_list_0),0);
  gtk_widget_show(box_data2_list_0);
  gtk_box_pack_start(GTK_BOX(box_data2),box_data2_list_0,TRUE,TRUE,0);

  label = gtk_label_new("cnt/int");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_data2),label,TRUE,TRUE,0);

  box_data2_list_1 = gtk_option_menu_new();
  gtk_widget_ref(box_data2_list_1);
  menu = gtk_menu_new();
  menu_item = gtk_menu_item_new_with_label ("cnt");
  gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)handle_cntint,"cnt");
  gtk_menu_append(GTK_MENU(menu),menu_item);
  gtk_widget_show(menu_item);
  menu_item = gtk_menu_item_new_with_label ("int");
  gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                        (GtkSignalFunc)handle_cntint,"int");
  gtk_menu_append(GTK_MENU(menu),menu_item);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_data2_list_1),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_data2_list_1),0);
  gtk_widget_show(box_data2_list_1);
  gtk_box_pack_start(GTK_BOX(box_data2),box_data2_list_1,TRUE,TRUE,0);

  box_title = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_title);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_title",box_title,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_title);
  gtk_box_pack_start(GTK_BOX(vbox1),box_title,FALSE,FALSE,0);

  label = gtk_label_new("Title");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_title),label,FALSE,FALSE,0);

  text_title = gtk_entry_new();
  gtk_widget_ref(text_title);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"text_title",text_title,(GtkDestroyNotify)gtk_widget_unref);
  gtk_entry_set_text((GtkEntry *)text_title,"");
  gtk_widget_show(text_title);
  gtk_box_pack_start(GTK_BOX(box_title),text_title,TRUE,TRUE,0);


	/* General parameters */

  box_param_h = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref(box_param_h);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_param_h",box_param_h,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_param_h);
  gtk_box_pack_start(GTK_BOX(vbox1),box_param_h,FALSE,FALSE,0);

  gen_par_frame = gtk_frame_new("General parameters");
  gtk_box_pack_start(GTK_BOX(box_param_h),gen_par_frame,FALSE,FALSE,0);
  gtk_frame_set_label_align(GTK_FRAME(gen_par_frame),0.5,0.5);
  gdk_color_parse("#7777FF", &color);
  gtk_widget_modify_fg(GTK_LABEL(gtk_frame_get_label_widget(gen_par_frame)), GTK_STATE_NORMAL, &color);
  gtk_widget_show(gen_par_frame);

  box_param_v = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref(box_param_v);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_param_v",box_param_v,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_param_v);
  gtk_widget_set_usize(box_param_v,600,0);
  gtk_container_add(GTK_FRAME(gen_par_frame),box_param_v);

  box_bg = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_bg);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg",box_bg,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_bg);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_bg,FALSE,FALSE,0);

  label = gtk_label_new("Backgr. (BG)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_bg),label,TRUE,TRUE,0);

  box_bg_text_0 = gtk_entry_new();
  gtk_widget_ref(box_bg_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg_text_0",box_bg_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_bg_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_bg_text_0,"0");
  gtk_widget_show(box_bg_text_0);
  gtk_box_pack_start(GTK_BOX(box_bg),box_bg_text_0,TRUE,TRUE,0);

  box_bg_text_1 = gtk_entry_new();
  gtk_widget_ref(box_bg_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg_text_1",box_bg_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_bg_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_bg_text_1,"0");
  gtk_widget_show(box_bg_text_1);
  gtk_box_pack_start(GTK_BOX(box_bg),box_bg_text_1,TRUE,TRUE,0);

  box_bg_text_2 = gtk_entry_new();
  gtk_widget_ref(box_bg_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg_text_2",box_bg_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_bg_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_bg_text_2,"0");
  gtk_widget_show(box_bg_text_2);
  gtk_box_pack_start(GTK_BOX(box_bg),box_bg_text_2,TRUE,TRUE,0);

  box_bg_text_3 = gtk_entry_new();
  gtk_widget_ref(box_bg_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg_text_3",box_bg_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_bg_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_bg_text_3,"0");
  gtk_widget_show(box_bg_text_3);
  gtk_box_pack_start(GTK_BOX(box_bg),box_bg_text_3,TRUE,TRUE,0);

  box_bg_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_bg_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bg_check_0",box_bg_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_bg_check_0,30,20);
  gtk_widget_show(box_bg_check_0);
  gtk_signal_connect (GTK_OBJECT(box_bg_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,0);
  gtk_box_pack_start(GTK_BOX(box_bg),box_bg_check_0,TRUE,TRUE,0);

  box_sl = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_sl);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl",box_sl,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_sl);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_sl,FALSE,FALSE,0);

  label = gtk_label_new("Slope (SL)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_sl),label,TRUE,TRUE,0);

  box_sl_text_0 = gtk_entry_new();
  gtk_widget_ref(box_sl_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl_text_0",box_sl_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_sl_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_sl_text_0,"0");
  gtk_widget_show(box_sl_text_0);
  gtk_box_pack_start(GTK_BOX(box_sl),box_sl_text_0,TRUE,TRUE,0);

  box_sl_text_1 = gtk_entry_new();
  gtk_widget_ref(box_sl_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl_text_1",box_sl_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_sl_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_sl_text_1,"0");
  gtk_widget_show(box_sl_text_1);
  gtk_box_pack_start(GTK_BOX(box_sl),box_sl_text_1,TRUE,TRUE,0);

  box_sl_text_2 = gtk_entry_new();
  gtk_widget_ref(box_sl_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl_text_2",box_sl_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_sl_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_sl_text_2,"0");
  gtk_widget_show(box_sl_text_2);
  gtk_box_pack_start(GTK_BOX(box_sl),box_sl_text_2,TRUE,TRUE,0);

  box_sl_text_3 = gtk_entry_new();
  gtk_widget_ref(box_sl_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl_text_3",box_sl_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_sl_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_sl_text_3,"0");
  gtk_widget_show(box_sl_text_3);
  gtk_box_pack_start(GTK_BOX(box_sl),box_sl_text_3,TRUE,TRUE,0);

  box_sl_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_sl_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_sl_check_0",box_sl_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_sl_check_0,30,20);
  gtk_widget_show(box_sl_check_0);
  gtk_signal_connect (GTK_OBJECT(box_sl_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)1);
  gtk_box_pack_start(GTK_BOX(box_sl),box_sl_check_0,TRUE,TRUE,0);

  box_zr = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_zr);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr",box_zr,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_zr);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_zr,FALSE,FALSE,0);

  label = gtk_label_new("Elas. line (ZR)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_zr),label,TRUE,TRUE,0);

  box_zr_text_0 = gtk_entry_new();
  gtk_widget_ref(box_zr_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr_text_0",box_zr_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_zr_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_zr_text_0,"0");
  gtk_widget_show(box_zr_text_0);
  gtk_box_pack_start(GTK_BOX(box_zr),box_zr_text_0,TRUE,TRUE,0);

  box_zr_text_1 = gtk_entry_new();
  gtk_widget_ref(box_zr_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr_text_1",box_zr_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_zr_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_zr_text_1,"0");
  gtk_widget_show(box_zr_text_1);
  gtk_box_pack_start(GTK_BOX(box_zr),box_zr_text_1,TRUE,TRUE,0);

  box_zr_text_2 = gtk_entry_new();
  gtk_widget_ref(box_zr_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr_text_2",box_zr_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_zr_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_zr_text_2,"0");
  gtk_widget_show(box_zr_text_2);
  gtk_box_pack_start(GTK_BOX(box_zr),box_zr_text_2,TRUE,TRUE,0);

  box_zr_text_3 = gtk_entry_new();
  gtk_widget_ref(box_zr_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr_text_3",box_zr_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_zr_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_zr_text_3,"0");
  gtk_widget_show(box_zr_text_3);
  gtk_box_pack_start(GTK_BOX(box_zr),box_zr_text_3,TRUE,TRUE,0);

  box_zr_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_zr_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_zr_check_0",box_zr_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_zr_check_0,30,20);
  gtk_widget_show(box_zr_check_0);
  gtk_signal_connect (GTK_OBJECT(box_zr_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)2);
  gtk_box_pack_start(GTK_BOX(box_zr),box_zr_check_0,TRUE,TRUE,0);

  box_gr = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_gr);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr",box_gr,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_gr);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_gr,FALSE,FALSE,0);

  gr_label = gtk_label_new("T");
  gtk_widget_set_usize(gr_label,30,20);
  gtk_widget_show(gr_label);
  gtk_box_pack_start(GTK_BOX(box_gr),gr_label,TRUE,TRUE,0);

  box_gr_text_0 = gtk_entry_new();
  gtk_widget_ref(box_gr_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr_text_0",box_gr_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gr_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_gr_text_0,"0");
  gtk_widget_show(box_gr_text_0);
  gtk_box_pack_start(GTK_BOX(box_gr),box_gr_text_0,TRUE,TRUE,0);

  box_gr_text_1 = gtk_entry_new();
  gtk_widget_ref(box_gr_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr_text_1",box_gr_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gr_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_gr_text_1,"0");
  gtk_widget_show(box_gr_text_1);
  gtk_box_pack_start(GTK_BOX(box_gr),box_gr_text_1,TRUE,TRUE,0);

  box_gr_text_2 = gtk_entry_new();
  gtk_widget_ref(box_gr_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr_text_2",box_gr_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gr_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_gr_text_2,"0");
  gtk_widget_show(box_gr_text_2);
  gtk_box_pack_start(GTK_BOX(box_gr),box_gr_text_2,TRUE,TRUE,0);

  box_gr_text_3 = gtk_entry_new();
  gtk_widget_ref(box_gr_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr_text_3",box_gr_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gr_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_gr_text_3,"0");
  gtk_widget_show(box_gr_text_3);
  gtk_box_pack_start(GTK_BOX(box_gr),box_gr_text_3,TRUE,TRUE,0);

  box_gr_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_gr_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gr_check_0",box_gr_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gr_check_0,30,20);
  gtk_widget_show(box_gr_check_0);
  gtk_signal_connect (GTK_OBJECT(box_gr_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)3);
  gtk_box_pack_start(GTK_BOX(box_gr),box_gr_check_0,TRUE,TRUE,0);

  box_pc = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_pc);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc",box_pc,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_pc);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_pc,FALSE,FALSE,0);

  label = gtk_label_new("Central int. (PC)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_pc),label,TRUE,TRUE,0);

  box_pc_text_0 = gtk_entry_new();
  gtk_widget_ref(box_pc_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc_text_0",box_pc_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_pc_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_pc_text_0,"0");
  gtk_widget_show(box_pc_text_0);
  gtk_box_pack_start(GTK_BOX(box_pc),box_pc_text_0,TRUE,TRUE,0);

  box_pc_text_1 = gtk_entry_new();
  gtk_widget_ref(box_pc_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc_text_1",box_pc_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_pc_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_pc_text_1,"0");
  gtk_widget_show(box_pc_text_1);
  gtk_box_pack_start(GTK_BOX(box_pc),box_pc_text_1,TRUE,TRUE,0);

  box_pc_text_2 = gtk_entry_new();
  gtk_widget_ref(box_pc_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc_text_2",box_pc_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_pc_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_pc_text_2,"0");
  gtk_widget_show(box_pc_text_2);
  gtk_box_pack_start(GTK_BOX(box_pc),box_pc_text_2,TRUE,TRUE,0);

  box_pc_text_3 = gtk_entry_new();
  gtk_widget_ref(box_pc_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc_text_3",box_pc_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_pc_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_pc_text_3,"0");
  gtk_widget_show(box_pc_text_3);
  gtk_box_pack_start(GTK_BOX(box_pc),box_pc_text_3,TRUE,TRUE,0);

  box_pc_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_pc_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_pc_check_0",box_pc_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_pc_check_0,30,20);
  gtk_widget_show(box_pc_check_0);
  gtk_signal_connect (GTK_OBJECT(box_pc_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)4);
  gtk_box_pack_start(GTK_BOX(box_pc),box_pc_check_0,TRUE,TRUE,0);

  box_gc = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_gc);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc",box_gc,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_gc);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_gc,FALSE,FALSE,0);

  label = gtk_label_new("Cent. width (GC)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_gc),label,TRUE,TRUE,0);

  box_gc_text_0 = gtk_entry_new();
  gtk_widget_ref(box_gc_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc_text_0",box_gc_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gc_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_gc_text_0,"0");
  gtk_widget_show(box_gc_text_0);
  gtk_box_pack_start(GTK_BOX(box_gc),box_gc_text_0,TRUE,TRUE,0);

  box_gc_text_1 = gtk_entry_new();
  gtk_widget_ref(box_gc_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc_text_1",box_gc_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gc_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_gc_text_1,"0");
  gtk_widget_show(box_gc_text_1);
  gtk_box_pack_start(GTK_BOX(box_gc),box_gc_text_1,TRUE,TRUE,0);

  box_gc_text_2 = gtk_entry_new();
  gtk_widget_ref(box_gc_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc_text_2",box_gc_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gc_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_gc_text_2,"0");
  gtk_widget_show(box_gc_text_2);
  gtk_box_pack_start(GTK_BOX(box_gc),box_gc_text_2,TRUE,TRUE,0);

  box_gc_text_3 = gtk_entry_new();
  gtk_widget_ref(box_gc_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc_text_3",box_gc_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gc_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_gc_text_3,"0");
  gtk_widget_show(box_gc_text_3);
  gtk_box_pack_start(GTK_BOX(box_gc),box_gc_text_3,TRUE,TRUE,0);

  box_gc_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_gc_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_gc_check_0",box_gc_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_gc_check_0,30,20);
  gtk_widget_show(box_gc_check_0);
  gtk_signal_connect (GTK_OBJECT(box_gc_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)5);
  gtk_box_pack_start(GTK_BOX(box_gc),box_gc_check_0,TRUE,TRUE,0);

	/* Beginning of the viscosity box */
  box_visco = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref(box_visco);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco",box_visco,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco);
  gtk_box_pack_start(GTK_BOX(box_param_v),box_visco,FALSE,FALSE,0);

  label = gtk_label_new("Viscoelastic excitation");
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco),label,TRUE,TRUE,0);

  box_visco_visi = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_visi);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi",box_visco_visi,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_visi);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_visi,FALSE,FALSE,0);

  label = gtk_label_new("Visco (visi)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),label,TRUE,TRUE,0);

  box_visco_visi_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_visi_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi_text_0",box_visco_visi_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_visi_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_visi_text_0,"100");
  gtk_widget_show(box_visco_visi_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),box_visco_visi_text_0,TRUE,TRUE,0);

  box_visco_visi_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_visi_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi_text_1",box_visco_visi_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_visi_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_visi_text_1,"1");
  gtk_widget_show(box_visco_visi_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),box_visco_visi_text_1,TRUE,TRUE,0);

  box_visco_visi_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_visi_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi_text_2",box_visco_visi_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_visi_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_visi_text_2,"0");
  gtk_widget_show(box_visco_visi_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),box_visco_visi_text_2,TRUE,TRUE,0);

  box_visco_visi_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_visi_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi_text_3",box_visco_visi_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_visi_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_visi_text_3,"1000000");
  gtk_widget_show(box_visco_visi_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),box_visco_visi_text_3,TRUE,TRUE,0);

  box_visco_visi_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_visi_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_visi_check_0",box_visco_visi_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_visi_check_0,30,20);
  gtk_widget_show(box_visco_visi_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_visi_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)0);
  gtk_box_pack_start(GTK_BOX(box_visco_visi),box_visco_visi_check_0,TRUE,TRUE,0);

  box_visco_omega0 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_omega0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0",box_visco_omega0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_omega0);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_omega0,FALSE,FALSE,0);

  label = gtk_label_new("Visco (omega0)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),label,TRUE,TRUE,0);

  box_visco_omega0_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_omega0_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0_text_0",box_visco_omega0_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omega0_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omega0_text_0,"1");
  gtk_widget_show(box_visco_omega0_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),box_visco_omega0_text_0,TRUE,TRUE,0);

  box_visco_omega0_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_omega0_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0_text_1",box_visco_omega0_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omega0_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omega0_text_1,"0.01");
  gtk_widget_show(box_visco_omega0_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),box_visco_omega0_text_1,TRUE,TRUE,0);

  box_visco_omega0_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_omega0_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0_text_2",box_visco_omega0_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omega0_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omega0_text_2,"0.1");
  gtk_widget_show(box_visco_omega0_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),box_visco_omega0_text_2,TRUE,TRUE,0);

  box_visco_omega0_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_omega0_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0_text_3",box_visco_omega0_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omega0_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omega0_text_3,"100");
  gtk_widget_show(box_visco_omega0_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),box_visco_omega0_text_3,TRUE,TRUE,0);

  box_visco_omega0_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_omega0_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omega0_check_0",box_visco_omega0_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omega0_check_0,30,20);
  gtk_widget_show(box_visco_omega0_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_omega0_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)1);
  gtk_box_pack_start(GTK_BOX(box_visco_omega0),box_visco_omega0_check_0,TRUE,TRUE,0);

  box_visco_gamma = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_gamma);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma",box_visco_gamma,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_gamma);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_gamma,FALSE,FALSE,0);

  label = gtk_label_new("Visco (gamma)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),label,TRUE,TRUE,0);

  box_visco_gamma_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma_text_0",box_visco_gamma_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma_text_0,"2");
  gtk_widget_show(box_visco_gamma_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),box_visco_gamma_text_0,TRUE,TRUE,0);

  box_visco_gamma_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma_text_1",box_visco_gamma_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma_text_1,"0.01");
  gtk_widget_show(box_visco_gamma_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),box_visco_gamma_text_1,TRUE,TRUE,0);

  box_visco_gamma_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma_text_2",box_visco_gamma_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma_text_2,"1");
  gtk_widget_show(box_visco_gamma_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),box_visco_gamma_text_2,TRUE,TRUE,0);

  box_visco_gamma_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma_text_3",box_visco_gamma_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma_text_3,"10");
  gtk_widget_show(box_visco_gamma_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),box_visco_gamma_text_3,TRUE,TRUE,0);

  box_visco_gamma_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_gamma_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma_check_0",box_visco_gamma_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma_check_0,30,20);
  gtk_widget_show(box_visco_gamma_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_gamma_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)2);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma),box_visco_gamma_check_0,TRUE,TRUE,0);

  box_visco_taut = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_taut);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut",box_visco_taut,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_taut);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_taut,FALSE,FALSE,0);

  label = gtk_label_new("Visco (taut)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),label,TRUE,TRUE,0);

  box_visco_taut_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_taut_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut_text_0",box_visco_taut_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taut_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taut_text_0,"1");
  gtk_widget_show(box_visco_taut_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),box_visco_taut_text_0,TRUE,TRUE,0);

  box_visco_taut_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_taut_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut_text_1",box_visco_taut_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taut_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taut_text_1,"0.01");
  gtk_widget_show(box_visco_taut_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),box_visco_taut_text_1,TRUE,TRUE,0);

  box_visco_taut_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_taut_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut_text_2",box_visco_taut_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taut_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taut_text_2,"0");
  gtk_widget_show(box_visco_taut_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),box_visco_taut_text_2,TRUE,TRUE,0);

  box_visco_taut_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_taut_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut_text_3",box_visco_taut_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taut_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taut_text_3,"100");
  gtk_widget_show(box_visco_taut_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),box_visco_taut_text_3,TRUE,TRUE,0);

  box_visco_taut_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_taut_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taut_check_0",box_visco_taut_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taut_check_0,30,20);
  gtk_widget_show(box_visco_taut_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_taut_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)3);
  gtk_box_pack_start(GTK_BOX(box_visco_taut),box_visco_taut_check_0,TRUE,TRUE,0);

  box_visco_omegainf = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_omegainf);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf",box_visco_omegainf,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_omegainf);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_omegainf,FALSE,FALSE,0);

  label = gtk_label_new("Visco (omegainf)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),label,TRUE,TRUE,0);

  box_visco_omegainf_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_omegainf_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf_text_0",box_visco_omegainf_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omegainf_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omegainf_text_0,"2");
  gtk_widget_show(box_visco_omegainf_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),box_visco_omegainf_text_0,TRUE,TRUE,0);

  box_visco_omegainf_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_omegainf_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf_text_1",box_visco_omegainf_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omegainf_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omegainf_text_1,"0.01");
  gtk_widget_show(box_visco_omegainf_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),box_visco_omegainf_text_1,TRUE,TRUE,0);

  box_visco_omegainf_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_omegainf_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf_text_2",box_visco_omegainf_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omegainf_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omegainf_text_2,"1");
  gtk_widget_show(box_visco_omegainf_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),box_visco_omegainf_text_2,TRUE,TRUE,0);

  box_visco_omegainf_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_omegainf_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf_text_3",box_visco_omegainf_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omegainf_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_omegainf_text_3,"100");
  gtk_widget_show(box_visco_omegainf_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),box_visco_omegainf_text_3,TRUE,TRUE,0);

  box_visco_omegainf_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_omegainf_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_omegainf_check_0",box_visco_omegainf_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_omegainf_check_0,30,20);
  gtk_widget_show(box_visco_omegainf_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_omegainf_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)4);
  gtk_box_pack_start(GTK_BOX(box_visco_omegainf),box_visco_omegainf_check_0,TRUE,TRUE,0);

  box_visco_tau1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_tau1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1",box_visco_tau1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_tau1);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_tau1,FALSE,FALSE,0);

  label = gtk_label_new("Visco (tau1)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),label,TRUE,TRUE,0);

  box_visco_tau1_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_tau1_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1_text_0",box_visco_tau1_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_tau1_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_tau1_text_0,"1");
  gtk_widget_show(box_visco_tau1_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),box_visco_tau1_text_0,TRUE,TRUE,0);

  box_visco_tau1_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_tau1_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1_text_1",box_visco_tau1_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_tau1_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_tau1_text_1,"0.01");
  gtk_widget_show(box_visco_tau1_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),box_visco_tau1_text_1,TRUE,TRUE,0);

  box_visco_tau1_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_tau1_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1_text_2",box_visco_tau1_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_tau1_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_tau1_text_2,"0");
  gtk_widget_show(box_visco_tau1_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),box_visco_tau1_text_2,TRUE,TRUE,0);

  box_visco_tau1_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_tau1_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1_text_3",box_visco_tau1_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_tau1_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_tau1_text_3,"100");
  gtk_widget_show(box_visco_tau1_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),box_visco_tau1_text_3,TRUE,TRUE,0);

  box_visco_tau1_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_tau1_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_tau1_check_0",box_visco_tau1_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_tau1_check_0,30,20);
  gtk_widget_show(box_visco_tau1_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_tau1_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)5);
  gtk_box_pack_start(GTK_BOX(box_visco_tau1),box_visco_tau1_check_0,TRUE,TRUE,0);

  box_visco_gamma0 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_gamma0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0",box_visco_gamma0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_gamma0);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_gamma0,FALSE,FALSE,0);

  label = gtk_label_new("Visco (gamma0)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),label,TRUE,TRUE,0);

  box_visco_gamma0_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma0_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0_text_0",box_visco_gamma0_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma0_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma0_text_0,"1");
  gtk_widget_show(box_visco_gamma0_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),box_visco_gamma0_text_0,TRUE,TRUE,0);

  box_visco_gamma0_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma0_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0_text_1",box_visco_gamma0_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma0_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma0_text_1,"0.01");
  gtk_widget_show(box_visco_gamma0_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),box_visco_gamma0_text_1,TRUE,TRUE,0);

  box_visco_gamma0_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma0_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0_text_2",box_visco_gamma0_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma0_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma0_text_2,"0");
  gtk_widget_show(box_visco_gamma0_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),box_visco_gamma0_text_2,TRUE,TRUE,0);

  box_visco_gamma0_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_gamma0_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0_text_3",box_visco_gamma0_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma0_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_gamma0_text_3,"100");
  gtk_widget_show(box_visco_gamma0_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),box_visco_gamma0_text_3,TRUE,TRUE,0);

  box_visco_gamma0_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_gamma0_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_gamma0_check_0",box_visco_gamma0_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_gamma0_check_0,30,20);
  gtk_widget_show(box_visco_gamma0_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_gamma0_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)6);
  gtk_box_pack_start(GTK_BOX(box_visco_gamma0),box_visco_gamma0_check_0,TRUE,TRUE,0);

  box_visco_delta = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_delta);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta",box_visco_delta,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_delta);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_delta,FALSE,FALSE,0);

  label = gtk_label_new("Visco (delta)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),label,TRUE,TRUE,0);

  box_visco_delta_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_delta_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta_text_0",box_visco_delta_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_delta_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_delta_text_0,"1");
  gtk_widget_show(box_visco_delta_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),box_visco_delta_text_0,TRUE,TRUE,0);

  box_visco_delta_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_delta_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta_text_1",box_visco_delta_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_delta_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_delta_text_1,"0.01");
  gtk_widget_show(box_visco_delta_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),box_visco_delta_text_1,TRUE,TRUE,0);

  box_visco_delta_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_delta_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta_text_2",box_visco_delta_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_delta_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_delta_text_2,"0");
  gtk_widget_show(box_visco_delta_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),box_visco_delta_text_2,TRUE,TRUE,0);

  box_visco_delta_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_delta_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta_text_3",box_visco_delta_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_delta_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_delta_text_3,"10");
  gtk_widget_show(box_visco_delta_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),box_visco_delta_text_3,TRUE,TRUE,0);

  box_visco_delta_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_delta_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_delta_check_0",box_visco_delta_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_delta_check_0,30,20);
  gtk_widget_show(box_visco_delta_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_delta_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)7);
  gtk_box_pack_start(GTK_BOX(box_visco_delta),box_visco_delta_check_0,TRUE,TRUE,0);

  box_visco_taud = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_taud);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud",box_visco_taud,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_taud);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_taud,FALSE,FALSE,0);

  label = gtk_label_new("Visco (taud)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),label,TRUE,TRUE,0);

  box_visco_taud_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_taud_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud_text_0",box_visco_taud_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taud_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taud_text_0,"1");
  gtk_widget_show(box_visco_taud_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),box_visco_taud_text_0,TRUE,TRUE,0);

  box_visco_taud_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_taud_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud_text_1",box_visco_taud_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taud_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taud_text_1,"0.01");
  gtk_widget_show(box_visco_taud_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),box_visco_taud_text_1,TRUE,TRUE,0);

  box_visco_taud_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_taud_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud_text_2",box_visco_taud_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taud_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taud_text_2,"0");
  gtk_widget_show(box_visco_taud_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),box_visco_taud_text_2,TRUE,TRUE,0);

  box_visco_taud_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_taud_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud_text_3",box_visco_taud_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taud_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_taud_text_3,"100");
  gtk_widget_show(box_visco_taud_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),box_visco_taud_text_3,TRUE,TRUE,0);

  box_visco_taud_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_taud_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_taud_check_0",box_visco_taud_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_taud_check_0,30,20);
  gtk_widget_show(box_visco_taud_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_taud_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)8);
  gtk_box_pack_start(GTK_BOX(box_visco_taud),box_visco_taud_check_0,TRUE,TRUE,0);

  box_visco_beta = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_visco_beta);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta",box_visco_beta,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_visco_beta);
  gtk_box_pack_start(GTK_BOX(box_visco),box_visco_beta,FALSE,FALSE,0);

  label = gtk_label_new("Visco (beta)");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),label,TRUE,TRUE,0);

  box_visco_beta_text_0 = gtk_entry_new();
  gtk_widget_ref(box_visco_beta_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta_text_0",box_visco_beta_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_beta_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_beta_text_0,"0.8");
  gtk_widget_show(box_visco_beta_text_0);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),box_visco_beta_text_0,TRUE,TRUE,0);

  box_visco_beta_text_1 = gtk_entry_new();
  gtk_widget_ref(box_visco_beta_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta_text_1",box_visco_beta_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_beta_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_beta_text_1,"0.01");
  gtk_widget_show(box_visco_beta_text_1);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),box_visco_beta_text_1,TRUE,TRUE,0);

  box_visco_beta_text_2 = gtk_entry_new();
  gtk_widget_ref(box_visco_beta_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta_text_2",box_visco_beta_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_beta_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_beta_text_2,"0");
  gtk_widget_show(box_visco_beta_text_2);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),box_visco_beta_text_2,TRUE,TRUE,0);

  box_visco_beta_text_3 = gtk_entry_new();
  gtk_widget_ref(box_visco_beta_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta_text_3",box_visco_beta_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_beta_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_visco_beta_text_3,"1");
  gtk_widget_show(box_visco_beta_text_3);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),box_visco_beta_text_3,TRUE,TRUE,0);

  box_visco_beta_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_visco_beta_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_beta_check_0",box_visco_beta_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_visco_beta_check_0,30,20);
  gtk_widget_show(box_visco_beta_check_0);
  gtk_signal_connect (GTK_OBJECT(box_visco_beta_check_0),"toggled",
                        (GtkSignalFunc)fix_visco_callback,(gpointer)9);
  gtk_box_pack_start(GTK_BOX(box_visco_beta),box_visco_beta_check_0,TRUE,TRUE,0);

	box_visco_check_e2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref(box_visco_check_e2);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_visco_check_e2",box_visco_check_e2,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_visco_check_e2);
	gtk_box_pack_start(GTK_BOX(box_visco),box_visco_check_e2,FALSE,FALSE,0);

	visco_check_e2 = gtk_check_button_new_with_label("Display and sum f.EE");
	gtk_widget_ref(visco_check_e2);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"visco_check_e2",visco_check_e2,(GtkDestroyNotify)gtk_widget_unref);
	gtk_signal_connect(GTK_OBJECT(visco_check_e2),"toggled",
		GTK_SIGNAL_FUNC(on_visco_check_e2_activate),(gpointer)0);
	if (fl_visco_e2)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(visco_check_e2),TRUE);
	gtk_widget_show(visco_check_e2);
	gtk_box_pack_start(GTK_BOX(box_visco_check_e2),visco_check_e2,TRUE,TRUE,0);


	/* Selection of the kind of fit */

	fit_seq_frame = gtk_frame_new("Fit parameters");
	gtk_box_pack_start(GTK_BOX(box_param_h),fit_seq_frame,TRUE,TRUE,0);
	gtk_frame_set_label_align(GTK_FRAME(fit_seq_frame),0.5,0.5);
	gdk_color_parse("#7777FF", &color);
	gtk_widget_modify_fg(GTK_LABEL(gtk_frame_get_label_widget(fit_seq_frame)), GTK_STATE_NORMAL, &color);
	gtk_widget_show(fit_seq_frame);

	box_fits_v = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref(box_fits_v);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_fits_v",box_fits_v,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_fits_v);
	gtk_container_add(GTK_FRAME(fit_seq_frame),box_fits_v);

	fit_type_simplex = gtk_check_button_new_with_label("SIMPLEX");
	gtk_widget_ref(fit_type_simplex);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"fit_type_simplex",fit_type_simplex,(GtkDestroyNotify)gtk_widget_unref);
	gtk_signal_connect(GTK_OBJECT(fit_type_simplex),"toggled",
		GTK_SIGNAL_FUNC(on_fit_type_activate),(gpointer)0);
	if (fit_types[0])
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fit_type_simplex),TRUE);
	gtk_widget_show(fit_type_simplex);
	gtk_box_pack_start(GTK_BOX(box_fits_v),fit_type_simplex,TRUE,TRUE,0);





	fit_type_migrad = gtk_check_button_new_with_label("MIGRAD");
	gtk_widget_ref(fit_type_migrad);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"fit_type_migrad",fit_type_migrad,(GtkDestroyNotify)gtk_widget_unref);
	gtk_signal_connect(GTK_OBJECT(fit_type_migrad),"toggled",
		GTK_SIGNAL_FUNC(on_fit_type_activate),(gpointer)1);
	if (fit_types[1])
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fit_type_migrad),TRUE);
	gtk_widget_show(fit_type_migrad);
	gtk_box_pack_start(GTK_BOX(box_fits_v),fit_type_migrad,TRUE,TRUE,0);

	/* --------------------------------------------------------- */
	calc_type_fft = gtk_check_button_new_with_label("FFT");
	gtk_widget_ref(calc_type_fft);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"calc_type_fft",calc_type_fft,(GtkDestroyNotify)gtk_widget_unref);

	gtk_signal_connect(GTK_OBJECT(calc_type_fft),"toggled",
		GTK_SIGNAL_FUNC(on_fft_activate  ),(gpointer)1);

	if ( fft_active)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(calc_type_fft),TRUE);
	gtk_widget_show(calc_type_fft);
	gtk_box_pack_start(GTK_BOX(box_fits_v),calc_type_fft,TRUE,TRUE,0);

	/* ----------------------------------------------------------- */

	separator1 = gtk_menu_item_new ();
	gtk_widget_ref (separator1);
	gtk_object_set_data_full (GTK_OBJECT (main_window), "separator1", separator1,
                	    (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (separator1);
	gtk_container_add (GTK_CONTAINER (box_fits_v), separator1);

	central_bose = gtk_check_button_new_with_label("Use Bose for PC");
	gtk_widget_ref(central_bose);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"central_bose",central_bose,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(central_bose);
	gtk_signal_connect (GTK_OBJECT(central_bose),"toggled",
		(GtkSignalFunc)central_bose_callback,(gpointer)1);
	gtk_box_pack_start(GTK_BOX(box_fits_v),central_bose,FALSE,FALSE,0);

	box_central_type = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref(box_central_type);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_central_type",box_central_type,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_central_type);
	gtk_box_pack_start(GTK_BOX(box_fits_v),box_central_type,FALSE,FALSE,0);

	label = gtk_label_new("C. shape");
	gtk_widget_set_usize(label,30,20);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(box_central_type),label,TRUE,TRUE,0);

	central_list = gtk_option_menu_new();
	gtk_widget_ref(central_list);
	menu = gtk_menu_new();
	menu_item = gtk_menu_item_new_with_label ("lor");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                	(GtkSignalFunc)handle_central_type,"lor");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	menu_item = gtk_menu_item_new_with_label ("del");
	gtk_signal_connect (GTK_OBJECT(menu_item),"activate",
                	(GtkSignalFunc)handle_central_type,"del");
	gtk_menu_append(GTK_MENU(menu),menu_item);
	gtk_widget_show(menu_item);
	gtk_option_menu_set_menu(GTK_OPTION_MENU(central_list),menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(central_list),0);
	gtk_widget_show(central_list);
	gtk_box_pack_start(GTK_BOX(box_central_type),central_list,TRUE,TRUE,0);

	separator1 = gtk_menu_item_new ();
	gtk_widget_ref (separator1);
	gtk_object_set_data_full (GTK_OBJECT (main_window), "separator1", separator1,
                	    (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (separator1);
	gtk_container_add (GTK_CONTAINER (box_fits_v), separator1);

	box_res_file = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref(box_res_file);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_res_file",box_res_file,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_res_file);
	gtk_box_pack_start(GTK_BOX(box_fits_v),box_res_file,FALSE,FALSE,0);

	label = gtk_label_new("Resolution file");
	gtk_widget_set_usize(label,30,20);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(box_res_file),label,TRUE,TRUE,0);

	box_res_file_text = gtk_entry_new();
	gtk_widget_ref(box_res_file_text);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_res_file_text",box_res_file_text,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_set_usize(box_res_file_text,30,20);
	gtk_entry_set_text((GtkEntry *)box_res_file_text,"?");
	gtk_widget_show(box_res_file_text);
	gtk_box_pack_start(GTK_BOX(box_res_file),box_res_file_text,TRUE,TRUE,0);

	box_res_file_button = gtk_button_new_with_label("Auto");
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_res_file_button",box_res_file_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_res_file_button);
	gtk_box_pack_start(GTK_BOX(box_res_file),box_res_file_button,FALSE,FALSE,0);

	box_convol = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref(box_convol);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol",box_convol,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_convol);
	gtk_box_pack_start(GTK_BOX(box_fits_v),box_convol,FALSE,FALSE,0);

	pv_load_button = gtk_button_new();
	pv_load_label = gtk_label_new("P-V load");
	gtk_container_add(GTK_CONTAINER(pv_load_button),pv_load_label);
	gtk_widget_show(pv_load_label);
	gtk_widget_show(pv_load_button);
	gtk_box_pack_start(GTK_BOX(box_convol),pv_load_button,TRUE,TRUE,0);

	label = gtk_label_new("L");
	gtk_widget_set_usize(label,30,20);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(box_convol),label,TRUE,TRUE,0);

	box_convol_lor_width = gtk_entry_new();
	gtk_widget_ref(box_convol_lor_width);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol_lor_width",box_convol_lor_width,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_set_usize(box_convol_lor_width,30,20);
	gtk_entry_set_text((GtkEntry *)box_convol_lor_width,"1.7");
	gtk_widget_show(box_convol_lor_width);
	gtk_box_pack_start(GTK_BOX(box_convol),box_convol_lor_width,TRUE,TRUE,0);

	label = gtk_label_new("G");
	gtk_widget_set_usize(label,30,20);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(box_convol),label,TRUE,TRUE,0);

	box_convol_gau_width = gtk_entry_new();
	gtk_widget_ref(box_convol_gau_width);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol_gau_width",box_convol_gau_width,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_set_usize(box_convol_gau_width,30,20);
	gtk_entry_set_text((GtkEntry *)box_convol_gau_width,"1.7");
	gtk_widget_show(box_convol_gau_width);
	gtk_box_pack_start(GTK_BOX(box_convol),box_convol_gau_width,TRUE,TRUE,0);

	label = gtk_label_new("eta");
	gtk_widget_set_usize(label,30,20);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(box_convol),label,TRUE,TRUE,0);

	box_convol_eta = gtk_entry_new();
	gtk_widget_ref(box_convol_eta);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol_eta",box_convol_eta,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_set_usize(box_convol_eta,30,20);
	gtk_entry_set_text((GtkEntry *)box_convol_eta,"0.5");
	gtk_widget_show(box_convol_eta);
	gtk_box_pack_start(GTK_BOX(box_convol),box_convol_eta,TRUE,TRUE,0);

	box_convol_choices_hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref(box_convol_choices_hbox);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol_choices_hbox",box_convol_choices_hbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_convol_choices_hbox);
	gtk_box_pack_start(GTK_BOX(box_fits_v),box_convol_choices_hbox,FALSE,FALSE,0);

	box_convol_choices = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref(box_convol_choices);
	gtk_object_set_data_full(GTK_OBJECT(main_window),"box_convol_choices",box_convol_choices,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(box_convol_choices);
	gtk_box_pack_start(GTK_BOX(box_convol_choices_hbox),box_convol_choices,FALSE,FALSE,0);

	convol_radio0 = gtk_radio_button_new_with_label(NULL,"Convolution");
	gtk_widget_show(convol_radio0);
	gtk_box_pack_start(GTK_BOX(box_convol_choices),convol_radio0,TRUE,TRUE,0);
	convol_radio1 = gtk_radio_button_new_with_label_from_widget(
		GTK_RADIO_BUTTON(convol_radio0),"No convolution");
	gtk_widget_show(convol_radio1);
	gtk_box_pack_start(GTK_BOX(box_convol_choices),convol_radio1,TRUE,TRUE,0);
	convol_radio2 = gtk_radio_button_new_with_label_from_widget(
		GTK_RADIO_BUTTON(convol_radio0),"Use P-V for convolution");
	gtk_widget_show(convol_radio2);
	gtk_box_pack_start(GTK_BOX(box_convol_choices),convol_radio2,TRUE,TRUE,0);
	convol_radio3 = gtk_radio_button_new_with_label_from_widget(
		GTK_RADIO_BUTTON(convol_radio0),"Use P-V everywhere");
	gtk_widget_show(convol_radio3);
	gtk_box_pack_start(GTK_BOX(box_convol_choices),convol_radio3,TRUE,TRUE,0);
	gtk_toggle_button_set_active(convol_radio0,TRUE);

	/* Beginning of the excitations. */
  excit_frame = gtk_frame_new("Excitations");
  gtk_box_pack_start(GTK_BOX(vbox1),excit_frame,FALSE,FALSE,0);
  gtk_frame_set_label_align(GTK_FRAME(excit_frame),0.5,0.5);
  gdk_color_parse("#7777FF", &color);
  gtk_widget_modify_fg(GTK_LABEL(gtk_frame_get_label_widget(excit_frame)), GTK_STATE_NORMAL, &color);
  gtk_widget_show(excit_frame);

  vbox_excit = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref(vbox_excit);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"vbox_excit",vbox_excit,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(vbox_excit);
  gtk_container_add(GTK_FRAME(excit_frame),vbox_excit);

  box_exc1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1",box_exc1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc1);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc1,FALSE,FALSE,0);

  exc1_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc1_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc1_swp_toggle",exc1_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc1_swp_toggle,40,20);
  gtk_widget_show(exc1_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc1_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)1);
  gtk_box_pack_start(GTK_BOX(box_exc1),exc1_swp_toggle,TRUE,TRUE,0);

  box_exc1_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc1_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc1_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc1_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc1_model),0);
  gtk_widget_show(box_exc1_model);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_model,TRUE,TRUE,0);

  label = gtk_label_new("P1");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc1),label,TRUE,TRUE,0);

  box_exc1_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_0",box_exc1_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_0,"0");
  gtk_widget_show(box_exc1_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_0,TRUE,TRUE,0);

  box_exc1_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_1",box_exc1_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_1,"0");
  gtk_widget_show(box_exc1_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_1,TRUE,TRUE,0);

  box_exc1_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_2",box_exc1_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_2,"0");
  gtk_widget_show(box_exc1_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_2,TRUE,TRUE,0);

  box_exc1_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_3",box_exc1_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_3,"0");
  gtk_widget_show(box_exc1_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_3,TRUE,TRUE,0);

  box_exc1_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc1_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_check_0",box_exc1_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_check_0,30,20);
  gtk_widget_show(box_exc1_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc1_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)6);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_check_0,TRUE,TRUE,0);

  label = gtk_label_new("01");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc1),label,TRUE,TRUE,0);

  box_exc1_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_4",box_exc1_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_4,"0");
  gtk_widget_show(box_exc1_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_4,TRUE,TRUE,0);

  box_exc1_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_5",box_exc1_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_5,"0");
  gtk_widget_show(box_exc1_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_5,TRUE,TRUE,0);

  box_exc1_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_6",box_exc1_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_6,"0");
  gtk_widget_show(box_exc1_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_6,TRUE,TRUE,0);

  box_exc1_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_7",box_exc1_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_7,"0");
  gtk_widget_show(box_exc1_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_7,TRUE,TRUE,0);

  box_exc1_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc1_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_check_1",box_exc1_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_check_1,30,20);
  gtk_widget_show(box_exc1_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc1_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)7);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G1");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc1),label,TRUE,TRUE,0);

  box_exc1_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_8",box_exc1_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_8,"0");
  gtk_widget_show(box_exc1_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_8,TRUE,TRUE,0);

  box_exc1_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_9",box_exc1_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_9,"0");
  gtk_widget_show(box_exc1_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_9,TRUE,TRUE,0);

  box_exc1_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_10",box_exc1_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_10,"0");
  gtk_widget_show(box_exc1_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_10,TRUE,TRUE,0);

  box_exc1_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc1_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_text_11",box_exc1_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc1_text_11,"0");
  gtk_widget_show(box_exc1_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_text_11,TRUE,TRUE,0);

  box_exc1_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc1_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc1_check_2",box_exc1_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc1_check_2,30,20);
  gtk_widget_show(box_exc1_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc1_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)8);
  gtk_box_pack_start(GTK_BOX(box_exc1),box_exc1_check_2,TRUE,TRUE,0);


  box_exc2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2",box_exc2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc2);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc2,FALSE,FALSE,0);

  exc2_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc2_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc2_swp_toggle",exc2_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc2_swp_toggle,40,20);
  gtk_widget_show(exc2_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc2_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)2);
  gtk_box_pack_start(GTK_BOX(box_exc2),exc2_swp_toggle,TRUE,TRUE,0);

  box_exc2_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc2_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc2_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc2_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc2_model),0);
  gtk_widget_show(box_exc2_model);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_model,TRUE,TRUE,0);

  label = gtk_label_new("P2");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc2),label,TRUE,TRUE,0);

  box_exc2_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_0",box_exc2_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_0,"0");
  gtk_widget_show(box_exc2_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_0,TRUE,TRUE,0);

  box_exc2_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_1",box_exc2_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_1,"0");
  gtk_widget_show(box_exc2_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_1,TRUE,TRUE,0);

  box_exc2_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_2",box_exc2_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_2,"0");
  gtk_widget_show(box_exc2_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_2,TRUE,TRUE,0);

  box_exc2_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_3",box_exc2_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_3,"0");
  gtk_widget_show(box_exc2_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_3,TRUE,TRUE,0);

  box_exc2_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc2_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_check_0",box_exc2_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_check_0,30,20);
  gtk_widget_show(box_exc2_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc2_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)9);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_check_0,TRUE,TRUE,0);

  label = gtk_label_new("02");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc2),label,TRUE,TRUE,0);

  box_exc2_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_4",box_exc2_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_4,"0");
  gtk_widget_show(box_exc2_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_4,TRUE,TRUE,0);

  box_exc2_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_5",box_exc2_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_5,"0");
  gtk_widget_show(box_exc2_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_5,TRUE,TRUE,0);

  box_exc2_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_6",box_exc2_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_6,"0");
  gtk_widget_show(box_exc2_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_6,TRUE,TRUE,0);

  box_exc2_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_7",box_exc2_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_7,"0");
  gtk_widget_show(box_exc2_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_7,TRUE,TRUE,0);

  box_exc2_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc2_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_check_1",box_exc2_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_check_1,30,20);
  gtk_widget_show(box_exc2_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc2_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)10);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G2");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc2),label,TRUE,TRUE,0);

  box_exc2_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_8",box_exc2_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_8,"0");
  gtk_widget_show(box_exc2_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_8,TRUE,TRUE,0);

  box_exc2_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_9",box_exc2_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_9,"0");
  gtk_widget_show(box_exc2_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_9,TRUE,TRUE,0);

  box_exc2_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_10",box_exc2_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_10,"0");
  gtk_widget_show(box_exc2_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_10,TRUE,TRUE,0);

  box_exc2_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc2_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_text_11",box_exc2_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc2_text_11,"0");
  gtk_widget_show(box_exc2_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_text_11,TRUE,TRUE,0);

  box_exc2_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc2_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc2_check_2",box_exc2_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc2_check_2,30,20);
  gtk_widget_show(box_exc2_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc2_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)11);
  gtk_box_pack_start(GTK_BOX(box_exc2),box_exc2_check_2,TRUE,TRUE,0);


  box_exc3 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3",box_exc3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc3);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc3,FALSE,FALSE,0);

  exc3_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc3_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc3_swp_toggle",exc3_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc3_swp_toggle,40,20);
  gtk_widget_show(exc3_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc3_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)3);
  gtk_box_pack_start(GTK_BOX(box_exc3),exc3_swp_toggle,TRUE,TRUE,0);

  box_exc3_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc3_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc3_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc3_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc3_model),0);
  gtk_widget_show(box_exc3_model);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_model,TRUE,TRUE,0);

  label = gtk_label_new("P3");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc3),label,TRUE,TRUE,0);

  box_exc3_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_0",box_exc3_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_0,"0");
  gtk_widget_show(box_exc3_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_0,TRUE,TRUE,0);

  box_exc3_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_1",box_exc3_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_1,"0");
  gtk_widget_show(box_exc3_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_1,TRUE,TRUE,0);

  box_exc3_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_2",box_exc3_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_2,"0");
  gtk_widget_show(box_exc3_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_2,TRUE,TRUE,0);

  box_exc3_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_3",box_exc3_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_3,"0");
  gtk_widget_show(box_exc3_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_3,TRUE,TRUE,0);

  box_exc3_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc3_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_check_0",box_exc3_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_check_0,30,20);
  gtk_widget_show(box_exc3_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc3_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)12);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_check_0,TRUE,TRUE,0);

  label = gtk_label_new("03");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc3),label,TRUE,TRUE,0);

  box_exc3_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_4",box_exc3_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_4,"0");
  gtk_widget_show(box_exc3_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_4,TRUE,TRUE,0);

  box_exc3_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_5",box_exc3_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_5,"0");
  gtk_widget_show(box_exc3_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_5,TRUE,TRUE,0);

  box_exc3_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_6",box_exc3_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_6,"0");
  gtk_widget_show(box_exc3_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_6,TRUE,TRUE,0);

  box_exc3_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_7",box_exc3_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_7,"0");
  gtk_widget_show(box_exc3_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_7,TRUE,TRUE,0);

  box_exc3_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc3_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_check_1",box_exc3_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_check_1,30,20);
  gtk_widget_show(box_exc3_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc3_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)13);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G3");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc3),label,TRUE,TRUE,0);

  box_exc3_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_8",box_exc3_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_8,"0");
  gtk_widget_show(box_exc3_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_8,TRUE,TRUE,0);

  box_exc3_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_9",box_exc3_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_9,"0");
  gtk_widget_show(box_exc3_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_9,TRUE,TRUE,0);

  box_exc3_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_10",box_exc3_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_10,"0");
  gtk_widget_show(box_exc3_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_10,TRUE,TRUE,0);

  box_exc3_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc3_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_text_11",box_exc3_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc3_text_11,"0");
  gtk_widget_show(box_exc3_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_text_11,TRUE,TRUE,0);

  box_exc3_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc3_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc3_check_2",box_exc3_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc3_check_2,30,20);
  gtk_widget_show(box_exc3_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc3_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)14);
  gtk_box_pack_start(GTK_BOX(box_exc3),box_exc3_check_2,TRUE,TRUE,0);


  box_exc4 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4",box_exc4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc4);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc4,FALSE,FALSE,0);

  exc4_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc4_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc4_swp_toggle",exc4_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc4_swp_toggle,40,20);
  gtk_widget_show(exc4_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc4_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)4);
  gtk_box_pack_start(GTK_BOX(box_exc4),exc4_swp_toggle,TRUE,TRUE,0);

  box_exc4_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc4_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc4_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc4_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc4_model),0);
  gtk_widget_show(box_exc4_model);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_model,TRUE,TRUE,0);

  label = gtk_label_new("P4");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc4),label,TRUE,TRUE,0);

  box_exc4_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_0",box_exc4_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_0,"0");
  gtk_widget_show(box_exc4_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_0,TRUE,TRUE,0);

  box_exc4_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_1",box_exc4_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_1,"0");
  gtk_widget_show(box_exc4_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_1,TRUE,TRUE,0);

  box_exc4_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_2",box_exc4_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_2,"0");
  gtk_widget_show(box_exc4_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_2,TRUE,TRUE,0);

  box_exc4_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_3",box_exc4_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_3,"0");
  gtk_widget_show(box_exc4_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_3,TRUE,TRUE,0);

  box_exc4_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc4_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_check_0",box_exc4_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_check_0,30,20);
  gtk_widget_show(box_exc4_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc4_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)15);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_check_0,TRUE,TRUE,0);

  label = gtk_label_new("04");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc4),label,TRUE,TRUE,0);

  box_exc4_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_4",box_exc4_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_4,"0");
  gtk_widget_show(box_exc4_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_4,TRUE,TRUE,0);

  box_exc4_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_5",box_exc4_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_5,"0");
  gtk_widget_show(box_exc4_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_5,TRUE,TRUE,0);

  box_exc4_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_6",box_exc4_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_6,"0");
  gtk_widget_show(box_exc4_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_6,TRUE,TRUE,0);

  box_exc4_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_7",box_exc4_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_7,"0");
  gtk_widget_show(box_exc4_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_7,TRUE,TRUE,0);

  box_exc4_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc4_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_check_1",box_exc4_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_check_1,30,20);
  gtk_widget_show(box_exc4_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc4_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)16);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G4");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc4),label,TRUE,TRUE,0);

  box_exc4_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_8",box_exc4_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_8,"0");
  gtk_widget_show(box_exc4_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_8,TRUE,TRUE,0);

  box_exc4_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_9",box_exc4_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_9,"0");
  gtk_widget_show(box_exc4_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_9,TRUE,TRUE,0);

  box_exc4_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_10",box_exc4_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_10,"0");
  gtk_widget_show(box_exc4_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_10,TRUE,TRUE,0);

  box_exc4_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc4_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_text_11",box_exc4_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc4_text_11,"0");
  gtk_widget_show(box_exc4_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_text_11,TRUE,TRUE,0);

  box_exc4_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc4_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc4_check_2",box_exc4_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc4_check_2,30,20);
  gtk_widget_show(box_exc4_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc4_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)17);
  gtk_box_pack_start(GTK_BOX(box_exc4),box_exc4_check_2,TRUE,TRUE,0);


  box_exc5 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5",box_exc5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc5);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc5,FALSE,FALSE,0);

  exc5_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc5_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc5_swp_toggle",exc5_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc5_swp_toggle,40,20);
  gtk_widget_show(exc5_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc5_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)5);
  gtk_box_pack_start(GTK_BOX(box_exc5),exc5_swp_toggle,TRUE,TRUE,0);

  box_exc5_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc5_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc5_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc5_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc5_model),0);
  gtk_widget_show(box_exc5_model);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_model,TRUE,TRUE,0);

  label = gtk_label_new("P5");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc5),label,TRUE,TRUE,0);

  box_exc5_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_0",box_exc5_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_0,"0");
  gtk_widget_show(box_exc5_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_0,TRUE,TRUE,0);

  box_exc5_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_1",box_exc5_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_1,"0");
  gtk_widget_show(box_exc5_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_1,TRUE,TRUE,0);

  box_exc5_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_2",box_exc5_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_2,"0");
  gtk_widget_show(box_exc5_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_2,TRUE,TRUE,0);

  box_exc5_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_3",box_exc5_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_3,"0");
  gtk_widget_show(box_exc5_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_3,TRUE,TRUE,0);

  box_exc5_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc5_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_check_0",box_exc5_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_check_0,30,20);
  gtk_widget_show(box_exc5_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc5_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)18);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_check_0,TRUE,TRUE,0);

  label = gtk_label_new("05");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc5),label,TRUE,TRUE,0);

  box_exc5_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_4",box_exc5_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_4,"0");
  gtk_widget_show(box_exc5_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_4,TRUE,TRUE,0);

  box_exc5_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_5",box_exc5_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_5,"0");
  gtk_widget_show(box_exc5_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_5,TRUE,TRUE,0);

  box_exc5_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_6",box_exc5_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_6,"0");
  gtk_widget_show(box_exc5_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_6,TRUE,TRUE,0);

  box_exc5_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_7",box_exc5_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_7,"0");
  gtk_widget_show(box_exc5_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_7,TRUE,TRUE,0);

  box_exc5_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc5_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_check_1",box_exc5_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_check_1,30,20);
  gtk_widget_show(box_exc5_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc5_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)19);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G5");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc5),label,TRUE,TRUE,0);

  box_exc5_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_8",box_exc5_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_8,"0");
  gtk_widget_show(box_exc5_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_8,TRUE,TRUE,0);

  box_exc5_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_9",box_exc5_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_9,"0");
  gtk_widget_show(box_exc5_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_9,TRUE,TRUE,0);

  box_exc5_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_10",box_exc5_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_10,"0");
  gtk_widget_show(box_exc5_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_10,TRUE,TRUE,0);

  box_exc5_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc5_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_text_11",box_exc5_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc5_text_11,"0");
  gtk_widget_show(box_exc5_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_text_11,TRUE,TRUE,0);

  box_exc5_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc5_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc5_check_2",box_exc5_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc5_check_2,30,20);
  gtk_widget_show(box_exc5_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc5_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)20);
  gtk_box_pack_start(GTK_BOX(box_exc5),box_exc5_check_2,TRUE,TRUE,0);


  box_exc6 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6",box_exc6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc6);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc6,FALSE,FALSE,0);

  exc6_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc6_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc6_swp_toggle",exc6_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc6_swp_toggle,40,20);
  gtk_widget_show(exc6_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc6_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)6);
  gtk_box_pack_start(GTK_BOX(box_exc6),exc6_swp_toggle,TRUE,TRUE,0);

  box_exc6_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc6_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc6_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc6_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc6_model),0);
  gtk_widget_show(box_exc6_model);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_model,TRUE,TRUE,0);

  label = gtk_label_new("P6");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc6),label,TRUE,TRUE,0);

  box_exc6_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_0",box_exc6_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_0,"0");
  gtk_widget_show(box_exc6_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_0,TRUE,TRUE,0);

  box_exc6_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_1",box_exc6_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_1,"0");
  gtk_widget_show(box_exc6_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_1,TRUE,TRUE,0);

  box_exc6_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_2",box_exc6_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_2,"0");
  gtk_widget_show(box_exc6_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_2,TRUE,TRUE,0);

  box_exc6_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_3",box_exc6_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_3,"0");
  gtk_widget_show(box_exc6_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_3,TRUE,TRUE,0);

  box_exc6_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc6_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_check_0",box_exc6_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_check_0,30,20);
  gtk_widget_show(box_exc6_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc6_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)21);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_check_0,TRUE,TRUE,0);

  label = gtk_label_new("06");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc6),label,TRUE,TRUE,0);

  box_exc6_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_4",box_exc6_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_4,"0");
  gtk_widget_show(box_exc6_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_4,TRUE,TRUE,0);

  box_exc6_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_5",box_exc6_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_5,"0");
  gtk_widget_show(box_exc6_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_5,TRUE,TRUE,0);

  box_exc6_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_6",box_exc6_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_6,"0");
  gtk_widget_show(box_exc6_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_6,TRUE,TRUE,0);

  box_exc6_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_7",box_exc6_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_7,"0");
  gtk_widget_show(box_exc6_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_7,TRUE,TRUE,0);

  box_exc6_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc6_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_check_1",box_exc6_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_check_1,30,20);
  gtk_widget_show(box_exc6_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc6_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)22);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G6");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc6),label,TRUE,TRUE,0);

  box_exc6_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_8",box_exc6_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_8,"0");
  gtk_widget_show(box_exc6_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_8,TRUE,TRUE,0);

  box_exc6_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_9",box_exc6_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_9,"0");
  gtk_widget_show(box_exc6_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_9,TRUE,TRUE,0);

  box_exc6_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_10",box_exc6_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_10,"0");
  gtk_widget_show(box_exc6_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_10,TRUE,TRUE,0);

  box_exc6_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc6_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_text_11",box_exc6_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc6_text_11,"0");
  gtk_widget_show(box_exc6_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_text_11,TRUE,TRUE,0);

  box_exc6_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc6_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc6_check_2",box_exc6_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc6_check_2,30,20);
  gtk_widget_show(box_exc6_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc6_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)23);
  gtk_box_pack_start(GTK_BOX(box_exc6),box_exc6_check_2,TRUE,TRUE,0);


  box_exc7 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7",box_exc7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc7);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc7,FALSE,FALSE,0);

  exc7_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc7_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc7_swp_toggle",exc7_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc7_swp_toggle,40,20);
  gtk_widget_show(exc7_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc7_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)7);
  gtk_box_pack_start(GTK_BOX(box_exc7),exc7_swp_toggle,TRUE,TRUE,0);

  box_exc7_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc7_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc7_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc7_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc7_model),0);
  gtk_widget_show(box_exc7_model);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_model,TRUE,TRUE,0);

  label = gtk_label_new("P7");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc7),label,TRUE,TRUE,0);

  box_exc7_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_0",box_exc7_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_0,"0");
  gtk_widget_show(box_exc7_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_0,TRUE,TRUE,0);

  box_exc7_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_1",box_exc7_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_1,"0");
  gtk_widget_show(box_exc7_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_1,TRUE,TRUE,0);

  box_exc7_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_2",box_exc7_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_2,"0");
  gtk_widget_show(box_exc7_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_2,TRUE,TRUE,0);

  box_exc7_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_3",box_exc7_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_3,"0");
  gtk_widget_show(box_exc7_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_3,TRUE,TRUE,0);

  box_exc7_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc7_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_check_0",box_exc7_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_check_0,30,20);
  gtk_widget_show(box_exc7_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc7_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)24);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_check_0,TRUE,TRUE,0);

  label = gtk_label_new("07");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc7),label,TRUE,TRUE,0);

  box_exc7_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_4",box_exc7_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_4,"0");
  gtk_widget_show(box_exc7_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_4,TRUE,TRUE,0);

  box_exc7_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_5",box_exc7_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_5,"0");
  gtk_widget_show(box_exc7_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_5,TRUE,TRUE,0);

  box_exc7_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_6",box_exc7_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_6,"0");
  gtk_widget_show(box_exc7_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_6,TRUE,TRUE,0);

  box_exc7_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_7",box_exc7_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_7,"0");
  gtk_widget_show(box_exc7_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_7,TRUE,TRUE,0);

  box_exc7_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc7_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_check_1",box_exc7_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_check_1,30,20);
  gtk_widget_show(box_exc7_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc7_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)25);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G7");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc7),label,TRUE,TRUE,0);

  box_exc7_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_8",box_exc7_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_8,"0");
  gtk_widget_show(box_exc7_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_8,TRUE,TRUE,0);

  box_exc7_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_9",box_exc7_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_9,"0");
  gtk_widget_show(box_exc7_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_9,TRUE,TRUE,0);

  box_exc7_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_10",box_exc7_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_10,"0");
  gtk_widget_show(box_exc7_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_10,TRUE,TRUE,0);

  box_exc7_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc7_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_text_11",box_exc7_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc7_text_11,"0");
  gtk_widget_show(box_exc7_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_text_11,TRUE,TRUE,0);

  box_exc7_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc7_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc7_check_2",box_exc7_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc7_check_2,30,20);
  gtk_widget_show(box_exc7_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc7_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)26);
  gtk_box_pack_start(GTK_BOX(box_exc7),box_exc7_check_2,TRUE,TRUE,0);


  box_exc8 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8",box_exc8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc8);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc8,FALSE,FALSE,0);

  exc8_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc8_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc8_swp_toggle",exc8_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc8_swp_toggle,40,20);
  gtk_widget_show(exc8_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc8_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)8);
  gtk_box_pack_start(GTK_BOX(box_exc8),exc8_swp_toggle,TRUE,TRUE,0);

  box_exc8_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc8_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc8_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc8_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc8_model),0);
  gtk_widget_show(box_exc8_model);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_model,TRUE,TRUE,0);

  label = gtk_label_new("P8");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc8),label,TRUE,TRUE,0);

  box_exc8_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_0",box_exc8_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_0,"0");
  gtk_widget_show(box_exc8_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_0,TRUE,TRUE,0);

  box_exc8_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_1",box_exc8_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_1,"0");
  gtk_widget_show(box_exc8_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_1,TRUE,TRUE,0);

  box_exc8_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_2",box_exc8_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_2,"0");
  gtk_widget_show(box_exc8_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_2,TRUE,TRUE,0);

  box_exc8_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_3",box_exc8_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_3,"0");
  gtk_widget_show(box_exc8_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_3,TRUE,TRUE,0);

  box_exc8_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc8_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_check_0",box_exc8_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_check_0,30,20);
  gtk_widget_show(box_exc8_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc8_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)27);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_check_0,TRUE,TRUE,0);

  label = gtk_label_new("08");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc8),label,TRUE,TRUE,0);

  box_exc8_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_4",box_exc8_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_4,"0");
  gtk_widget_show(box_exc8_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_4,TRUE,TRUE,0);

  box_exc8_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_5",box_exc8_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_5,"0");
  gtk_widget_show(box_exc8_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_5,TRUE,TRUE,0);

  box_exc8_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_6",box_exc8_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_6,"0");
  gtk_widget_show(box_exc8_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_6,TRUE,TRUE,0);

  box_exc8_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_7",box_exc8_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_7,"0");
  gtk_widget_show(box_exc8_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_7,TRUE,TRUE,0);

  box_exc8_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc8_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_check_1",box_exc8_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_check_1,30,20);
  gtk_widget_show(box_exc8_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc8_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)28);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G8");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc8),label,TRUE,TRUE,0);

  box_exc8_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_8",box_exc8_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_8,"0");
  gtk_widget_show(box_exc8_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_8,TRUE,TRUE,0);

  box_exc8_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_9",box_exc8_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_9,"0");
  gtk_widget_show(box_exc8_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_9,TRUE,TRUE,0);

  box_exc8_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_10",box_exc8_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_10,"0");
  gtk_widget_show(box_exc8_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_10,TRUE,TRUE,0);

  box_exc8_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc8_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_text_11",box_exc8_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc8_text_11,"0");
  gtk_widget_show(box_exc8_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_text_11,TRUE,TRUE,0);

  box_exc8_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc8_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc8_check_2",box_exc8_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc8_check_2,30,20);
  gtk_widget_show(box_exc8_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc8_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)29);
  gtk_box_pack_start(GTK_BOX(box_exc8),box_exc8_check_2,TRUE,TRUE,0);


  box_exc9 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9",box_exc9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc9);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc9,FALSE,FALSE,0);

  exc9_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc9_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc9_swp_toggle",exc9_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc9_swp_toggle,40,20);
  gtk_widget_show(exc9_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc9_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)9);
  gtk_box_pack_start(GTK_BOX(box_exc9),exc9_swp_toggle,TRUE,TRUE,0);

  box_exc9_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc9_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc9_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc9_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc9_model),0);
  gtk_widget_show(box_exc9_model);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_model,TRUE,TRUE,0);

  label = gtk_label_new("P9");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc9),label,TRUE,TRUE,0);

  box_exc9_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_0",box_exc9_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_0,"0");
  gtk_widget_show(box_exc9_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_0,TRUE,TRUE,0);

  box_exc9_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_1",box_exc9_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_1,"0");
  gtk_widget_show(box_exc9_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_1,TRUE,TRUE,0);

  box_exc9_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_2",box_exc9_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_2,"0");
  gtk_widget_show(box_exc9_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_2,TRUE,TRUE,0);

  box_exc9_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_3",box_exc9_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_3,"0");
  gtk_widget_show(box_exc9_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_3,TRUE,TRUE,0);

  box_exc9_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc9_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_check_0",box_exc9_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_check_0,30,20);
  gtk_widget_show(box_exc9_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc9_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)30);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_check_0,TRUE,TRUE,0);

  label = gtk_label_new("09");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc9),label,TRUE,TRUE,0);

  box_exc9_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_4",box_exc9_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_4,"0");
  gtk_widget_show(box_exc9_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_4,TRUE,TRUE,0);

  box_exc9_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_5",box_exc9_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_5,"0");
  gtk_widget_show(box_exc9_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_5,TRUE,TRUE,0);

  box_exc9_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_6",box_exc9_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_6,"0");
  gtk_widget_show(box_exc9_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_6,TRUE,TRUE,0);

  box_exc9_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_7",box_exc9_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_7,"0");
  gtk_widget_show(box_exc9_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_7,TRUE,TRUE,0);

  box_exc9_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc9_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_check_1",box_exc9_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_check_1,30,20);
  gtk_widget_show(box_exc9_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc9_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)31);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G9");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc9),label,TRUE,TRUE,0);

  box_exc9_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_8",box_exc9_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_8,"0");
  gtk_widget_show(box_exc9_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_8,TRUE,TRUE,0);

  box_exc9_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_9",box_exc9_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_9,"0");
  gtk_widget_show(box_exc9_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_9,TRUE,TRUE,0);

  box_exc9_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_10",box_exc9_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_10,"0");
  gtk_widget_show(box_exc9_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_10,TRUE,TRUE,0);

  box_exc9_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc9_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_text_11",box_exc9_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc9_text_11,"0");
  gtk_widget_show(box_exc9_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_text_11,TRUE,TRUE,0);

  box_exc9_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc9_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc9_check_2",box_exc9_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc9_check_2,30,20);
  gtk_widget_show(box_exc9_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc9_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)32);
  gtk_box_pack_start(GTK_BOX(box_exc9),box_exc9_check_2,TRUE,TRUE,0);


  box_exc10 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10",box_exc10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc10);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc10,FALSE,FALSE,0);

  exc10_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc10_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc10_swp_toggle",exc10_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc10_swp_toggle,40,20);
  gtk_widget_show(exc10_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc10_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)10);
  gtk_box_pack_start(GTK_BOX(box_exc10),exc10_swp_toggle,TRUE,TRUE,0);

  box_exc10_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc10_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc10_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc10_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc10_model),0);
  gtk_widget_show(box_exc10_model);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_model,TRUE,TRUE,0);

  label = gtk_label_new("P10");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc10),label,TRUE,TRUE,0);

  box_exc10_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_0",box_exc10_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_0,"0");
  gtk_widget_show(box_exc10_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_0,TRUE,TRUE,0);

  box_exc10_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_1",box_exc10_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_1,"0");
  gtk_widget_show(box_exc10_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_1,TRUE,TRUE,0);

  box_exc10_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_2",box_exc10_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_2,"0");
  gtk_widget_show(box_exc10_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_2,TRUE,TRUE,0);

  box_exc10_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_3",box_exc10_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_3,"0");
  gtk_widget_show(box_exc10_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_3,TRUE,TRUE,0);

  box_exc10_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc10_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_check_0",box_exc10_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_check_0,30,20);
  gtk_widget_show(box_exc10_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc10_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)33);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_check_0,TRUE,TRUE,0);

  label = gtk_label_new("010");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc10),label,TRUE,TRUE,0);

  box_exc10_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_4",box_exc10_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_4,"0");
  gtk_widget_show(box_exc10_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_4,TRUE,TRUE,0);

  box_exc10_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_5",box_exc10_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_5,"0");
  gtk_widget_show(box_exc10_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_5,TRUE,TRUE,0);

  box_exc10_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_6",box_exc10_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_6,"0");
  gtk_widget_show(box_exc10_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_6,TRUE,TRUE,0);

  box_exc10_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_7",box_exc10_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_7,"0");
  gtk_widget_show(box_exc10_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_7,TRUE,TRUE,0);

  box_exc10_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc10_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_check_1",box_exc10_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_check_1,30,20);
  gtk_widget_show(box_exc10_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc10_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)34);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G10");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc10),label,TRUE,TRUE,0);

  box_exc10_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_8",box_exc10_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_8,"0");
  gtk_widget_show(box_exc10_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_8,TRUE,TRUE,0);

  box_exc10_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_9",box_exc10_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_9,"0");
  gtk_widget_show(box_exc10_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_9,TRUE,TRUE,0);

  box_exc10_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_10",box_exc10_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_10,"0");
  gtk_widget_show(box_exc10_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_10,TRUE,TRUE,0);

  box_exc10_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc10_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_text_11",box_exc10_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc10_text_11,"0");
  gtk_widget_show(box_exc10_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_text_11,TRUE,TRUE,0);

  box_exc10_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc10_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc10_check_2",box_exc10_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc10_check_2,30,20);
  gtk_widget_show(box_exc10_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc10_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)35);
  gtk_box_pack_start(GTK_BOX(box_exc10),box_exc10_check_2,TRUE,TRUE,0);


  box_exc11 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11",box_exc11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc11);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc11,FALSE,FALSE,0);

  exc11_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc11_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc11_swp_toggle",exc11_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc11_swp_toggle,40,20);
  gtk_widget_show(exc11_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc11_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)11);
  gtk_box_pack_start(GTK_BOX(box_exc11),exc11_swp_toggle,TRUE,TRUE,0);

  box_exc11_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc11_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc11_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc11_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc11_model),0);
  gtk_widget_show(box_exc11_model);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_model,TRUE,TRUE,0);

  label = gtk_label_new("P11");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc11),label,TRUE,TRUE,0);

  box_exc11_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_0",box_exc11_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_0,"0");
  gtk_widget_show(box_exc11_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_0,TRUE,TRUE,0);

  box_exc11_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_1",box_exc11_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_1,"0");
  gtk_widget_show(box_exc11_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_1,TRUE,TRUE,0);

  box_exc11_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_2",box_exc11_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_2,"0");
  gtk_widget_show(box_exc11_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_2,TRUE,TRUE,0);

  box_exc11_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_3",box_exc11_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_3,"0");
  gtk_widget_show(box_exc11_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_3,TRUE,TRUE,0);

  box_exc11_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc11_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_check_0",box_exc11_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_check_0,30,20);
  gtk_widget_show(box_exc11_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc11_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)36);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_check_0,TRUE,TRUE,0);

  label = gtk_label_new("011");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc11),label,TRUE,TRUE,0);

  box_exc11_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_4",box_exc11_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_4,"0");
  gtk_widget_show(box_exc11_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_4,TRUE,TRUE,0);

  box_exc11_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_5",box_exc11_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_5,"0");
  gtk_widget_show(box_exc11_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_5,TRUE,TRUE,0);

  box_exc11_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_6",box_exc11_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_6,"0");
  gtk_widget_show(box_exc11_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_6,TRUE,TRUE,0);

  box_exc11_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_7",box_exc11_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_7,"0");
  gtk_widget_show(box_exc11_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_7,TRUE,TRUE,0);

  box_exc11_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc11_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_check_1",box_exc11_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_check_1,30,20);
  gtk_widget_show(box_exc11_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc11_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)37);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G11");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc11),label,TRUE,TRUE,0);

  box_exc11_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_8",box_exc11_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_8,"0");
  gtk_widget_show(box_exc11_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_8,TRUE,TRUE,0);

  box_exc11_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_9",box_exc11_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_9,"0");
  gtk_widget_show(box_exc11_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_9,TRUE,TRUE,0);

  box_exc11_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_10",box_exc11_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_10,"0");
  gtk_widget_show(box_exc11_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_10,TRUE,TRUE,0);

  box_exc11_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc11_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_text_11",box_exc11_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc11_text_11,"0");
  gtk_widget_show(box_exc11_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_text_11,TRUE,TRUE,0);

  box_exc11_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc11_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc11_check_2",box_exc11_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc11_check_2,30,20);
  gtk_widget_show(box_exc11_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc11_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)38);
  gtk_box_pack_start(GTK_BOX(box_exc11),box_exc11_check_2,TRUE,TRUE,0);


  box_exc12 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_exc12);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12",box_exc12,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_exc12);
  gtk_box_pack_start(GTK_BOX(vbox_excit),box_exc12,FALSE,FALSE,0);

  exc12_swp_toggle = gtk_check_button_new_with_label("SWP");
  gtk_widget_ref(exc12_swp_toggle);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"exc12_swp_toggle",exc12_swp_toggle,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(exc12_swp_toggle,40,20);
  gtk_widget_show(exc12_swp_toggle);
  gtk_signal_connect (GTK_OBJECT(exc12_swp_toggle),"toggled",
                        (GtkSignalFunc)swap_callback,(gpointer)12);
  gtk_box_pack_start(GTK_BOX(box_exc12),exc12_swp_toggle,TRUE,TRUE,0);

  box_exc12_model = gtk_option_menu_new();
  gtk_widget_ref(box_exc12_model);
  menu = gtk_menu_new();
  init_model_menu(menu,exc12_model_menu);
  gtk_widget_show(menu_item);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(box_exc12_model),menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(box_exc12_model),0);
  gtk_widget_show(box_exc12_model);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_model,TRUE,TRUE,0);

  label = gtk_label_new("P12");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc12),label,TRUE,TRUE,0);

  box_exc12_text_0 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_0",box_exc12_text_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_0,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_0,"0");
  gtk_widget_show(box_exc12_text_0);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_0,TRUE,TRUE,0);

  box_exc12_text_1 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_1",box_exc12_text_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_1,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_1,"0");
  gtk_widget_show(box_exc12_text_1);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_1,TRUE,TRUE,0);

  box_exc12_text_2 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_2",box_exc12_text_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_2,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_2,"0");
  gtk_widget_show(box_exc12_text_2);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_2,TRUE,TRUE,0);

  box_exc12_text_3 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_3);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_3",box_exc12_text_3,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_3,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_3,"0");
  gtk_widget_show(box_exc12_text_3);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_3,TRUE,TRUE,0);

  box_exc12_check_0 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc12_check_0);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_check_0",box_exc12_check_0,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_check_0,30,20);
  gtk_widget_show(box_exc12_check_0);
  gtk_signal_connect (GTK_OBJECT(box_exc12_check_0),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)39);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_check_0,TRUE,TRUE,0);

  label = gtk_label_new("012");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc12),label,TRUE,TRUE,0);

  box_exc12_text_4 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_4);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_4",box_exc12_text_4,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_4,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_4,"0");
  gtk_widget_show(box_exc12_text_4);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_4,TRUE,TRUE,0);

  box_exc12_text_5 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_5);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_5",box_exc12_text_5,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_5,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_5,"0");
  gtk_widget_show(box_exc12_text_5);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_5,TRUE,TRUE,0);

  box_exc12_text_6 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_6);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_6",box_exc12_text_6,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_6,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_6,"0");
  gtk_widget_show(box_exc12_text_6);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_6,TRUE,TRUE,0);

  box_exc12_text_7 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_7);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_7",box_exc12_text_7,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_7,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_7,"0");
  gtk_widget_show(box_exc12_text_7);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_7,TRUE,TRUE,0);

  box_exc12_check_1 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc12_check_1);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_check_1",box_exc12_check_1,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_check_1,30,20);
  gtk_widget_show(box_exc12_check_1);
  gtk_signal_connect (GTK_OBJECT(box_exc12_check_1),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)40);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_check_1,TRUE,TRUE,0);

  label = gtk_label_new("G12");
  gtk_widget_set_usize(label,30,20);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(box_exc12),label,TRUE,TRUE,0);

  box_exc12_text_8 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_8);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_8",box_exc12_text_8,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_8,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_8,"0");
  gtk_widget_show(box_exc12_text_8);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_8,TRUE,TRUE,0);

  box_exc12_text_9 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_9);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_9",box_exc12_text_9,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_9,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_9,"0");
  gtk_widget_show(box_exc12_text_9);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_9,TRUE,TRUE,0);

  box_exc12_text_10 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_10);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_10",box_exc12_text_10,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_10,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_10,"0");
  gtk_widget_show(box_exc12_text_10);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_10,TRUE,TRUE,0);

  box_exc12_text_11 = gtk_entry_new();
  gtk_widget_ref(box_exc12_text_11);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_text_11",box_exc12_text_11,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_text_11,30,20);
  gtk_entry_set_text((GtkEntry *)box_exc12_text_11,"0");
  gtk_widget_show(box_exc12_text_11);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_text_11,TRUE,TRUE,0);

  box_exc12_check_2 = gtk_check_button_new_with_label("FIX");
  gtk_widget_ref(box_exc12_check_2);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_exc12_check_2",box_exc12_check_2,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_set_usize(box_exc12_check_2,30,20);
  gtk_widget_show(box_exc12_check_2);
  gtk_signal_connect (GTK_OBJECT(box_exc12_check_2),"toggled",
                        (GtkSignalFunc)fix_callback,(gpointer)41);
  gtk_box_pack_start(GTK_BOX(box_exc12),box_exc12_check_2,TRUE,TRUE,0);

  box_bottom_buttons = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(box_bottom_buttons);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"box_bottom_buttons",box_bottom_buttons,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(box_bottom_buttons);
  gtk_box_pack_start(GTK_BOX(vbox1),box_bottom_buttons,FALSE,FALSE,0);

  preview_button=gtk_button_new_with_label("Preview");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"preview_button",preview_button,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(preview_button);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),preview_button,TRUE,TRUE,0);

  fit_button=gtk_button_new_with_label("Fit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"fit_button",fit_button,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(fit_button);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),fit_button,TRUE,TRUE,0);

  plot_button=gtk_button_new_with_label("Plot fit");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"plot_button",plot_button,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(plot_button);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),plot_button,TRUE,TRUE,0);

  print_button=gtk_button_new_with_label("Print");
  gtk_object_set_data_full(GTK_OBJECT(main_window),"print_button",print_button,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(print_button);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),print_button,TRUE,TRUE,0);

  log_fix = gtk_check_button_new_with_label("Y-LOG");
  gtk_widget_ref(log_fix);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"log_fix",log_fix,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(log_fix);
  gtk_signal_connect (GTK_OBJECT(log_fix),"toggled",
                        (GtkSignalFunc)log_fix_callback,(gpointer)1);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),log_fix,FALSE,FALSE,0);

  live_chk = gtk_check_button_new_with_label("Live");
  gtk_widget_ref(live_chk);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"live_chk",live_chk,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_show(live_chk);
  gtk_signal_connect (GTK_OBJECT(live_chk),"toggled",
                        (GtkSignalFunc)live_chk_callback,(gpointer)1);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),live_chk,FALSE,FALSE,0);

  live_adj = gtk_adjustment_new(1,0,50,0.1,1,0);
  live_spin = gtk_spin_button_new(GTK_ADJUSTMENT(live_adj),0.05,2);
  gtk_widget_ref(live_spin);
  gtk_widget_show(live_spin);
  gtk_box_pack_start(GTK_BOX(box_bottom_buttons),live_spin,TRUE,TRUE,0);

  main_info_box = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref(main_info_box);
  gtk_object_set_data_full(GTK_OBJECT(main_window),"main_info_box",main_info_box,(GtkDestroyNotify)gtk_widget_unref);
  gtk_widget_hide(main_info_box);
  gtk_box_pack_start(GTK_BOX(vbox1),main_info_box,FALSE,FALSE,0);

  info_text=gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(info_text),FALSE);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(info_text),FALSE);
  gtk_widget_show(info_text);
  gtk_box_pack_start(GTK_BOX(main_info_box),info_text,TRUE,TRUE,0);


temp = 294;
frac = 5;
fit_types_loaded = 1;
strcpy(counts,"100");
init_excitations_boxes();
show_excitations_boxes(1);
refresh_displayed_data();
check_info_id = g_timeout_add(5*1000,(GtkFunction)check_info_box,NULL);


  /* Gtk signal connections */
  gtk_signal_connect (GTK_OBJECT (quit), "activate",
                      GTK_SIGNAL_FUNC (on_quit_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (about), "activate",
                      GTK_SIGNAL_FUNC (on_about_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (load), "activate",
                      GTK_SIGNAL_FUNC (on_load_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (save), "activate",
                      GTK_SIGNAL_FUNC (on_save_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (save_as), "activate",
                      GTK_SIGNAL_FUNC (on_save_as_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (undo), "activate",
                      GTK_SIGNAL_FUNC (on_undo_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (preferences), "activate",
                      GTK_SIGNAL_FUNC (create_options_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (example), "activate",
                      GTK_SIGNAL_FUNC (on_example_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (report_bug), "activate",
                      GTK_SIGNAL_FUNC (on_report_bug_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (manual), "activate",
                      GTK_SIGNAL_FUNC (create_manual_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (import), "activate",
                      GTK_SIGNAL_FUNC (create_import_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (print_ana_state), "activate",
                      GTK_SIGNAL_FUNC (create_analysers_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (wavevector), "activate",
                      GTK_SIGNAL_FUNC (create_wavevector_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (kill_fit), "activate",
                      GTK_SIGNAL_FUNC (on_kill_fit),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (refresh), "activate",
                      GTK_SIGNAL_FUNC (refresh_displayed_data),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (print_to_file), "activate",
                      GTK_SIGNAL_FUNC (print_file_do_file),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (box_data2_adj_0), "value_changed",
                      GTK_SIGNAL_FUNC (on_box_data2_adj_0_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (inp_button), "clicked",
                      GTK_SIGNAL_FUNC (on_load_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (pv_load_button), "clicked",
                      GTK_SIGNAL_FUNC (on_pv_load_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (preview_button), "clicked",
                      GTK_SIGNAL_FUNC (on_preview_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (fit_button), "clicked",
                      GTK_SIGNAL_FUNC (on_fit_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (plot_button), "clicked",
                      GTK_SIGNAL_FUNC (on_plot_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (print_button), "clicked",
                      GTK_SIGNAL_FUNC (print_do_file),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (box_files1_button_0), "clicked",
                      GTK_SIGNAL_FUNC (edit_text_file),
                      box_files1_text_0);
  gtk_signal_connect (GTK_OBJECT (box_files1_button_1), "clicked",
                      GTK_SIGNAL_FUNC (edit_text_file),
                      box_files1_text_1);
  gtk_signal_connect (GTK_OBJECT (box_res_file_button), "clicked",
                      GTK_SIGNAL_FUNC (create_resol_window),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (box_files2_button_1), "clicked",
                      GTK_SIGNAL_FUNC (edit_text_file),
                      box_files2_text_1);
  gtk_signal_connect (GTK_OBJECT (box_files2_button_2), "clicked",
                      GTK_SIGNAL_FUNC (edit_text_file),
                      box_files2_text_2);
  gtk_signal_connect (GTK_TOGGLE_BUTTON (convol_radio0), "toggled",
                      GTK_SIGNAL_FUNC (convol_choices_callback),0);
  gtk_signal_connect (GTK_TOGGLE_BUTTON (convol_radio1), "toggled",
                      GTK_SIGNAL_FUNC (convol_choices_callback),1);
  gtk_signal_connect (GTK_TOGGLE_BUTTON (convol_radio2), "toggled",
                      GTK_SIGNAL_FUNC (convol_choices_callback),2);
  gtk_signal_connect (GTK_TOGGLE_BUTTON (convol_radio3), "toggled",
                      GTK_SIGNAL_FUNC (convol_choices_callback),3);

  return main_window;
}

