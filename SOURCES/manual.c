/*
 * options.c - A. BERAUD  (October 2003)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "manual.h"
#include "interface.h"

GtkWidget *man_win,*man_hbox,*man_vbox,*man_button,*man_scr1,*man_scr2,*man_text;
GtkWidget *man_tree[10],*man_tree_item;


/* Self explanatory. */
void create_manual_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	GtkWidget *pTreeView;
	GtkTreeStore *pTreeStore;
	GtkTreeViewColumn *pColumn;
	GtkCellRenderer *pCellRenderer;
 	GtkTreeIter pIter,pIter2,pIter3,pIter4;

	man_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(man_win),"Fit28 manual");
	gtk_signal_connect(GTK_OBJECT(man_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(man_win));
	gtk_signal_connect(GTK_OBJECT(man_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(man_win));

	man_hbox=gtk_hbox_new(FALSE,0);
	gtk_container_add(GTK_CONTAINER(man_win),man_hbox);

	man_scr1=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(man_scr1),GTK_POLICY_ALWAYS,
				      GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(man_hbox),man_scr1,TRUE,TRUE,0);

	man_text=gtk_text_new(NULL,NULL);
	gtk_widget_set_usize(man_text,450,400);
	gtk_text_set_editable(man_text,FALSE);
	gtk_container_add(GTK_CONTAINER(man_scr1),man_text);

	man_vbox=gtk_vbox_new(FALSE,0);
	gtk_box_pack_start(GTK_BOX(man_hbox),man_vbox,TRUE,TRUE,0);

	man_scr2=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(man_scr2),GTK_POLICY_ALWAYS,
				      GTK_POLICY_AUTOMATIC);
	gtk_widget_set_usize(man_scr2,200,200);
	gtk_box_pack_start(GTK_BOX(man_vbox),man_scr2,TRUE,TRUE,0);


	pTreeStore = gtk_tree_store_new(2,G_TYPE_STRING,G_TYPE_STRING);

	/* First degree */
	gtk_tree_store_append(pTreeStore, &pIter, NULL);
	gtk_tree_store_set(pTreeStore, &pIter,
		0,"Menus",1,"menus.txt",-1);

	/* Second degree */
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"General",1,"general.txt",-1);

	/* Third degree */
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Load",1,"load.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Save",1,"save.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Save As",1,"save_as.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Undo",1,"undo.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Preferences",1,"preferences.txt",-1);

	/* Fourth degree */
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Plotting tool",1,"plotting_tool.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Printer",1,"printer.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Resolutions dir.",1,"resol_dir.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Undo depth",1,"undo_depth.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Example file",1,"example_file.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"About",1,"about.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Quit",1,"quit.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Tools",1,"tools.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Analyzers state",1,"ana_state.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"File name",1,"ana_file_name.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Extension",1,"ana_extension.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Counting time",1,"ana_count_time.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Description",1,"ana_desc.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Analyzer",1,"ana_analyzer.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Reflexion",1,"ana_reflexion.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Q (optional)",1,"q_optional.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Print main ana only",1,"main_ana_only.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Xmin and Xmax",1,"xmin_xmax.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Xshift",1,"xshift.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Import data file",1,"import_data_file.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Add file",1,"import_add_file.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Remove file",1,"import_remove_file.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Shift",1,"import_shift.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Reflexion",1,"import_reflexion.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Normalization",1,"import_norm.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Analyzer",1,"import_analyzer.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Target file",1,"import_target_file.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Center",1,"import_center.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter4, &pIter3);
	gtk_tree_store_set(pTreeStore, &pIter4,
		0,"Sum",1,"import_sum.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Wave vector",1,"wave_vector.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Kill current fitting",1,"kill_current_fit.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Refresh data",1,"refresh_data.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Print plot to file",1,"print_plot_to_file.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Help",1,"help.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Report bug",1,"report_bug.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Manual",1,"manual.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter, NULL);
	gtk_tree_store_set(pTreeStore, &pIter,
		0,"Main window",1,"main_window.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Top part",1,"top_part.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Input file button",1,"input_file.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Data",1,"data_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Fit",1,"fit_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Do",1,"do_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Parameters",1,"parameters_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"T",1,"t_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Frac step",1,"frac_step_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Integ time",1,"integ_time_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Number of excitations",1,"num_of_exc.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Model",1,"model.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Cnt/Int",1,"cnt_int.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Title",1,"title_box.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"General parameters",1,"gene_param.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Fit parameters",1,"fit_parameters.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Fitting sequences",1,"fitting_seq.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Central shape",1,"central_shape.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Resolution",1,"resolution_text_box.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"P-V load",1,"pv_load.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter3, &pIter2);
	gtk_tree_store_set(pTreeStore, &pIter3,
		0,"Convolution types",1,"convol_types.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter, NULL);
	gtk_tree_store_set(pTreeStore, &pIter,
		0,"Bottom buttons",1,"bottom_buttons.txt",-1);

	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Preview",1,"preview.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Fit",1,"fit.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Plot fit",1,"plot.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Print",1,"print.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Y-LOG",1,"y_log.txt",-1);
	gtk_tree_store_append(pTreeStore, &pIter2, &pIter);
	gtk_tree_store_set(pTreeStore, &pIter2,
		0,"Live",1,"live.txt",-1);

	/* Display the TreeStore */
	pTreeView = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pTreeStore));
	pCellRenderer = gtk_cell_renderer_text_new();
	pColumn = gtk_tree_view_column_new_with_attributes("Topics",
        	pCellRenderer,"text",0,NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pTreeView), pColumn);
	pColumn = gtk_tree_view_column_new_with_attributes("Files",
        	pCellRenderer,"text",1,NULL);
	gtk_tree_view_column_set_visible(pColumn,FALSE);
	gtk_tree_view_append_column(GTK_TREE_VIEW(pTreeView), pColumn);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(pTreeView),FALSE);

	gtk_signal_connect_object(GTK_OBJECT(pTreeView),"cursor-changed",GTK_SIGNAL_FUNC(man_selection_changed),
		pTreeView);


	gtk_container_add(GTK_CONTAINER(man_scr2), pTreeView);

	man_button=gtk_button_new_with_label("OK");
	gtk_signal_connect_object(GTK_OBJECT(man_button),"clicked",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(man_win));
	gtk_box_pack_start(GTK_BOX(man_vbox),man_button,FALSE,FALSE,0);

	gtk_widget_show_all(man_win);

} 

void on_man_ok_activate (GtkWidget *item, gpointer user_data)
{
	gtk_widget_destroy(GTK_WIDGET(man_win));
}

void man_selection_changed(GtkWidget *pTreeView)
{
	GList *i;
	GtkLabel *label;
	GtkWidget *item;
	GtkTreeModel *tmodel;
	GtkTreeSelection *tselect;
 	GtkTreeIter pIter;
	char buffer[BUFSIZ];
	char full_name[300];
	gchar *fname;
	int lvl;
	int nchars;
	FILE *ff;

	
	tselect = gtk_tree_view_get_selection(pTreeView);
	gtk_tree_selection_get_selected(tselect,NULL,&pIter);
	tmodel = gtk_tree_view_get_model(pTreeView);
	gtk_tree_model_get(tmodel,&pIter,1,&fname,-1);

	strcpy(full_name,getenv("HOME"));
	strcat(full_name,"/.fit28/manual/");
	strcat(full_name,fname);

	ff=fopen(full_name,"r");
	if (!ff)
	{
		printf("Error: can not find file [%s]\n",full_name);
		return;
	}
	gtk_editable_delete_text(GTK_EDITABLE(man_text),0,-1);
	while (1)
	{
		nchars=fread(buffer,1,1024,ff);
		gtk_text_insert(man_text,NULL,NULL,NULL,buffer,nchars);
		if (nchars<1024)
			break;
	}
	fclose(ff);
}

