
#include <gtk/gtk.h>

extern GtkWidget *man_win,*man_hbox,*man_vbox,*man_button,*man_scr1,*man_scr2,*man_text;
extern GtkWidget *man_tree[10],*man_tree_item;


void create_manual_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void on_man_ok_activate (GtkWidget *item, gpointer user_data);
void man_selection_changed(GtkWidget *tree);
