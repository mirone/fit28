/*
 * options.c - A. BERAUD  (September 2003,2004)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "options.h"
#include "interface.h"
#include "callbacks.h"


GtkWidget *opt_win,*opt_vbox,*opt_button,*opt_label;
GtkWidget *opt_hbox_plot_tool,*opt_plot_button1,*opt_plot_button2,*opt_plot_button3;
GtkWidget *opt_hbox_printer,*opt_txt_printer;
GtkWidget *opt_hbox_resol,*opt_txt_resol;
GtkWidget *opt_hbox_undo,*opt_txt_undo;

int opt_loaded;

/* Self explanatory. */
void create_options_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	char tmp[10];
	opt_loaded = 0;

	opt_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(opt_win),"Fit28 options");
	gtk_signal_connect(GTK_OBJECT(opt_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(opt_win));
	gtk_signal_connect(GTK_OBJECT(opt_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(opt_win));

	opt_vbox=gtk_vbox_new(FALSE,2);
	gtk_widget_ref(opt_vbox);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_vbox",opt_vbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_container_add(GTK_CONTAINER(opt_win),opt_vbox);

	opt_hbox_plot_tool = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(opt_hbox_plot_tool);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_hbox_plot_tool",opt_hbox_plot_tool,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(opt_hbox_plot_tool);
	gtk_box_pack_start(GTK_BOX(opt_vbox),opt_hbox_plot_tool,FALSE,FALSE,0);

	opt_label=gtk_label_new("Plotting tool:");
	gtk_box_pack_start(GTK_BOX(opt_hbox_plot_tool),opt_label,TRUE,TRUE,0);

	opt_plot_button1 = gtk_radio_button_new_with_label(NULL,"c-plot");
	gtk_signal_connect(GTK_OBJECT(opt_plot_button1),"clicked",
		GTK_SIGNAL_FUNC(on_plot_activate_plot_tool_button),"c-plot");
	if (plot_prgm == 0)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(opt_plot_button1),TRUE);
	gtk_box_pack_start(GTK_BOX(opt_hbox_plot_tool),opt_plot_button1,TRUE,TRUE,0);
	gtk_widget_show(opt_plot_button1);

	opt_plot_button2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(opt_plot_button1),"gnuplot");
	gtk_signal_connect(GTK_OBJECT(opt_plot_button2),"clicked",
		GTK_SIGNAL_FUNC(on_plot_activate_plot_tool_button),"gnuplot");
	if (plot_prgm == 1)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(opt_plot_button2),TRUE);
	gtk_box_pack_start(GTK_BOX(opt_hbox_plot_tool),opt_plot_button2,TRUE,TRUE,0);
	gtk_widget_show(opt_plot_button2);

	opt_plot_button3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(opt_plot_button1),"own");
	gtk_signal_connect(GTK_OBJECT(opt_plot_button3),"clicked",
		GTK_SIGNAL_FUNC(on_plot_activate_plot_tool_button),"own");
	if (plot_prgm == 2)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(opt_plot_button3),TRUE);
	gtk_box_pack_start(GTK_BOX(opt_hbox_plot_tool),opt_plot_button3,TRUE,TRUE,0);
	gtk_widget_show(opt_plot_button3);

	opt_hbox_printer = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(opt_hbox_printer);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_hbox_printer",opt_hbox_printer,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(opt_hbox_printer);
	gtk_box_pack_start(GTK_BOX(opt_vbox),opt_hbox_printer,FALSE,FALSE,0);

	opt_label=gtk_label_new("Printer");
	gtk_box_pack_start(GTK_BOX(opt_hbox_printer),opt_label,TRUE,TRUE,0);

	opt_txt_printer = gtk_entry_new();
	gtk_widget_ref(opt_txt_printer);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_txt_printer",opt_txt_printer,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)opt_txt_printer,printer);
	gtk_widget_show(opt_txt_printer);
	gtk_box_pack_start(GTK_BOX(opt_hbox_printer),opt_txt_printer,TRUE,TRUE,0);

	opt_hbox_resol = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(opt_hbox_resol);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_hbox_resol",opt_hbox_resol,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(opt_hbox_resol);
	gtk_box_pack_start(GTK_BOX(opt_vbox),opt_hbox_resol,FALSE,FALSE,0);

	opt_label=gtk_label_new("Resolutions path");
	gtk_box_pack_start(GTK_BOX(opt_hbox_resol),opt_label,TRUE,TRUE,0);

	opt_txt_resol = gtk_entry_new();
	gtk_widget_ref(opt_txt_resol);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_txt_resol",opt_txt_resol,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)opt_txt_resol,resol_dir);
	gtk_widget_show(opt_txt_resol);
	gtk_box_pack_start(GTK_BOX(opt_hbox_resol),opt_txt_resol,TRUE,TRUE,0);

	opt_hbox_undo = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(opt_hbox_undo);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_hbox_undo",opt_hbox_undo,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(opt_hbox_undo);
	gtk_box_pack_start(GTK_BOX(opt_vbox),opt_hbox_undo,FALSE,FALSE,0);

	opt_label=gtk_label_new("Undo depth");
	gtk_box_pack_start(GTK_BOX(opt_hbox_undo),opt_label,TRUE,TRUE,0);

	opt_txt_undo = gtk_entry_new();
	gtk_widget_ref(opt_txt_undo);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_txt_undo",opt_txt_undo,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmp,"%d",undo_depth);
	gtk_entry_set_text((GtkEntry *)opt_txt_undo,tmp);
	gtk_widget_show(opt_txt_undo);
	gtk_box_pack_start(GTK_BOX(opt_hbox_undo),opt_txt_undo,TRUE,TRUE,0);

	opt_button=gtk_button_new_with_label("OK");
	gtk_widget_ref(opt_button);
	gtk_object_set_data_full(GTK_OBJECT(opt_win),"opt_button",opt_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(opt_button);
	gtk_signal_connect(GTK_OBJECT(opt_button),"clicked",
		GTK_SIGNAL_FUNC(on_opt_ok_activate),opt_win);
	gtk_box_pack_start(GTK_BOX(opt_vbox),opt_button,FALSE,FALSE,0);

	gtk_widget_show_all(opt_win);
	opt_loaded = 1;
} 

void on_opt_ok_activate (GtkWidget *item, gpointer user_data)
{
	strcpy(printer,gtk_entry_get_text(GTK_ENTRY(opt_txt_printer)));
	strcpy(resol_dir,gtk_entry_get_text(GTK_ENTRY(opt_txt_resol)));
	if (resol_dir[strlen(resol_dir)-1] != '/')
		strcat(resol_dir,"/");
	undo_depth = atoi(gtk_entry_get_text(GTK_ENTRY(opt_txt_undo)));
	if (undo_depth<0)
		undo_depth=0;
	if (undo_depth>1000)
		undo_depth=1000;

	save_preferences();

	gtk_widget_destroy(GTK_WIDGET(opt_win));
}

void on_plot_activate_plot_tool_button (GtkWidget *item, gpointer user_data)
{
	char *str;
	if (!opt_loaded)
		return;

	str = g_strdup((char *) user_data);

	if (!strcmp(str,"c-plot"))
	{
		close_gnuplot();
		plot_prgm = 0;
	}
	if (!strcmp(str,"gnuplot"))
	{
		close_cplot();
		plot_prgm = 1;
	}
	if (!strcmp(str,"own"))
	{
		close_cplot();
		close_gnuplot();
		plot_prgm = 2;
	}
}

void save_preferences (void)
{
	char fname[300],tmp[300];
	FILE *fil;

	strcpy(fname,getenv("HOME"));
	strcat(fname,"/.fit28/preferences.dat");

	fil = fopen(fname,"w");
	if (!fil)
	{
		printf("Error: could not open preferences file [%s]\n",fname);
		return;
	}
	sprintf(tmp,"%8s %d\n","plotprgm",plot_prgm);
	fputs(tmp,fil);
	sprintf(tmp,"%8s %s\n","printer_",printer);
	fputs(tmp,fil);
	sprintf(tmp,"%8s %s\n","resoldir",resol_dir);
	fputs(tmp,fil);
	sprintf(tmp,"%8s %d\n","undodept",undo_depth);
	fputs(tmp,fil);
	fclose(fil);
}

void load_preferences (void)
{
	char fname[300],tmp[300],type[10],data[BUFSIZ];
	FILE *fil;

	strcpy(fname,getenv("HOME"));
	strcat(fname,"/.fit28/preferences.dat");

	fil = fopen(fname,"r");
	if (!fil)
	{
		printf("Error: could not open preferences file [%s]\n",fname);
		return;
	}

	printf("Reading preferences.\n");
	while (fgets(tmp,BUFSIZ,fil))
	{
		sscanf(tmp,"%8s %s",type,data);
		if (!strcmp(type,"plotprgm")) {
			plot_prgm = atoi(data);
		} else if (!strcmp(type,"printer_")) {
			strcpy(printer,data);
		} else if (!strcmp(type,"resoldir")) {
			strcpy(resol_dir,data);
		} else if (!strcmp(type,"undodept")) {
			undo_depth = atoi(data);
		}
	}
	fclose(fil);
}

