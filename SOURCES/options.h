
#include <gtk/gtk.h>

extern GtkWidget *opt_win,*opt_vbox,*opt_button,*opt_label;
extern GtkWidget *opt_hbox_plot_tool,*opt_plot_button1,*opt_plot_button2,*opt_plot_button3;
extern GtkWidget *opt_hbox_printer,*opt_txt_printer;
extern GtkWidget *opt_hbox_resol,*opt_txt_resol;
extern GtkWidget *opt_hbox_undo,*opt_txt_undo;
extern int opt_loaded;

void create_options_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void on_opt_ok_activate (GtkWidget *item, gpointer user_data);
void on_plot_activate_plot_tool_button (GtkWidget *item, gpointer user_data);
void save_preferences (void);
void load_preferences (void);
