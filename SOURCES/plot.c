/*
 * plot.c - A. BERAUD  (September 2003)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "plot.h"


GtkWidget *plot_win,*plot_vbox,*plot_close_button;
GtkWidget *plot_drawing_area;
GdkPixmap *plot_pixmap;
GdkFont *plot_font;


plot_point *plot_table[NUM_PLOT_TABLES];
p_info plot_info[NUM_PLOT_TABLES];

/* Create a new backing pixmap of the appropriate size. */
gint plot_configure_event (GtkWidget *widget, GdkEventConfigure *event)
{
	if (plot_pixmap)
		gdk_pixmap_unref(plot_pixmap);

	plot_pixmap = gdk_pixmap_new(widget->window,
		widget->allocation.width,
		widget->allocation.height,
		-1);
	gdk_draw_rectangle (plot_pixmap,
		widget->style->white_gc,
		TRUE,
		0, 0,
		widget->allocation.width,
		widget->allocation.height);

  return TRUE;
}

/* Redraw the screen from the backing pixmap. */
gint plot_expose_event (GtkWidget *widget, GdkEventExpose *event)
{
	int i;
	for (i=0;i<NUM_PLOT_TABLES;i++)
	{
		if (plot_info[i].used)
			plot_data(i);
	}
	return FALSE;
}

int find_free_plot_table (void)
{
	int i;
	for (i=0;i<NUM_PLOT_TABLES;i++)
	{
		if (plot_info[i].used == 0)
			return i;
	}
	return -1;
}

/* Puts the useful data in the plot_info structure by reading the plot_array. */
void find_extrema (int n)
{
	int i;
	double x_min,x_max,y_min,y_max;
	plot_info[n].x_min = 0;
	plot_info[n].x_max = 0;
	plot_info[n].y_min = 1;
	plot_info[n].y_max = 1;
	if (!plot_table[n])
		return;
	x_min=x_max=y_min=y_max=0;
	for (i=0;i<plot_info[n].nb_points;i++)
	{
		if (plot_table[n][i].x>x_max)
			x_max = plot_table[n][i].x;
		if (plot_table[n][i].x<x_min)
			x_min = plot_table[n][i].x;
		if (plot_table[n][i].y>y_max)
			y_max = plot_table[n][i].y;
		if (plot_table[n][i].y<y_min)
			y_min = plot_table[n][i].y;
	}
	plot_info[n].x_min = x_min;
	plot_info[n].x_max = x_max;
	plot_info[n].y_min = y_min;
	plot_info[n].y_max = y_max;
}

/* Plot the two axis on the white background. */
void plot_axis (int n)
{
	char tmp[300];

	plot_info[n].b_x_min = 50;
	plot_info[n].b_x_max = plot_drawing_area->allocation.width-20;
	plot_info[n].b_y_min = 20;
	plot_info[n].b_y_max = plot_drawing_area->allocation.height-20;
	gdk_draw_line(plot_pixmap,plot_drawing_area->style->black_gc,
		plot_info[n].b_x_min,plot_info[n].b_y_min,
		plot_info[n].b_x_min,plot_info[n].b_y_max);
	gdk_draw_line(plot_pixmap,plot_drawing_area->style->black_gc,
		plot_info[n].b_x_min,plot_info[n].b_y_max,
		plot_info[n].b_x_max,plot_info[n].b_y_max);
	sprintf(tmp,"%.2f",plot_info[n].x_min);
	gdk_draw_string(plot_pixmap,plot_font,plot_drawing_area->style->black_gc,
		plot_info[n].b_x_min-gdk_string_width(plot_font,tmp)/2,plot_info[n].b_y_max+16,tmp);
	sprintf(tmp,"%.2f",plot_info[n].x_max);
	gdk_draw_string(plot_pixmap,plot_font,plot_drawing_area->style->black_gc,
		plot_info[n].b_x_max-gdk_string_width(plot_font,tmp)/2,plot_info[n].b_y_max+16,tmp);
}

/* This is where the plot is actually drawn. */
void plot_data (int n)
{
	int i,x,y,sx=0,sy=0;
	int arc_size;

	/* Draw a white background on the pixmap. */
	gdk_draw_rectangle (plot_pixmap,
		plot_drawing_area->style->white_gc,
		TRUE,0,0,
		plot_drawing_area->allocation.width,
		plot_drawing_area->allocation.height);

	find_extrema(n);
	plot_axis(n);
	arc_size = 5;
	/* Plot the data. */
	for (i=0;i<plot_info[n].nb_points;i++)
	{
		x = plot_info[n].b_x_min +
			((plot_table[n][i].x-plot_info[n].x_min)/(plot_info[n].x_max-plot_info[n].x_min))
			*(plot_info[n].b_x_max-plot_info[n].b_x_min);
		y = plot_info[n].b_y_max -
			((plot_table[n][i].y-plot_info[n].y_min)/(plot_info[n].y_max-plot_info[n].y_min))
			*(plot_info[n].b_y_max-plot_info[n].b_y_min);
		gdk_draw_arc(plot_pixmap,plot_drawing_area->style->black_gc,TRUE,
			x-(int)(arc_size/2),y-(int)(arc_size/2),arc_size,arc_size,0,64*360);
		if (i>0)
			gdk_draw_line(plot_pixmap,plot_drawing_area->style->black_gc,
				x,y,sx,sy);
		sx = x;
		sy = y;
	}

	/* Draw the pixmap on the window. */
	gdk_draw_pixmap(plot_drawing_area->window,
		plot_drawing_area->style->fg_gc[GTK_WIDGET_STATE(plot_drawing_area)],
		plot_pixmap,
		0,0,0,0,plot_drawing_area->allocation.width,
		plot_drawing_area->allocation.height);
}

/* Self explanatory. */
void create_plot_window (void)
{

	plot_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(plot_win),"Plotting tool");
	gtk_signal_connect(GTK_OBJECT(plot_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(plot_win));
	gtk_signal_connect(GTK_OBJECT(plot_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(plot_win));

	plot_vbox=gtk_vbox_new(FALSE,2);
	gtk_widget_ref(plot_vbox);
	gtk_object_set_data_full(GTK_OBJECT(plot_win),"plot_vbox",plot_vbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_container_add(GTK_CONTAINER(plot_win),plot_vbox);

	plot_drawing_area = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA (plot_drawing_area),PLOT_WIDTH,PLOT_HEIGHT);
	gtk_widget_ref(plot_drawing_area);
	gtk_object_set_data_full(GTK_OBJECT(plot_win),"plot_drawing_area",plot_drawing_area,(GtkDestroyNotify)gtk_widget_unref);
	gtk_box_pack_start(GTK_BOX(plot_vbox),plot_drawing_area,TRUE,TRUE,0);

	gtk_signal_connect (GTK_OBJECT (plot_drawing_area), "expose_event",
		(GtkSignalFunc) plot_expose_event, NULL);
	gtk_signal_connect (GTK_OBJECT(plot_drawing_area),"configure_event",
		(GtkSignalFunc) plot_configure_event, NULL);

/*  gtk_widget_set_events (map, GDK_EXPOSURE_MASK
                            | GDK_LEAVE_NOTIFY_MASK
                            | GDK_BUTTON_PRESS_MASK
                            | GDK_BUTTON_RELEASE_MASK
                            | GDK_POINTER_MOTION_MASK
                            | GDK_POINTER_MOTION_HINT_MASK);
  gtk_signal_connect (GTK_OBJECT (map),"motion_notify_event",
                     (GtkSignalFunc)Motion_Notify_Onmap,NULL);
  gtk_signal_connect (GTK_OBJECT (map), "button_press_event",
                     (GtkSignalFunc)Button_Press_Onmap,NULL);
  gtk_signal_connect (GTK_OBJECT (map), "button_release_event",
                     (GtkSignalFunc)Button_Release_Onmap,NULL);
  gtk_signal_connect (GTK_OBJECT (map), "leave_notify_event",
                     (GtkSignalFunc)Mouse_Leave_Onmap,NULL);
*/

	plot_close_button=gtk_button_new_with_label("Close");
	gtk_widget_ref(plot_close_button);
	gtk_object_set_data_full(GTK_OBJECT(plot_win),"plot_close_button",plot_close_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(plot_close_button);
	gtk_signal_connect(GTK_OBJECT(plot_close_button),"clicked",
		GTK_SIGNAL_FUNC(on_plot_close_activate),plot_win);
	gtk_box_pack_start(GTK_BOX(plot_vbox),plot_close_button,FALSE,FALSE,0);

	plot_font = gdk_font_load("-adobe-helvetica-bold-r-normal--12-120-75-75-p-70-iso8859-1");

	gtk_widget_show_all(plot_win);

} 

void on_plot_close_activate (GtkWidget *item, gpointer user_data)
{
	gtk_widget_destroy(GTK_WIDGET(plot_win));
}

