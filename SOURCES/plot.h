
#include <gtk/gtk.h>

#define PLOT_WIDTH 400
#define PLOT_HEIGHT 300
#define NUM_PLOT_TABLES 100

extern GtkWidget *plot_win,*plot_vbox,*plot_close_button;
extern GtkWidget *plot_drawing_area;
extern GdkPixmap *plot_pixmap;
extern GdkFont *plot_font;

typedef struct {
	double x,y,dx,dy;
	short int shape;
} plot_point;

extern plot_point *plot_table[NUM_PLOT_TABLES];

typedef struct {
	short int used;
	double x_min,x_max,y_min,y_max; /* Coordinates */
	int b_x_min,b_x_max,b_y_min,b_y_max; /* Pixels */
	int nb_points;
} p_info;

extern p_info plot_info[NUM_PLOT_TABLES];

int find_free_plot_table (void);
void find_extrema (int n);
void plot_axis (int n);
void plot_data (int n);
void create_plot_window (void);
void on_plot_close_activate (GtkWidget *item, gpointer user_data);
gint plot_configure_event (GtkWidget *widget, GdkEventConfigure *event);
gint plot_expose_event (GtkWidget *widget, GdkEventExpose *event);
