/*
 * resol_win.c - A. BERAUD  (April 2004)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "resol_win.h"
#include "interface.h"
#include "callbacks.h"


GtkWidget *resol_win,*resol_vbox,*resol_button,*resol_label;
GtkWidget *resol_hbox_analyzer;
GtkWidget *resol_ana_button1,*resol_ana_button2,*resol_ana_button3;
GtkWidget *resol_ana_button4,*resol_ana_button5;
GtkWidget *resol_hbox_reflexion;
GtkWidget *resol_ref_button1,*resol_ref_button2,*resol_ref_button3;
GtkWidget *resol_ref_button4,*resol_ref_button5;
GtkWidget *resol_hbox_current;
GtkWidget *resol_cur_button1,*resol_cur_button2,*resol_cur_button3;
GtkWidget *resol_cur_button4,*resol_cur_button5;
GtkWidget *resol_hbox_name;
GtkWidget *resol_txt_name;
int resol_loaded;
int resol_ana,resol_ref,resol_cur;
char resol_name[300];



/* Self explanatory. */
void create_resol_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	resol_loaded = 0;
	resol_ana = 2;
	resol_ref = 9;
	resol_cur = 200;
	strcpy(resol_name,"a2res9.200");

	resol_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(resol_win),"Chose resolution");
	gtk_signal_connect(GTK_OBJECT(resol_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(resol_win));
	gtk_signal_connect(GTK_OBJECT(resol_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(resol_win));

	resol_vbox=gtk_vbox_new(FALSE,4);
	gtk_widget_ref(resol_vbox);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_vbox",resol_vbox,(GtkDestroyNotify)gtk_widget_unref);
	gtk_container_add(GTK_CONTAINER(resol_win),resol_vbox);

	resol_hbox_analyzer = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(resol_hbox_analyzer);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_hbox_analyzer",resol_hbox_analyzer,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(resol_hbox_analyzer);
	gtk_box_pack_start(GTK_BOX(resol_vbox),resol_hbox_analyzer,FALSE,FALSE,0);

	resol_label=gtk_label_new("Analyzer");
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_label,TRUE,TRUE,0);

	resol_ana_button1 = gtk_radio_button_new_with_label(NULL,"1");
	gtk_signal_connect(GTK_OBJECT(resol_ana_button1),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_analyzer_button),"1");
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_ana_button1,TRUE,TRUE,0);
	gtk_widget_show(resol_ana_button1);

	resol_ana_button2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ana_button1),"2");
	gtk_signal_connect(GTK_OBJECT(resol_ana_button2),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_analyzer_button),"2");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(resol_ana_button2),TRUE);
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_ana_button2,TRUE,TRUE,0);
	gtk_widget_show(resol_ana_button2);

	resol_ana_button3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ana_button1),"3");
	gtk_signal_connect(GTK_OBJECT(resol_ana_button3),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_analyzer_button),"3");
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_ana_button3,TRUE,TRUE,0);
	gtk_widget_show(resol_ana_button3);

	resol_ana_button4 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ana_button1),"4");
	gtk_signal_connect(GTK_OBJECT(resol_ana_button4),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_analyzer_button),"4");
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_ana_button4,TRUE,TRUE,0);
	gtk_widget_show(resol_ana_button4);

	resol_ana_button5 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ana_button1),"5");
	gtk_signal_connect(GTK_OBJECT(resol_ana_button5),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_analyzer_button),"5");
	gtk_box_pack_start(GTK_BOX(resol_hbox_analyzer),resol_ana_button5,TRUE,TRUE,0);
	gtk_widget_show(resol_ana_button5);

	resol_hbox_reflexion = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(resol_hbox_reflexion);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_hbox_reflexion",resol_hbox_reflexion,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(resol_hbox_reflexion);
	gtk_box_pack_start(GTK_BOX(resol_vbox),resol_hbox_reflexion,FALSE,FALSE,0);

	resol_label=gtk_label_new("Reflexion");
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_label,TRUE,TRUE,0);

	resol_ref_button1 = gtk_radio_button_new_with_label(NULL,"7 7 7");
	gtk_signal_connect(GTK_OBJECT(resol_ref_button1),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_reflexion_button),"7");
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_ref_button1,TRUE,TRUE,0);
	gtk_widget_show(resol_ref_button1);

	resol_ref_button2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ref_button1),"8 8 8");
	gtk_signal_connect(GTK_OBJECT(resol_ref_button2),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_reflexion_button),"8");
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_ref_button2,TRUE,TRUE,0);
	gtk_widget_show(resol_ref_button2);

	resol_ref_button3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ref_button1),"9 9 9");
	gtk_signal_connect(GTK_OBJECT(resol_ref_button3),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_reflexion_button),"9");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(resol_ref_button3),TRUE);
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_ref_button3,TRUE,TRUE,0);
	gtk_widget_show(resol_ref_button3);

	resol_ref_button4 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ref_button1),"11 11 11");
	gtk_signal_connect(GTK_OBJECT(resol_ref_button4),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_reflexion_button),"11");
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_ref_button4,TRUE,TRUE,0);
	gtk_widget_show(resol_ref_button4);

	resol_ref_button5 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_ref_button1),"13 13 13");
	gtk_signal_connect(GTK_OBJECT(resol_ref_button5),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_reflexion_button),"13");
	gtk_box_pack_start(GTK_BOX(resol_hbox_reflexion),resol_ref_button5,TRUE,TRUE,0);
	gtk_widget_show(resol_ref_button5);

	resol_hbox_current = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(resol_hbox_current);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_hbox_current",resol_hbox_current,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(resol_hbox_current);
	gtk_box_pack_start(GTK_BOX(resol_vbox),resol_hbox_current,FALSE,FALSE,0);

	resol_label=gtk_label_new("Operating mode");
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_label,TRUE,TRUE,0);

	resol_cur_button1 = gtk_radio_button_new_with_label(NULL,"Uniform filling");
	gtk_signal_connect(GTK_OBJECT(resol_cur_button1),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_current_button),"200");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(resol_cur_button1),TRUE);
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_cur_button1,TRUE,TRUE,0);
	gtk_widget_show(resol_cur_button1);

	resol_cur_button2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_cur_button1),"Single bunch");
	gtk_signal_connect(GTK_OBJECT(resol_cur_button2),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_current_button),"16");
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_cur_button2,TRUE,TRUE,0);
	gtk_widget_show(resol_cur_button2);

	resol_cur_button3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_cur_button1),"16 bunch");
	gtk_signal_connect(GTK_OBJECT(resol_cur_button3),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_current_button),"90");
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_cur_button3,TRUE,TRUE,0);
	gtk_widget_show(resol_cur_button3);

	resol_cur_button4 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_cur_button1),"Hybrid mode");
	gtk_signal_connect(GTK_OBJECT(resol_cur_button4),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_current_button),"200");
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_cur_button4,TRUE,TRUE,0);
	gtk_widget_show(resol_cur_button4);

	resol_cur_button5 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(resol_cur_button1),"2 x 1/3 filling");
	gtk_signal_connect(GTK_OBJECT(resol_cur_button5),"clicked",
		GTK_SIGNAL_FUNC(on_resol_activate_current_button),"200");
	gtk_box_pack_start(GTK_BOX(resol_hbox_current),resol_cur_button5,TRUE,TRUE,0);
	gtk_widget_show(resol_cur_button5);


	resol_hbox_name = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(resol_hbox_name);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_hbox_name",resol_hbox_name,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(resol_hbox_name);
	gtk_box_pack_start(GTK_BOX(resol_vbox),resol_hbox_name,FALSE,FALSE,0);

	resol_txt_name = gtk_entry_new();
	gtk_widget_ref(resol_txt_name);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_txt_name",resol_txt_name,(GtkDestroyNotify)gtk_widget_unref);
	gtk_entry_set_text((GtkEntry *)resol_txt_name,resol_name);
	gtk_widget_show(resol_txt_name);
	gtk_box_pack_start(GTK_BOX(resol_hbox_name),resol_txt_name,TRUE,TRUE,0);

	resol_button=gtk_button_new_with_label("OK");
	gtk_widget_ref(resol_button);
	gtk_object_set_data_full(GTK_OBJECT(resol_win),"resol_button",resol_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(resol_button);
	gtk_signal_connect(GTK_OBJECT(resol_button),"clicked",
		GTK_SIGNAL_FUNC(on_resol_ok_activate),resol_win);
	gtk_box_pack_start(GTK_BOX(resol_hbox_name),resol_button,TRUE,TRUE,0);

	gtk_widget_show_all(resol_win);
	resol_loaded = 1;
} 

void refresh_resolution_name (void)
{
	sprintf(resol_name,"a%ires%i.%03i",resol_ana,resol_ref,resol_cur);
	gtk_entry_set_text(GTK_ENTRY(resol_txt_name),resol_name);
}

void on_resol_ok_activate (GtkWidget *item, gpointer user_data)
{
/*	strcpy(printer,gtk_entry_get_text(GTK_ENTRY(opt_txt_printer))); */

	strcpy(nomeris,resol_name);
	gtk_entry_set_text(GTK_ENTRY(box_res_file_text),nomeris);
	gtk_widget_destroy(GTK_WIDGET(resol_win));
}

void on_resol_activate_analyzer_button (GtkWidget *item, gpointer user_data)
{
	char *str;
	if (!resol_loaded)
		return;

	str = g_strdup((char *) user_data);

	resol_ana = atoi(str);
	refresh_resolution_name();
}

void on_resol_activate_reflexion_button (GtkWidget *item, gpointer user_data)
{
	char *str;
	if (!resol_loaded)
		return;

	str = g_strdup((char *) user_data);

	resol_ref = atoi(str);
	refresh_resolution_name();
}

void on_resol_activate_current_button (GtkWidget *item, gpointer user_data)
{
	char *str;
	if (!resol_loaded)
		return;

	str = g_strdup((char *) user_data);

	resol_cur = atoi(str);
	refresh_resolution_name();
}
