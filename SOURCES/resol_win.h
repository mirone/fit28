
#include <gtk/gtk.h>

extern GtkWidget *resol_win,*resol_vbox,*resol_button,*resol_label;
extern GtkWidget *resol_hbox_analyzer;
extern GtkWidget *resol_ana_button1,*resol_ana_button2,*resol_ana_button3;
extern GtkWidget *resol_ana_button4,*resol_ana_button5;
extern GtkWidget *resol_hbox_reflexion;
extern GtkWidget *resol_ref_button1,*resol_ref_button2,*resol_ref_button3;
extern GtkWidget *resol_ref_button4,*resol_ref_button5;
extern GtkWidget *resol_hbox_current;
extern GtkWidget *resol_cur_button1,*resol_cur_button2,*resol_cur_button3;
extern GtkWidget *resol_cur_button4,*resol_cur_button5;
extern GtkWidget *resol_hbox_name;
extern GtkWidget *resol_txt_name;
extern int resol_loaded;
extern int resol_ana,resol_ref,resol_cur;
extern char resol_name[300];

void create_resol_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void refresh_resolution_name (void);
void on_resol_ok_activate (GtkWidget *item, gpointer user_data);
void on_resol_activate_analyzer_button (GtkWidget *item, gpointer user_data);
void on_resol_activate_reflexion_button (GtkWidget *item, gpointer user_data);
void on_resol_activate_current_button (GtkWidget *item, gpointer user_data);
