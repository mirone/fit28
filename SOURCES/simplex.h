/******************************************************************************

  simplex.h

  2002-08-14 Vesa Oikonen
  2003-02-04 Vesa Oikonen
    MAX_PARAMETERS changed from 10 to 20.

******************************************************************************/
#ifndef MAX_PARAMETERS
#define MAX_PARAMETERS 20
#endif
/*****************************************************************************/
#ifndef _SIMPLEX_H
#define _SIMPLEX_H
/*****************************************************************************/
extern int SIMPLEX_TEST;
/*****************************************************************************/
double simplex(
  double (*_fun)(double*),
  int parNr,
  double *par,
  double *delta,
  double maxerr,
  int maxiter
);
/*****************************************************************************/
#endif

