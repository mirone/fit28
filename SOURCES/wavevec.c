/*
 * wavevec.c - A. BERAUD  (June 2004)
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <errno.h>

#include "wavevec.h"
#include "interface.h"
#include "callbacks.h"


#define PI 3.1415926536

GtkWidget *wave_win,*wave_vbox,*wave_label;
GtkWidget *wave_hbox_buttons;
GtkWidget *wave_cancel_button,*wave_print_button,*wave_calculation_button;
GtkWidget *wave_hbox_lambda,*wave_txt_lambda;
GtkWidget *wave_hbox_bz,*wave_txt_bzx,*wave_txt_bzy,*wave_txt_bzz;
GtkWidget *wave_hbox_n1,*wave_txt_n1x,*wave_txt_n1y,*wave_txt_n1z;
GtkWidget *wave_hbox_n2,*wave_txt_n2x,*wave_txt_n2y,*wave_txt_n2z;
GtkWidget *wave_hbox_g,*wave_txt_g1,*wave_txt_g2;
GtkWidget *wave_hbox_angles,*wave_txt_th,*wave_txt_tth,*wave_txt_ana;
GtkWidget *wave_hbox_text,*wave_txt_text;
double w_lambda;
double w_b[3];
int w_g[2];
int w_n1[3],w_n2[3];
double w_th,w_tth;
int w_ana;


/* Self explanatory. */
void create_wavevector_window (GtkMenuItem     *menuitem,
                            gpointer         user_data)
{
	char tmpstr[100];

/*	if (!w_lambda)
		w_lambda = 0;
	if (!ana_fname)
		ana_fname = g_strdup("/path/to/data_file"); */

	wave_win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(wave_win),"Wave vectors calculation");
	gtk_signal_connect(GTK_OBJECT(wave_win),"delete_event",
		      (GtkSignalFunc)gtk_widget_destroy,GTK_OBJECT(wave_win));
	gtk_signal_connect(GTK_OBJECT(wave_win),"destroy",(GtkSignalFunc)gtk_widget_destroy,
		      GTK_OBJECT(wave_win));

	wave_vbox=gtk_vbox_new(FALSE,2);
	gtk_widget_ref(wave_vbox);
/*
gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_vbox",wave_vbox,(GtkDestroyNotify)gtk_widget_unref);
*/
	gtk_container_add(GTK_CONTAINER(wave_win),wave_vbox);

	wave_hbox_lambda = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_lambda);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_lambda",wave_hbox_lambda,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_lambda);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_lambda,FALSE,FALSE,0);

	wave_label=gtk_label_new("Lambda");
	gtk_box_pack_start(GTK_BOX(wave_hbox_lambda),wave_label,FALSE,FALSE,0);

	wave_txt_lambda = gtk_entry_new();
	gtk_widget_ref(wave_txt_lambda);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_lambda",wave_txt_lambda,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_lambda);
	gtk_entry_set_text((GtkEntry *)wave_txt_lambda,tmpstr);
	gtk_widget_show(wave_txt_lambda);
	gtk_box_pack_start(GTK_BOX(wave_hbox_lambda),wave_txt_lambda,FALSE,FALSE,0);

	wave_hbox_bz = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_bz);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_bz",wave_hbox_bz,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_bz);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_bz,FALSE,FALSE,0);

	wave_label=gtk_label_new("BZ dimension (x)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_label,FALSE,FALSE,0);

	wave_txt_bzx = gtk_entry_new();
	gtk_widget_ref(wave_txt_bzx);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_bzx",wave_txt_bzx,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_b[0]);
	gtk_entry_set_text((GtkEntry *)wave_txt_bzx,tmpstr);
	gtk_widget_show(wave_txt_bzx);
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_txt_bzx,FALSE,FALSE,0);

	wave_label=gtk_label_new("BZ dimension (y)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_label,FALSE,FALSE,0);

	wave_txt_bzy = gtk_entry_new();
	gtk_widget_ref(wave_txt_bzy);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_bzy",wave_txt_bzy,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_b[1]);
	gtk_entry_set_text((GtkEntry *)wave_txt_bzy,tmpstr);
	gtk_widget_show(wave_txt_bzy);
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_txt_bzy,FALSE,FALSE,0);

	wave_label=gtk_label_new("BZ dimension (z)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_label,FALSE,FALSE,0);

	wave_txt_bzz = gtk_entry_new();
	gtk_widget_ref(wave_txt_bzz);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_bzz",wave_txt_bzz,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_b[2]);
	gtk_entry_set_text((GtkEntry *)wave_txt_bzz,tmpstr);
	gtk_widget_show(wave_txt_bzz);
	gtk_box_pack_start(GTK_BOX(wave_hbox_bz),wave_txt_bzz,FALSE,FALSE,0);

	wave_hbox_n1 = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_n1);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_n1",wave_hbox_n1,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_n1);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_n1,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector1 (h)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_label,FALSE,FALSE,0);

	wave_txt_n1x = gtk_entry_new();
	gtk_widget_ref(wave_txt_n1x);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n1x",wave_txt_n1x,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n1[0]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n1x,tmpstr);
	gtk_widget_show(wave_txt_n1x);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_txt_n1x,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector1 (k)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_label,FALSE,FALSE,0);

	wave_txt_n1y = gtk_entry_new();
	gtk_widget_ref(wave_txt_n1y);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n1y",wave_txt_n1y,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n1[1]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n1y,tmpstr);
	gtk_widget_show(wave_txt_n1y);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_txt_n1y,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector1 (l)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_label,FALSE,FALSE,0);

	wave_txt_n1z = gtk_entry_new();
	gtk_widget_ref(wave_txt_n1z);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n1z",wave_txt_n1z,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n1[2]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n1z,tmpstr);
	gtk_widget_show(wave_txt_n1z);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n1),wave_txt_n1z,FALSE,FALSE,0);

	wave_hbox_n2 = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_n2);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_n2",wave_hbox_n2,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_n2);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_n2,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector2 (h)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_label,FALSE,FALSE,0);

	wave_txt_n2x = gtk_entry_new();
	gtk_widget_ref(wave_txt_n2x);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n2x",wave_txt_n2x,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n2[0]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n2x,tmpstr);
	gtk_widget_show(wave_txt_n2x);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_txt_n2x,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector2 (k)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_label,FALSE,FALSE,0);

	wave_txt_n2y = gtk_entry_new();
	gtk_widget_ref(wave_txt_n2y);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n2y",wave_txt_n2y,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n2[1]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n2y,tmpstr);
	gtk_widget_show(wave_txt_n2y);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_txt_n2y,FALSE,FALSE,0);

	wave_label=gtk_label_new("Unit vector2 (l)");
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_label,FALSE,FALSE,0);

	wave_txt_n2z = gtk_entry_new();
	gtk_widget_ref(wave_txt_n2z);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_n2z",wave_txt_n2z,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_n2[2]);
	gtk_entry_set_text((GtkEntry *)wave_txt_n2z,tmpstr);
	gtk_widget_show(wave_txt_n2z);
	gtk_box_pack_start(GTK_BOX(wave_hbox_n2),wave_txt_n2z,FALSE,FALSE,0);

	wave_hbox_g = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_g);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_g",wave_hbox_g,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_g);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_g,FALSE,FALSE,0);

	wave_label=gtk_label_new("G component 1");
	gtk_box_pack_start(GTK_BOX(wave_hbox_g),wave_label,FALSE,FALSE,0);

	wave_txt_g1 = gtk_entry_new();
	gtk_widget_ref(wave_txt_g1);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_g1",wave_txt_g1,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_g[0]);
	gtk_entry_set_text((GtkEntry *)wave_txt_g1,tmpstr);
	gtk_widget_show(wave_txt_g1);
	gtk_box_pack_start(GTK_BOX(wave_hbox_g),wave_txt_g1,FALSE,FALSE,0);

	wave_label=gtk_label_new("G component 2");
	gtk_box_pack_start(GTK_BOX(wave_hbox_g),wave_label,FALSE,FALSE,0);

	wave_txt_g2 = gtk_entry_new();
	gtk_widget_ref(wave_txt_g2);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_g2",wave_txt_g2,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_g[1]);
	gtk_entry_set_text((GtkEntry *)wave_txt_g2,tmpstr);
	gtk_widget_show(wave_txt_g2);
	gtk_box_pack_start(GTK_BOX(wave_hbox_g),wave_txt_g2,FALSE,FALSE,0);

	wave_hbox_angles = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_angles);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_angles",wave_hbox_angles,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_angles);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_angles,FALSE,FALSE,0);

	wave_label=gtk_label_new("Theta");
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_label,FALSE,FALSE,0);

	wave_txt_th = gtk_entry_new();
	gtk_widget_ref(wave_txt_th);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_th",wave_txt_th,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_th);
	gtk_entry_set_text((GtkEntry *)wave_txt_th,tmpstr);
	gtk_widget_show(wave_txt_th);
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_txt_th,FALSE,FALSE,0);

	wave_label=gtk_label_new("Two theta");
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_label,FALSE,FALSE,0);

	wave_txt_tth = gtk_entry_new();
	gtk_widget_ref(wave_txt_tth);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_tth",wave_txt_tth,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%g",w_tth);
	gtk_entry_set_text((GtkEntry *)wave_txt_tth,tmpstr);
	gtk_widget_show(wave_txt_tth);
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_txt_tth,FALSE,FALSE,0);

	wave_label=gtk_label_new("For analyser");
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_label,FALSE,FALSE,0);

	wave_txt_ana = gtk_entry_new();
	gtk_widget_ref(wave_txt_ana);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_ana",wave_txt_ana,(GtkDestroyNotify)gtk_widget_unref);
	sprintf(tmpstr,"%d",w_ana);
	gtk_entry_set_text((GtkEntry *)wave_txt_ana,tmpstr);
	gtk_widget_show(wave_txt_ana);
	gtk_box_pack_start(GTK_BOX(wave_hbox_angles),wave_txt_ana,FALSE,FALSE,0);

	wave_hbox_text = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_text);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_text",wave_hbox_text,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_text);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_text,FALSE,FALSE,0);

	wave_txt_text=gtk_text_new(NULL,NULL);
	gtk_widget_ref(wave_txt_text);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_txt_text",wave_txt_text,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_txt_text);
	gtk_text_set_editable(wave_txt_text,TRUE);
	gtk_box_pack_start(GTK_BOX(wave_hbox_text),wave_txt_text,TRUE,TRUE,0);

	wave_hbox_buttons = gtk_hbox_new(FALSE,0);
	gtk_widget_ref(wave_hbox_buttons);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_hbox_buttons",wave_hbox_buttons,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_hbox_buttons);
	gtk_box_pack_start(GTK_BOX(wave_vbox),wave_hbox_buttons,FALSE,FALSE,0);

	wave_calculation_button=gtk_button_new_with_label("Calculation");
	gtk_widget_ref(wave_calculation_button);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_calculation_button",wave_calculation_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_calculation_button);
	gtk_signal_connect(GTK_OBJECT(wave_calculation_button),"clicked",
		GTK_SIGNAL_FUNC(on_wave_calculation_activate),wave_win);
	gtk_box_pack_start(GTK_BOX(wave_hbox_buttons),wave_calculation_button,TRUE,TRUE,0);

	wave_print_button=gtk_button_new_with_label("Print");
	gtk_widget_ref(wave_print_button);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_print_button",wave_print_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_print_button);
	gtk_signal_connect(GTK_OBJECT(wave_print_button),"clicked",
		GTK_SIGNAL_FUNC(on_wave_print_activate),wave_win);
	gtk_box_pack_start(GTK_BOX(wave_hbox_buttons),wave_print_button,TRUE,TRUE,0);

	wave_cancel_button=gtk_button_new_with_label("Cancel");
	gtk_widget_ref(wave_cancel_button);
	gtk_object_set_data_full(GTK_OBJECT(wave_win),"wave_cancel_button",wave_cancel_button,(GtkDestroyNotify)gtk_widget_unref);
	gtk_widget_show(wave_cancel_button);
	gtk_signal_connect(GTK_OBJECT(wave_cancel_button),"clicked",
		GTK_SIGNAL_FUNC(on_wave_cancel_activate),wave_win);
	gtk_box_pack_start(GTK_BOX(wave_hbox_buttons),wave_cancel_button,TRUE,TRUE,0);

	gtk_widget_show_all(wave_win);
} 

void on_wave_cancel_activate (GtkWidget *item, gpointer user_data)
{
	wave_capture_data();
	gtk_widget_destroy(GTK_WIDGET(wave_win));
}

void on_wave_calculation_activate (GtkWidget *widget, gpointer data)
{
	int i;
	char tmpstr[300];
	double k0,gx,gy,k0x,k0y,k1x,k1y,gqx,gqy,norm_gq,qx,qy,norm_q;
	double norm_qx,norm_qy;
	double tths[5];
	double tth_diff [5][5] =  /* 2theta difference between analyzers */
		{
			{0,1.577,3.085,4.625,6.203},
			{-1.577,0,1.508,3.058,4.626},
			{-3.085,-1.508,0,1.55,3.118},
			{-4.625,-3.058,-1.55,0,1.568},
			{-6.203,-4.626,-3.118,-1.568,0}
		};

	gtk_text_set_point(wave_txt_text,0);
	gtk_text_forward_delete(wave_txt_text,
		gtk_text_get_length(wave_txt_text));
	wave_capture_data();
	/* Perform some tests */
	if (!w_lambda || !w_ana)
	{
		gtk_text_insert(wave_txt_text,NULL,NULL,NULL,
			"Bad parameter.\n",-1);
		return;
	}

	k0 = 2*PI/w_lambda;
	for (i=0;i<5;i++)
		tths[i] = w_tth+tth_diff[w_ana-1][i];
	gx = w_g[0]*sqrt(pow(w_b[0]*w_n1[0],2)+pow(w_b[1]*w_n1[1],2)+
		pow(w_b[2]*w_n1[2],2));
	gy = w_g[1]*sqrt(pow(w_b[0]*w_n2[0],2)+pow(w_b[1]*w_n2[1],2)+
		pow(w_b[2]*w_n2[2],2));
	k0x = -k0*sin(w_th*PI/180);
	k0y = k0*cos(w_th*PI/180);
	for (i=0;i<5;i++)
	{
		k1x = k0*sin((tths[i]-w_th)*PI/180);	
		k1y = k0*cos((tths[i]-w_th)*PI/180);	
		gqx = k1x-k0x;
		gqy = k1y-k0y;
		norm_gq = sqrt(gqx*gqx+gqy*gqy);
		qx = gqx-gx;
		qy = gqy-gy;
		norm_q = sqrt(qx*qx+qy*qy);
		norm_qx = qx/sqrt(pow(w_b[0]*w_n1[0],2) +
				pow(w_b[1]*w_n1[1],2) + pow(w_b[2]*w_n1[2],2));
		norm_qy = qy/sqrt(pow(w_b[0]*w_n2[0],2) +
				pow(w_b[1]*w_n2[1],2) + pow(w_b[2]*w_n2[2],2));
		sprintf(tmpstr,"Analyser %d: qx = %.3f   qy = %.3f   |q| = %.5f\n",
			i+1,norm_qx,norm_qy,sqrt(pow(norm_qx,2)+pow(norm_qy,2)));
		gtk_text_insert(wave_txt_text,NULL,NULL,NULL,tmpstr,-1);
	}

}

void wave_capture_data (void)
{

	w_lambda = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_lambda)));
	w_b[0] = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_bzx)));
	w_b[1] = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_bzy)));
	w_b[2] = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_bzz)));
	w_g[0] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_g1)));
	w_g[1] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_g2)));
	w_n1[0] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n1x)));
	w_n1[1] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n1y)));
	w_n1[2] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n1z)));
	w_n2[0] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n2x)));
	w_n2[1] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n2y)));
	w_n2[2] = atoi(gtk_entry_get_text(GTK_ENTRY(wave_txt_n2z)));
	w_th = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_th)));
	w_tth = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_tth)));
	w_ana = atof(gtk_entry_get_text(GTK_ENTRY(wave_txt_ana)));
}

void on_wave_print_activate (GtkWidget *item, gpointer user_data)
{
	guint len;
	gchar *t_data;
	char *fname;
	char cmd[300];
	FILE *ff;

	len = gtk_text_get_length(wave_txt_text);
	t_data = gtk_editable_get_chars(GTK_EDITABLE(wave_txt_text),0,len);

	fname = tmpnam((char *)0);
	ff=fopen(fname,"w");
	if (!ff)
	{
		printf("Error: can not open file [%s]\n",fname);
		return;
	}
	fwrite(t_data,sizeof(char),len,ff);
	fclose(ff);
	strcpy(cmd,"lpr -P");
	strcat(cmd,printer);
	strcat(cmd," ");
	strcat(cmd,fname);
	system(cmd);
	unlink(fname);
}

