
#include <gtk/gtk.h>

extern GtkWidget *wave_win,*wave_vbox,*wave_label;
extern GtkWidget *wave_hbox_buttons;
extern GtkWidget *wave_cancel_button,*wave_print_button,*wave_calculation_button;
extern GtkWidget *wave_hbox_lambda,*wave_txt_lambda;
extern GtkWidget *wave_hbox_bz,*wave_txt_bzx,*wave_txt_bzy,*wave_txt_bzz;
extern GtkWidget *wave_hbox_n1,*wave_txt_n1x,*wave_txt_n1y,*wave_txt_n1z;
extern GtkWidget *wave_hbox_n2,*wave_txt_n2x,*wave_txt_n2y,*wave_txt_n2z;
extern GtkWidget *wave_hbox_g,*wave_txt_g1,*wave_txt_g2;
extern GtkWidget *wave_hbox_angles,*wave_txt_th,*wave_txt_tth,*wave_txt_ana;
extern GtkWidget *wave_hbox_text,*wave_txt_text;
extern double w_lambda;
extern double w_b[3];
extern int w_g[2];
extern int w_n1[3],w_n2[3];
extern double w_th,w_tth;
extern int w_ana;



/*
char *ana_fname,*ana_small_fname,ana_ext[100],ana_desc[300];
char ana_ref[100];
int ana_num,ana_ctime,ref_num;
double ana_q;
char ana_xmin[100],ana_xmax[100];
double ana_xshift;
int main_only;
*/


void create_wavevector_window (GtkMenuItem     *menuitem,
                            gpointer         user_data);
void wave_capture_data (void);
void on_wave_cancel_activate (GtkWidget *item, gpointer user_data);
void on_wave_calculation_activate (GtkWidget *widget, gpointer data);
void on_wave_print_activate (GtkWidget *item, gpointer user_data);
