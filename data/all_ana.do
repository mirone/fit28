tx
{replace1}
DE
meV
{replace2}
a.u.

{replace3}

#Conversion ntc analyzer T##########
 fn calc.4 x = 0.0002405245*log(x) + 0.0000001089848*log(x)*log(x)*log(x)
 fn calc.4 x = -266.5 + 1.0/(0.001130065 + x)

# conversion from T in deg Celsius into Delta E in meV

{replace4}

fn calc.4 y = z

np
sy L
#ty . +8 .

ra
#-20
#30
#0
#0.0006

cs t 3
cs l 2
cs n 2
{replace5}

{replace6}

### T-analyser

tx
{replace7}
Tsetpoint
degC
DE
meV

{replace8}

fn calc.4 y = 0.0002405245*log(y) + 0.0000001089848*log(y)*log(y)*log(y)
fn calc.4 y = -266.5 + 1.0/(0.001130065 + y)

{replace9}

np
sy L

cs t 3
cs l 2
cs n 2
{replace10}

zaltp

