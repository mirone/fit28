Chose any kind of minimization procedure: SIMPLEX or
MIGRAD. If both of them are selected, the software will
first perform a SIMPLEX minimization and then the MIGRAD
one using the parameters obtained with the SIMPLEX. By
default both procedures are used. Using MIGRAD alone is
risky.
