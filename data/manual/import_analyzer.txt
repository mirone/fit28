Select the analyzer to be treated. Each scan extracted
off NEWPLOT contains the data for the five analyzers of
the 7m arm. Only one analyzer data can be treated at a
time since the shift differs from one analyzer to another.
