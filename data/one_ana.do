# Treatment for IXS Scans
re
tu 1

{replace1}

tx
{replace2}
DE
meV
norm.(Ione) Intensity
{replace3}

#Conversion ntc analyser T
 fn calc.4 x = 0.0002405245*log(x) + 0.0000001089848*log(x)*log(x)*log(x)
 fn calc.4 x = -266.5 + 1.0/(0.001130065 + x)

{replace4}

################################

fn calc.4 y = z
sa
np
sy .
ra
{replace12}

cs t 3
ft 0
cs l 3
cs n 3
wi 4 15 12 9

zi psfilter @lp @-da4bid28
#zi x11 -rotate
ztapldw
sy L
zp

zaltp

#############################################
# I mirr

{replace5}

tx
Imirr
T setpoint
deg
Imirr
Counts

np
sy L

cs t 3
cs l 2
cs n 2
wi 3.0 8.0 5.5 4

zaltp

######################################################################
# DE of Analyser crystal
tx
DE of Analyser
T Setpoint
deg
DE
meV

{replace6}

fn calc.4 y = 0.0002405245*log(y) + 0.0000001089848*log(y)*log(y)*log(y)
fn calc.4 y = -266.5 + 1.0/(0.001130065 + y)

{replace7}

np
sy L  
cs t 3
cs l 2
cs n 2
wi 11.5 8.0 5.5 4

zaltp

######################################################################
# Ione 
tx
"Optical" stability
T setpoint
deg
I1/Imirr
{replace8}

{replace9}

fn calc.4 y=y/z

np
sy L  
cs t 3
cs l 2
cs n 2
wi 3.0 2.0 5.5 4

zaltp

######################################################################
# Unnorm. Signal versus T set point
tx
Scan vs Setpoint Mono
T Setpoint
deg
Counts
{replace10}

{replace11}

np
sy L  
cs t 3
cs l 2
cs n 2
wi 11.5 2.0 5.5 4
zaltp
zs
