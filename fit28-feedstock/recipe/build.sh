if [ -z ${CONDA_BUILD+x} ]; then
    source /home/aless/miniconda4/conda-bld/debug_1715847768725/work/build_env_setup.sh
fi
#!/usr/bin/env bash
# Get an updated config.sub and config.guess
cp $BUILD_PREFIX/share/gnuconfig/config.* .

export XDG_DATA_DIRS=${XDG_DATA_DIRS}:$PREFIX/share


GDKTARGET=""
if [[ "${target_platform}" == osx-* ]]; then
    export GDKTARGET="quartz"
    export LDFLAGS="${LDFLAGS} -Wl,-rpath,${PREFIX}/lib -framework Carbon"
    # https://discourse.llvm.org/t/clang-16-notice-of-potentially-breaking-changes/65562
    export CFLAGS="${CFLAGS} -Wno-error=incompatible-function-pointer-types"
elif [[ "${target_platform}" == linux-* ]]; then
    export GDKTARGET="x11"
    export LDFLAGS="${LDFLAGS} -Wl,-rpath=${PREFIX}/lib"
fi

configure_args=(
    --disable-dependency-tracking
    --disable-silent-rules
    --disable-glibtest
    --enable-introspection=yes
    --with-gdktarget="${GDKTARGET}"
    --disable-visibility
    --with-html-dir="${SRC_DIR}/html"
)

if [[ "$CONDA_BUILD_CROSS_COMPILATION" == 1 ]]; then
  unset _CONDA_PYTHON_SYSCONFIGDATA_NAME
  (
    mkdir -p native-build
    pushd native-build

    export CC=$CC_FOR_BUILD
    export AR=($CC_FOR_BUILD -print-prog-name=ar)
    export NM=($CC_FOR_BUILD -print-prog-name=nm)
    export LDFLAGS=${LDFLAGS//$PREFIX/$BUILD_PREFIX}
    export PKG_CONFIG_PATH=${BUILD_PREFIX}/lib/pkgconfig

    # Unset them as we're ok with builds that are either slow or non-portable
    unset CFLAGS
    unset CPPFLAGS
    export host_alias=$build_alias
    export PKG_CONFIG_PATH=$BUILD_PREFIX/lib/pkgconfig

    ../configure --prefix=$BUILD_PREFIX "${configure_args[@]}"

    # This script would generate the functions.txt and dump.xml and save them
    # This is loaded in the native build. We assume that the functions exported
    # by glib are the same for the native and cross builds
    export GI_CROSS_LAUNCHER=$BUILD_PREFIX/libexec/gi-cross-launcher-save.sh
    make -j${CPU_COUNT}
    make install
    popd
  )
  export GI_CROSS_LAUNCHER=$BUILD_PREFIX/libexec/gi-cross-launcher-load.sh
fi

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$BUILD_PREFIX/lib/pkgconfig

./configure \
    --prefix="${PREFIX}" \
    "${configure_args[@]}"

make V=0 -j$CPU_COUNT
# make check -j$CPU_COUNT
make install -j$CPU_COUNT


echo "QUI "
pwd

git clone https://gitlab.esrf.fr/mirone/fit28
cd fit28
cd minuit
${fortran_compiler} -c -O3 *.F
rm -f libminuit.a
ar r libminuit.a *.o
cp libminuit.a ../SOURCES
cd ../SOURCES


cat <<EOF > fit28
#!/bin/bash
export GNUPLOT_DRIVER_DIR=\$(dirname \$(realpath \$(find  \$(dirname \$(which gnuplot))/../libexec/gnuplot -name gnuplot_qt)))
fit28exe

EOF

chmod a+xr fit28

cat <<EOF > Makefile
CC = ${c_compiler}
FORT = ${fortran_compiler}
LIBSDIR = ${PREFIX}/lib
BINSDIR = ${PREFIX}/bin


# CFLAGS = -Wall -Wunused		 	\
# 	-DG_DISABLE_DEPRECATED 	 	\
# 	-DGDK_DISABLE_DEPRECATED 	\
# 	-DGDK_PIXBUF_DISABLE_DEPRECATED \
# 	-DGTK_DISABLE_DEPRECATED -g -O3 -Df2cFortran=1

CFLAGS = -Wall -Wunused		 	\
	 -g -O3 -Df2cFortran=1

OBJS    = fit28.o callbacks.o interface.o options.o plot.o ana_win.o\
	manual.o fitvie.o resol_win.o wavevec.o import.o simplex.o                                                              

fit28exe:  \$(OBJS)    
	\$(CC) -Wall -Wextra -g -o fit28exe \$(OBJS)  `pkg-config gtk+-2.0 --cflags --libs`  -Wl,-rpath-link=\$(LIBSDIR) -lX11 -lfftw3 -lm libminuit.a -lgfortran -lpthread
 

.c.o:
	\$(CC) -c $<  \$(CFLAGS) `pkg-config gtk+-2.0 --cflags --libs`  -Wl,-rpath-link=\$(LIBSDIR)  -lfftw3 -lm libminuit.a -lgfortran -lpthread

clean: 
	rm -f *.o fit28exe

install: fit28exe
	cp  fit28exe fit28 \$(BINSDIR)

EOF

make VERBOSE=4
chmod a+xr fit28exe
make install
cd ../..

# We use the GTK 3 version of gtk-update-icon-cache
# https://github.com/conda-forge/gtk2-feedstock/issues/24
rm -f ${PREFIX}/bin/gtk-update-icon-cache
rm -f ${PREFIX}/share/man/man1/gtk-update-icon-cache.1
